import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CInputRadio,
  CLabel,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCollapse,
  CFade,
  CInput,
  CSelect,
  CTooltip 
} from '@coreui/react'
import { userService, authenticationService } from '../../_services';
import { apiRombelDetail, apiRombelList, apiGetMapel, apiPostJurusan, apiJurusan } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

class KelolaMateriSekolah extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            params: this.props.location.query,
            paramsName: this.props.location.name,
            tp_id: this.props.location.tp_id,
            smstr_id: this.props.location.smstr_id,
            arrSmstr: [],
            idJurusan: localStorage.getItem('idjurusan'),
            statusRes: false,
            isLoadData: true,
            list_mapel: [],
            row_id: [],
            modalInfo: false,
            collapseOne: true,
            collapseTwo: false,
            opsiChange: '1',
            conditionAlert: '',
            messageRes: '',
            valueNameClass:'',
            selectedRows: [],
            condEmpatThn: '',
            collapseNon: false,
            kelompok: [],
            jurusanSpId: null,
            jurusan_spId: '',
            mapel_Id: '',
            bidang_keahlian: [],
            data_subjurusan: [],
            data_rombel: [],
            mpid: ''
        };
        //console.log(this.state.arrSmstr)
    }

    componentDidMount() {
        this.getPembelajaran();
        this.getBidangKeahlian();
    }
    
    getBidangKeahlian(){
        try {
            userService.getData(apiJurusan + '1')
            .then(
                user => {
                    this.setState({ bidang_keahlian: user.data.data })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 502){
                            swal("502", "Kesalahan pada Server", "error");
                        }
                    }
                }
            );
            } catch (error) {
                console.log("try Catch" +error);
        }
    }

    async getPembelajaran() {
        await userService.getData(apiGetMapel)
        .then(
            user => {
                this.setState({ list_mapel: user.data.data, isLoadData: false })
            },
            error => {
                if (error.response) {
                    if(error.response.status == 502){
                        swal("502", "Kesalahan pada Server", "error");
                    }
                }
            }
        );
    }

    handleChangeSubSub = event => {
        this.setState({ jurusan_spId: event.target.value, mapel_Id: event.target.value })
    }

    handleBidangKeahlian = event =>{
        this.setState({ data_subjurusan: [], data_rombel: [] })
        try {
            userService.getData(apiJurusan + '2&jurusan_induk=' + event.target.value)
            .then(
                user => {
                    this.setState({ data_subjurusan: user.data.data, data_rombel: [] })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
        } catch (error) {
            console.log("try Catch" +error);
        }
    }

    handleKompetensiKeahlian = event =>{
        this.setState({ data_rombel: [] })
        try {
            userService.getData(apiRombelList + event.target.value + '&semester_id=20201')
            .then(
                user => {
                  this.setState({ data_rombel: user.data.data, isLoadData: false })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
          }catch (error) {
            console.log("try Catch" +error);
          }
    }

    handleSubmit = event => {
        event.preventDefault();

        const formData = {
            mata_pelajaran_id: this.state.mpid,
            jurusan_sp_id: event.target.elements.jurusan_sp_id.value
        };
        
        userService.postData(apiGetMapel, formData)
        .then(
            user => {
                swal("Sukses!", "Berhasil Pilih Mata Pelajaran", "success");
                this.setState({ 
                    modalAssign: false
                });
                this.getPembelajaran();
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'warning',
                            messageRes: 'Input Ada Yang Salah'
                        })
                    }else if(error.response.status == 401){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'Unauthorized!'
                        })
                    }else if(error.response.status == 409){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'warning',
                            messageRes: 'Duplicate Jurusan!'
                        })
                    }
                }
            }
        );
    }

    handleAssign(value){
        const formData = {
            mata_pelajaran_id: value
        }
        
        userService.postData(apiGetMapel, formData)
        .then(
            res =>{
                swal("Sukses!", "Berhasil Pilih Mata Pelajaran", "success");
                this.setState({ 
                    modalAssign: false
                });
                this.getPembelajaran();
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'warning',
                            messageRes: 'Input Ada Yang Salah'
                        })
                    }else if(error.response.status == 401){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'Unauthorized!'
                        })
                    }else if(error.response.status == 409){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'warning',
                            messageRes: 'Duplicate Jurusan!'
                        })
                    }
                }
            }
        );
    }

    handleDelete(pembelajaran_sekolah_id, nama){
        swal({
            title: "Apakah Anda Yakin ?",
            text: "Hapus " + nama + " ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                axios.delete(apiGetMapel, {
                    headers: {
                      Authorization: 'token=' + this.state.currentUser.token
                    },
                    data: {
                        pembelajaran_sekolah_id: pembelajaran_sekolah_id
                    }
                })
                .then(res => {
                    swal("Berhasil!", "Berhasil Hapus " + nama, {
                        icon: "success",
                    });
                    this.getPembelajaran();
                })
                
            }else {
              swal("Data tidak jadi dihapus");
            }
        });
    }
    
    render() {
        const { modalAssign, collapseOne, collapseTwo, modalWali, collapseNon} = this.state;
        
        return (
            <>
            <CRow>
                <CCol sm="12" className="text-center">
                    <CCard>
                        <CCardBody >
                            <h3 id="traffic" className="card-title mb-0">Kelola Mata Pelajaran Sekolah</h3>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="12">
                    <CCard>
                            <CCardBody>
                            <CDataTable
                                responsive
                                hover
                                items={ this.state.list_mapel }
                                fields={ [ 'nama', {key: 'kelompok'}, 'Aksi'] }
                                itemsPerPage={10}
                                pagination
                                sorter
                                loading={ this.state.isLoadData }
                                tableFilter={{label: 'Cari :', placeholder: 'Cari Mata Pelajaran...'}}
                                scopedSlots = {{
                                    'kelompok':
                                    (item)=>(
                                        <td>
                                            { item.kelompok == 'A0' ? <>A</> : item.kelompok == 'B0' ? <>B</> : item.kelompok }
                                        </td>
                                    ),
                                    'Aksi':
                                    (item)=>(
                                        <td>
                                            { item.kelompok == 'A0' ?
                                                item.pembelajaran_sekolah_id == null ?
                                                <>
                                                    <CTooltip placement="right-end" content={ 'Pilih Mata Pelajaran\n ' + item.nama + ' di Sekolah' }>
                                                        <CButton type="submit" size="sm" color="success" onClick={() => { if (window.confirm('Pilih Mata Pelajaran\n' + '(' + item.nama + ') ?')) this.handleAssign(item.mata_pelajaran_id)} } >Pilih</CButton>
                                                    </CTooltip>
                                                    {/* <CButton type="submit" color="danger" >Delete</CButton> */}
                                                </>
                                                :
                                                <>
                                                    <CTooltip placement="right-end" content={ 'Menghapus Mata Pelajaran\n ' + item.nama + ' dari Sekolah'}>
                                                        <CButton
                                                            color="danger"
                                                            size="sm"
                                                            onClick={() => this.handleDelete(item.pembelajaran_sekolah_id, item.nama)}
                                                        >
                                                            Hapus
                                                        </CButton>
                                                    </CTooltip>
                                                </>
                                            : item.kelompok == "B0" ?
                                                item.pembelajaran_sekolah_id == null ?
                                                <>
                                                    <CTooltip placement="right-end" content={ 'Pilih Mata Pelajaran\n' + item.nama + ' di Sekolah' }>
                                                        <CButton type="submit" size="sm" color="success" onClick={() => { if (window.confirm('Pilih Mata Pelajaran\n' + '(' + item.nama + ') ?')) this.handleAssign(item.mata_pelajaran_id)} } >Pilih</CButton>
                                                    </CTooltip>
                                                </>
                                                :
                                                <>
                                                    <CTooltip placement="right-end" content={ 'Menghapus Mata Pelajaran\n' + item.nama + ' dari Sekolah' }>
                                                        <CButton
                                                            color="danger"
                                                            size="sm"
                                                            onClick={() => this.handleDelete(item.pembelajaran_sekolah_id, item.nama)}
                                                        >
                                                            Hapus
                                                        </CButton>
                                                    </CTooltip>
                                                </>
                                            :
                                                item.pembelajaran_sekolah_id == null ?
                                                <>
                                                    <CTooltip placement="right-end" content={ 'Ketuk untuk memilih Bidang Keahlian' }>
                                                        <CButton type="submit" size="sm" color="success" onClick={() => this.setState({ modalAssign: true, mpid: item.mata_pelajaran_id }) }>Pilih</CButton>
                                                    </CTooltip>
                                                </>
                                                :
                                                <>
                                                    <CTooltip placement="right-end" content={ 'Menghapus Mata Pelajaran\n' + item.nama + ' dari Sekolah' }>
                                                        <CButton
                                                            color="danger"
                                                            size="sm"
                                                            // onClick={() => { if(swal('Anda Yakin ?', 'Yakin akan menghapus Mata Pelajaran\n' + '(' + item.nama + '? )', 'warning', {button: "Konfirmasi"})) this.handleDelete(item.pembelajaran_sekolah_id)} }
                                                            onClick={() => this.handleDelete(item.pembelajaran_sekolah_id, item.nama)}
                                                        >
                                                            Hapus
                                                        </CButton>
                                                    </CTooltip>
                                                </>
                                            }
                                        </td>
                                    )

                                }}
                                />
                            </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
            <CModal 
              show={modalAssign} 
              onClose={() => this.setState( { modalAssign: false }) }
              color="success"
            >
            <CForm onSubmit={this.handleSubmit}>
              <CModalHeader>
                <CModalTitle>Pilih Bidang Keahlian</CModalTitle>
              </CModalHeader>
              <CModalBody>
                
                <CFormGroup>
                    <CLabel>Bidang Keahlian</CLabel>
                    <CSelect name="jurusan_sp_id" id="jurusan_sp_id" >
                        <option defaultValue="0" readOnly>Pilih Bidang Keahlian...</option>
                        { this.state.bidang_keahlian.map((item, i) => {
                            return(
                                <option value={ item.jurusan_sp_id } key={i}>{ item.nama_jurusan }</option>
                            )
                        })}
                    </CSelect>
                    <br></br>
                </CFormGroup>
              </CModalBody>
              <CModalFooter>
                <CButton color="danger" onClick={() => this.setState( { modalAssign: false }) }>Batal</CButton>
                <CButton type="submit" color="success">Simpan</CButton>
              </CModalFooter>
              </CForm>
            </CModal>
          </>
        )
    }
}

export default KelolaMateriSekolah