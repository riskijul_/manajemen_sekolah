import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CInputRadio,
  CLabel,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCollapse,
  CFade,
  CInput,
  CSelect,
  CTooltip 
} from '@coreui/react'
import { userService, authenticationService } from '../../_services';
import { apiGetSiswaBaru, apiJurusanSp, apiRombel, apiPostSiswaBaru } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import DataTableExtensions from 'react-data-table-component-extensions';
import swal from 'sweetalert';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

class SiswaBaru extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            list_siswa_baru: [],
            bidang_keahlian: [],
            program_keahlian: [],
            ps_id: '',
            selectedRows: {data_send: []},
            valueSK: '',
            data_get_select_rombel: []
        };
        //console.log(this.state.arrSmstr)
    }

    componentDidMount() {
        this.getSiswaBaru();
        this.getRombel();
        // this.searchRombel();
        this.getJurusan();
    }

    async getSiswaBaru(){
        try {
          await userService.getData(apiGetSiswaBaru+"?selection=1")
            .then(
              user => {
                this.setState({ list_siswa_baru: user.data.data, isLoadData: false, statusRes: false })
              },
              error => {
                if (error.response) {
                  if (error.response.status == 422) {
                    this.setState({ statusRes: true, messageRes: 'Terjadi Kesalahan Saat Menerima Data', conditionAlert: 'danger' })
                  }
                }
              }
            );
        } catch (error) {
          console.log("try Catch" + error);
        }
    }

    async getRombel(){
        await userService.getData(apiRombel + "?tingkat_pendidikan_id=10")
        .then(
            user => {
                var datas = user.data.data.map(item => {
                    return(
                        { value: item.rombongan_belajar_id, label: item.nama }
                    )
                })
              this.setState({ data_rombel: datas, isLoadingRombel: false })
            },

            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'NPSN Tidak Di Input'
                        })
                    }
                }
            }
        );
    }

    getSelectRombel(value) {
        this.setState({ rombongan_belajar_id: value });    
    }

    async getJurusan(){
        await userService.getData(apiJurusanSp + '?jurusan_induk=001301')
        .then(
            user => {
            var datas = user.data.data.map(item => {
                return(
                    { value: item.jurusan_id, label: item.nama_jurusan }
                )
            })
            this.setState({ data_jurusan: datas })
            //console.log(datas)
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            }
        );
    }

    // async getJurusan() {
    //     await axios.get(apiJurusanSp, {
    //         headers: {
    //             'Authorization' : 'token=' + this.state.currentUser.token
    //           }
    //         })
    //         .then(response => {
    //             this.setState({ bidang_keahlian: response.data.data })
    //         })
    //         .catch(error => {
    //             if (error.response) {
    //                 if(error.response.status == 422){
    //                     this.setState({ statusRes: true })
    //                 }
    //             }
    //         });
    // }

    // selectProgramKeahlian = value => {
    //     this.setState({ program_keahlian: [], bidang_keahlian: [] })
    //     axios.get(apiJurusanSp + '?jurusan_induk=' + value.target.value, {
    //         headers: {
    //             'Authorization' : 'token=' + this.state.currentUser.token
    //           }
    //         })
    //     .then(response => {
    //         this.setState({ program_keahlian: response.data.data, bidang_keahlian: [] })
    //     })
    //     .catch(error => {
    //         if (error.response) {
    //             if(error.response.status == 422){
    //                     this.setState({ statusRes: true })
    //             }
    //         }
    //     });
    // };

    handleChangeRow = (state) => {
        // You can use setState or dispatch with something like Redux so we can use the retrieved data
        var data_send = {
            data_send: state.selectedRows
        }
        this.setState({ selectedRows: data_send });
    };

    handleAmbilData(){
        // const pd_arr = this.state.selectedRows.map((item, i) => {
        //     return(
        //         item.peserta_didik_id
        //     )
        // })

        // this.setState({ modalRombel: true, pd_id_arr: pd_arr})

        if(this.state.selectedRows.data_send.length > 0){
            const pd_arr = this.state.selectedRows.data_send.map((item, i) => {
                return(
                    item.peserta_didik_id
                )
            })

            this.setState({ modalRombel: true, pd_id_arr: pd_arr});
        }else{
          this.setState({ statusRes: true, messageRes: 'Mohon pilih peserta didik terlebih dahulu!', conditionAlert: 'warning' })
        }
    }

    submitSiswa = (event) => {
        const formData = {
            rombongan_belajar_id: this.state.rombongan_belajar_id,
            peserta_didik_id: this.state.pd_id_arr
        }
        userService.postData(apiPostSiswaBaru, formData)
            .then(
            user => {
                swal("Sukses!", "Berhasil Menambahkan Peserta Didik Baru ke Rombel", "success");
                this.setState({ modalRombel: false, selectedRows: {data_send:[]} })
                this.getSiswaBaru();
                // if(!alert('Pesan Dari Server : ' + user.data.message)){window.location.reload();}
            },
            error => {
                swal("Gagal!", "Gagal Menambahkan Peserta Didik Baru ke Rombel", "success");
                this.setState({ modalRombel: false })
            }
        )
    }

    render() {
        const { modalRombel } = this.state;
        
        return (
            <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                    <CCardHeader>
                        <CRow>
                        <CCol sm="12">
                            <h4>List Peserta Didik Baru</h4>
                        </CCol>
                        </CRow>
                    </CCardHeader>
                    <CCardBody>
                        <CRow>
                        <CCol sm="12">
                            <CAlert color={this.state.conditionAlert} show={this.state.statusRes}>
                            {this.state.messageRes}
                            </CAlert>
                        </CCol>
                        <CCol sm="12">
                            <CRow>
                            <CCol sm="12" xl="12">
                                <CCard className="card h-100">
                                <CCardBody>
                                    <CCol className="text-center">
                                    <b>{this.state.selectedRows.data_send.length > 0 ? this.state.selectedRows.data_send.length : <>0</>}</b> Peserta Didik Terpilih
                                    </CCol>
                                    <CCol  className="text-center">
                                    <br></br>
                                    <CButton color="info" onClick={() => this.handleAmbilData()}><b>Tambahkan ke Rombel...</b></CButton>
                                    </CCol>
                                </CCardBody>
                                </CCard>
                            </CCol>
                            </CRow>
                        </CCol>
                        <CCol sm="12" style={{ paddingTop: 15}}>
                            <DataTableExtensions
                            columns={[
                                {
                                name: 'Nama Peserta Didik',
                                selector: 'nama',
                                sortable: true,
                                },
                                {
                                name: 'NISN',
                                selector: 'nisn',
                                sortable: true
                                },
                                {
                                name: 'Jenis Kelamin',
                                selector: 'jenis_kelamin',
                                sortable: true
                                },
                                {
                                name: 'Alamat',
                                selector: 'alamat_jalan',
                                sortable: true
                                },
                            ]}
                            data={this.state.list_siswa_baru}
                            export={false}
                            print={false}
                            filterPlaceholder="Cari Nama Peserta Didik/NISN..."
                            >
                            <DataTable
                                noHeader
                                pagination
                                striped
                                paginationPerPage={10}
                                responsive
                                onSelectedRowsChange={this.handleChangeRow}
                                selectableRows
                            />
                            </DataTableExtensions>
                        </CCol>
                        </CRow>
                    </CCardBody>
                    </CCard>
                </CCol>
            </CRow>

        <CModal
          show={modalRombel}
          onClose={() => this.setState({ modalRombel: false })}
          color="info"
        >
          <CForm onSubmit={this.submitSiswa}>
            <CModalHeader>
              <CModalTitle>Tambah Peserta Didik</CModalTitle>
            </CModalHeader>
            <CModalBody>
                <CFormGroup>
                    <CLabel>Peserta Didik yang terpilih : </CLabel>
                    <CCard>
                        <CCardBody style={{ overflowY: 'scroll', height: 200 }}>
                        <ol>
                            {this.state.selectedRows.data_send.length > 0 ? this.state.selectedRows.data_send.map((item, i) => {
                            return (
                                <li key={i}>
                                {item.nama}
                                </li>
                            )
                            }) : <></>}
                        </ol>
                        </CCardBody>
                    </CCard>
                </CFormGroup>

                <hr/>
                <CFormGroup>
                    <CLabel htmlFor="appendedInput">Akan ditambahkan ke Rombel</CLabel>
                    <div className="controls">
                        <Select
                            isLoading={ this.state.isLoadingRombelGuru }
                            options={ this.state.data_rombel }
                            placeholder="Cari Rombel..."
                            onChange={ (value) => this.getSelectRombel(value.value) }
                        />
                    </div>
                </CFormGroup>

            </CModalBody>
            <CModalFooter>
              <CButton color="danger" onClick={() => this.setState({ modalRombel: false })}>Cancel</CButton>
              <CButton color="info" onClick={() => this.submitSiswa()}>Simpan</CButton>
            </CModalFooter>
          </CForm>
        </CModal>
            
          </>
        )
    }
}

export default SiswaBaru