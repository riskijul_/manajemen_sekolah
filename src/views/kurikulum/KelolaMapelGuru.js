import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CBadge,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable
} from '@coreui/react'
import { userService, authenticationService } from '../../_services';
import { apiRombelDetail, apiRombelList, apiGetMapel, apiPostJurusan, apiJurusan, apiGetMapelGuru, apiGetGuru } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import { Redirect } from 'react-router-dom';
import swal from 'sweetalert';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

class KelolaMapelGuru extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            params: this.props.location.query,
            paramsName: this.props.location.name,
            tp_id: this.props.location.tp_id,
            smstr_id: this.props.location.smstr_id,
            arrSmstr: [],
            idJurusan: localStorage.getItem('idjurusan'),
            statusRes: false,
            isLoadData: true,
            list_mapel: [],
            row_id: [],
            modalInfo: false,
            collapseOne: true,
            collapseTwo: false,
            opsiChange: '1',
            conditionAlert: '',
            messageRes: '',
            valueNameClass:'',
            selectedRows: [],
            condEmpatThn: '',
            collapseNon: false,
            kelompok: [],
            jurusanSpId: null,
            jurusan_spId: '',
            mapel_Id: '',
            bidang_keahlian: [],
            data_subjurusan: [],
            data_rombel: [],
            mpid: '',
            nama_mapel: '',
            data_guru: [],
            mapel_id: '',
            isLoadGuru: true
        };
        //console.log(this.state.arrSmstr)
    }

    componentDidMount() {
        this.getMapelGuru();
        this.getBidangKeahlian();
    }

    getBidangKeahlian(){
        try {
            userService.getData(apiJurusan + '1')
            .then(
                user => {
                    this.setState({ bidang_keahlian: user.data.data })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
            } catch (error) {
                console.log("try Catch" +error);
        }
    }

    componentWillUnmount(){
        //this.getRevJurusan();
    }

    async getMapelGuru() {
        await userService.getData(apiGetMapelGuru)
        .then(
            user => {
                this.setState({ list_mapel: user.data.data, isLoadData: false })
            },

            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'NPSN Tidak Di Input'
                        })
                    }
                }
            }
        );
    }

    handleChangeSubSub = event => {
        this.setState({ jurusan_spId: event.target.value, mapel_Id: event.target.value })
    }

    handleBidangKeahlian = event =>{
        try {
            userService.getData(apiJurusan + '2&jurusan_induk=' + event.target.value)
            .then(
                user => {
                    this.setState({ data_subjurusan: user.data.data })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
        } catch (error) {
            console.log("try Catch" +error);
        }
    }

    handleKompetensiKeahlian = event =>{
        try {
            userService.getData(apiRombelList + event.target.value + '&semester_id=20201')
            .then(
                user => {
                  this.setState({ data_rombel: user.data.data, isLoadData: false })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
          }catch (error) {
            console.log("try Catch" +error);
          }
    }

    handleAssign(value){
        const formData = {
            mata_pelajaran_id: value
        }
        
        userService.postData(apiGetMapel, formData)
        .then(
            res =>{
                this.setState({ 
                    statusRes: true,
                    conditionAlert: 'success',
                    messageRes: 'Berhasil Input'
                })
                this.getMapelGuru()
                },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'warning',
                            messageRes: 'Input Ada Yang Salah'
                        })
                    }else if(error.response.status == 401){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'Unauthorized!'
                        })
                    }
                }
            }
        );
    }

    handleDelete(value, nama){
        swal({
            title: "Apakah Anda Yakin ?",
            text: "Hapus " + nama + "?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                const formData = {
                    mata_pelajaran_id: value
                }

                axios.delete(apiGetMapel, {
                    headers: {
                      Authorization: 'token=' + this.state.currentUser.token
                    },
                    data: {
                        pembelajaran_sekolah_id: value
                    }
                })
                .then(
                    res => {
                        swal("Berhasil!", "Berhasil Hapus " + nama, {
                            icon: "success",
                        });
                        this.getMapelGuru();
                    },
                    error => {
                        if (error.response) {
                            if(error.response.status == 422){
                                swal("Gagal", "Input Ada yang Salah", "error");
                            }else if(error.response.status == 401){
                                swal("Gagal", "Unauthorized", "error");
                            }
                        }
                    }
                );
            }else {
              swal("Data tidak jadi dihapus");
            }
        });
    }

    handleAktifkan(value){
        axios.delete(apiGetMapel, {
            headers: {
              Authorization: 'token=' + this.state.currentUser.token
            },
            data: {
                pembelajaran_sekolah_id: value
            }
          })
        .then(res => {
            swal("Berhasil!", "Berhasil Diaktifkan", "success");
            this.getMapelGuru()
        })
        .catch(error => {
            if (error.response) {
                if(error.response.status == 422){
                    swal("Gagal", "Input Ada yang Salah", "error");
                }else if(error.response.status == 401){
                    swal("Gagal", "Unauthorized", "error");
                }
            }
        });
    }

    handleNonAktifkan(value, nama){
        swal({
            title: "Apakah Anda Yakin ?",
            text: "Non Aktifkan " +  nama + "?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                const formData = {
                    mata_pelajaran_id: value
                }

                userService.postData(apiGetMapel, formData)
                .then(
                    res => {
                        swal("Berhasil!", "Berhasil Non Aktifkan " + nama, {
                            icon: "success",
                        });
                        this.getMapelGuru();
                    },
                    error => {
                        if (error.response) {
                            if(error.response.status == 422){
                                swal("Gagal", "Input Ada yang Salah", "error");
                            }else if(error.response.status == 401){
                                swal("Gagal", "Unauthorized", "error");
                            }
                        }
                    }
                );
            }else {
              swal("Data Tidak Jadi di Non Aktifkan");
            }
        });
    }

    handleSessData(cond, nama){
        const data = {
            "id"    : cond,
            "nama"  : nama
        }
        localStorage.setItem('mapelid', JSON.stringify(data));
        //sessionStorage.setItem('idjurusan', cond)
    }
    
    render() {
        const { modalAssign, collapseOne, collapseTwo, modalWali, collapseNon} = this.state;
        
        return (
            <>
            <CRow>
                <CCol sm="12" className="text-center">
                    <CCard>
                        <CCardBody >
                            <h3 id="traffic" className="card-title mb-0">Kelola Mata Pelajaran Guru</h3>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="12">
                    <CCard>
                            <CCardBody>
                            <CAlert color={this.state.conditionAlert} show={this.state.statusRes} closeButton>
                                {this.state.messageRes}
                            </CAlert>
                            <CDataTable
                                items={ this.state.list_mapel }
                                fields={ [{key: 'nama_mata_pelajaran' }, {key: 'kelompok'}, 'Status', 'Aksi'] }
                                itemsPerPage={10}
                                pagination
                                sorter
                                loading={ this.state.isLoadData }
                                tableFilter={{label: 'Cari :', placeholder: 'Cari Mata Pelajaran...'}}
                                scopedSlots = {{
                                    'kelompok':
                                    (item)=>(
                                        <td>
                                            { item.kelompok == 'A0' ? <>A</> : item.kelompok == 'B0' ? <>B</> : item.kelompok }
                                        </td>
                                    ),
                                    'Status':
                                    (item)=>(
                                        <td>
                                            { item.kelompok != 'C1' && item.pembelajaran_sekolah_id != null ?
                                                <>
                                                    <CBadge color="danger">Non Aktif</CBadge>
                                                </>
                                            : item.kelompok == 'C1' && item.pembelajaran_sekolah_id != null ?
                                                <>
                                                    <CBadge color="info">Aktif</CBadge>
                                                </>
                                            : item.kelompok != 'C1' && item.pembelajaran_sekolah_id == null ?
                                                <>
                                                    <CBadge color="info">Aktif</CBadge>
                                                </>
                                            :
                                                <>
                                                
                                                </>
                                            }
                                        </td>
                                    ),
                                    'Aksi':
                                    (item)=>(
                                        <td>
                                            { item.kelompok != 'C1' && item.pembelajaran_sekolah_id != null ?
                                                <>
                                                    <CButton color="success" size="sm" disabled style={{ marginRight: '10px' }} >Pilih</CButton>
                                                    <CButton type="submit" size="sm" color="info" style={{ color: '#ffffff', marginRight: '10px' }} onClick={() => this.handleAktifkan(item.pembelajaran_sekolah_id)} >Aktifkan?</CButton>
                                                </>
                                            : item.kelompok == 'C1' && item.pembelajaran_sekolah_id != null ?
                                                <>
                                                    <CButton color="success" size="sm" style={{ marginRight: '10px' }} onClick={() => this.handleSessData(item.mata_pelajaran_id, item.nama_mata_pelajaran)} to={{ pathname: `/manajemen_sekolah/kelola_mapel_guru/penugasan_guru`, query: item.mata_pelajaran_id, name: item.nama_mata_pelajaran }} >Pilih</CButton>
                                                    <CButton type="submit" size="sm" color="danger" onClick={() => this.handleDelete(item.pembelajaran_sekolah_id, item.nama_mata_pelajaran)} >Hapus</CButton>
                                                </>
                                            : item.kelompok != 'C1' && item.pembelajaran_sekolah_id == null ?
                                                <>
                                                    <CButton color="success" size="sm" style={{ marginRight: '10px' }} onClick={() => this.handleSessData(item.mata_pelajaran_id, item.nama_mata_pelajaran)} to={{ pathname: `/manajemen_sekolah/kelola_mapel_guru/penugasan_guru`, query: item.mata_pelajaran_id, name: item.nama_mata_pelajaran }} >Pilih</CButton>
                                                    <CButton type="submit" size="sm" color="danger" style={{ color: '#ffffff', marginRight: '10px' }} onClick={() => this.handleNonAktifkan(item.mata_pelajaran_id, item.nama_mata_pelajaran)} >Non Aktifkan?</CButton>
                                                </>
                                            :
                                                <>
                                                
                                                </>
                                            }
                                        </td>
                                    ),
                                }}
                                />
                            </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
          </>
        )
    }
}

export default KelolaMapelGuru
