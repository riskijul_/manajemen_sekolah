import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../../_services';
import { history } from '../../../_helpers';
import { apiRombelList, apiGetSemester, apiGetGuru } from '../../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios';
import Select from 'react-select';

class ListRombel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      users: null,
      paramsName: this.props.location.name,
      params: this.props.location.query,
      data_kompetensi: [],
      isLoadData: true,
      idJurusan: localStorage.getItem('idjurusan'),
      rombelID: localStorage.getItem('rombel_id'),
      data_semester: [],
      modalAssign: false,
      isLoadGuru: true
    };

    if (!localStorage.getItem('idjurusan')) {
        this.props.history.push('/manajemen_sekolah/jadwal/list_jurusan');
    }

    //console.log(localStorage.getItem('idjurusan'))
  }

  componentDidMount() {
    try {
      userService.getData(apiRombelList + this.state.idJurusan + '&semester_id=20192')
      .then(
          user => {
            this.setState({ data_kompetensi: user.data.data, isLoadData: false })
          },
          error => {
              if (error.response) {
                  if(error.response.status == 422){
                      this.setState({ statusRes: true })
                  }
              }
          }
      );
    }catch (error) {
      console.log("try Catch" +error);
    }

    this.getSemester()
  }

  getSemester(){
    try {
      userService.getData(apiGetSemester)
      .then(
          user => {
            var datas = user.data.data.map(item => {
              return(
                { value: item, label: item.substring(0, 4) + ' Smt. ' + item.substring(4, 5)}
              )
            })
          this.setState({ data_semester: datas })
          },
          error => {
              if (error.response) {
                  if(error.response.status == 422){
                      this.setState({ statusRes: true })
                  }
              }
          }
      );
    }catch (error) {
      console.log("try Catch" +error);
    }
  }

  getList(value){
    try {
      userService.getData(apiRombelList + this.state.idJurusan + '&semester_id=' + value)
      .then(
          user => {
            this.setState({ data_kompetensi: user.data.data, isLoadData: false })
          },
          error => {
              if (error.response) {
                  if(error.response.status == 422){
                      this.setState({ statusRes: true })
                  }
              }
          }
      );
    }catch (error) {
      console.log("try Catch" +error);
    }
  }

  handleSessData(cond){
    localStorage.removeItem('rombel_id');
    localStorage.setItem('rombel_id', cond);
    //sessionStorage.setItem('idjurusan', cond)
  }

  render() {
    const { currentUser, users, modalAssign } = this.state;
    return (
      <>
        <CRow>
            <CCol sm="12" className="text-center">
                <CCard>
                    <CCardBody>
                        <h3 id="traffic" className="card-title mb-0">Jadwal Mata Pelajaran</h3>
                    </CCardBody>
                </CCard>
            </CCol>
          <CCol>
            <CCard>
                {/* <CCol sm="3" className="text-lg-right">
                  <Select
                    options={this.state.data_semester}
                    placeholder="Pilih Semester..."
                    onChange={(value) => this.getList(value.value)}
                  />
                </CCol> */}
              <CCardBody>  
                <hr style={{ border: "1px solid black" }}/>
                <h4 style={{ margin: "auto", marginLeft: "10px" }}>Pilih Rombongan Belajar</h4>
                <hr style={{ marginBottom: "20px", border: "1px solid black" }}/>

                    <CRow>
                        <CCol sm="9"></CCol>
                        <CCol sm="3" className="text-lg-right" style={{ marginBottom: "20px" }}>
                        <Select
                            options={this.state.data_semester}
                            placeholder="Pilih Semester..."
                            onChange={(value) => this.getList(value.value)}
                        />
                        </CCol>
                    </CRow>

              <CDataTable
                items={this.state.data_kompetensi}
                fields={[{key: 'nama_rombel' }, { key: 'semester' }, { key: 'aksi', sorter: false }]}
                hover
                border
                itemsPerPage={10}
                pagination
                loading={this.state.isLoadData}
                scopedSlots = {{
                  'nama_rombel':
                    (item)=>(
                      <td>
                          {item.nama}
                      </td>
                    ),
                  'semester':
                    (item)=>(
                      <td>
                          {item.semester_id.substring(0, 4) + ' Smt. ' + item.semester_id.substring(4, 5)}
                      </td>
                    ),
                  'aksi':
                    (item)=>(
                      <td>
                          <CButton color="success"  to={{ pathname: `/manajemen_sekolah/jadwal/list_jurusan/list_rombel/list_jadwal`, query: item.rombongan_belajar_id }} onClick={() => this.handleSessData(item.rombongan_belajar_id)} >PILIH</CButton>
                      </td>
                    )
                }}
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    )
  }
}

export default ListRombel
