import React, { lazy } from 'react'
import {
    CButton,
    CCard,
    CAlert,
    CCardBody,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CDataTable,
    CForm,
    CInputRadio,
    CLabel,
    CFormGroup,
    CInput,
    CSelect,
    CTextarea,
    CCardFooter,
    CInvalidFeedback,
    CInputGroup
} from '@coreui/react'
import { userService, authenticationService } from '../../../_services';
import { apiGetJadwalGuru, apiPutJadwal } from '../../../_components';
// import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';
import Moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from '@fortawesome/fontawesome-free-solid';
import TimePicker from 'react-time-picker';
import { formatDate } from '../../../_helpers/date_format';

class EditJadwal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            rombelID: localStorage.getItem('rombel_id'),
            params: JSON.parse(localStorage.getItem('jadwal')),
            pembelajaran_id: JSON.parse(localStorage.getItem('jadwal')).pembelajaran_id,
            nama_matpel: JSON.parse(localStorage.getItem('jadwal')).nama_matpel,
            ptk_id: JSON.parse(localStorage.getItem('jadwal')).ptk_id,
            nama_pengajar: JSON.parse(localStorage.getItem('jadwal')).nama_pengajar,
            nama_rombel: JSON.parse(localStorage.getItem('jadwal')).nama_rombel,
            nm_ruang: JSON.parse(localStorage.getItem('jadwal')).nm_ruang,
            day: JSON.parse(localStorage.getItem('jadwal')).day,
            start: JSON.parse(localStorage.getItem('jadwal')).start,
            end: JSON.parse(localStorage.getItem('jadwal')).end,
            list_guru: [],
            list_mapel: []
        };
    }

    componentDidMount() {
        this.getGuru();
    }

    componentWillUnmount() {
    }

    getGuru() {
        userService.getData(apiGetJadwalGuru + this.state.rombelID)
            .then(response => {
                this.setState({ list_guru: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    handleJamMulai(event) {
        this.setState({ start: event });
    }

    handleJamSelesai(event) {
        this.setState({ end: event });
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const id = target.id;
        console.log(event);

        this.setState({
            [id]: value,
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        var formData = {
            jadwal_kurikulum_id: JSON.parse(localStorage.getItem('jadwal')).jadwal_kurikulum_id,
            day: event.target.elements.day.value,
            start: this.state.start,
            end: this.state.end,
        }

        axios.put(apiPutJadwal, formData, {
            headers: {
                'Authorization': 'token=' + this.state.currentUser.token
            }
        })

            .then(res => {
                swal("Sukses!", "Berhasil Melakukan Perubahan", "success");
                this.props.history.push('/manajemen_sekolah/jadwal/list_jurusan/list_rombel/list_jadwal');
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                        swal("Gagal!", "Input ada yang salah", "error");
                        error.response.data.errors.map((x, key) => {
                            switch (x.param) {
                                case 'nuptk': {
                                    return (
                                        this.setState({ nuptk_err: x.msg, color: '#d93025', invalid: false })
                                    )
                                }
                                case 'tyguh': {
                                    return (console.log('nik'))
                                }
                            }
                        });

                        //   swal("Gagal!", "Input ada yang salah", "error");
                    } else if (error.response.status == 401) {
                        swal("Gagal!", "Unauthorized", "error");
                    } else if (error.response.status == 409) {
                        swal("Gagal!", "Duplikat Data", "error");
                    }
                }
            });
    }

    render() {
        const { } = this.state;

        return (
            <>
                <CForm onSubmit={this.handleSubmit}>
                    <CCard style={{ boxShadow: "3px 3px 3px gray" }}>
                        <CCardBody>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Hari : </CLabel>
                                    <div className="controls">
                                        <CSelect id="day" onChange={(event) => this.handleChange(event)}>
                                            <option defaultValue={this.state.day} value={this.state.day}>{this.state.day} (Hari Asal)</option>
                                            <option value="Senin">Senin</option>
                                            <option value="Selasa">Selasa</option>
                                            <option value="Rabu">Rabu</option>
                                            <option value="Kamis">Kamis</option>
                                            <option value="Jumat">Jumat</option>
                                            <option value="Sabtu">Sabtu</option>
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Jam Mulai : </CLabel>
                                    <div className="controls">
                                        <TimePicker
                                            id="start"
                                            format="HH:mm"
                                            clockIcon={null}
                                            value={this.state.start}
                                            required={true}
                                            onChange={(event) => this.handleJamMulai(event)}
                                        />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Jam Selesai : </CLabel>
                                    <div className="controls">
                                        <TimePicker
                                            id="end"
                                            format="HH:mm"
                                            clockIcon={null}
                                            value={this.state.end}
                                            required={true}
                                            onChange={(event) => this.handleJamSelesai(event)}
                                        />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CButton color="success" type="submit"><FontAwesomeIcon icon={faSave} /> Simpan Perubahan</CButton>
                                </CCol>
                            </CFormGroup>

                        </CCardBody>
                    </CCard>
                </CForm>
            </>
        )
    }
}

export default EditJadwal
