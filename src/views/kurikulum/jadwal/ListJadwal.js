import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CSelect,
  CForm,
  CLabel,
  CFormGroup,
  CInput,
  CInputGroup
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../../_services';
import { history } from '../../../_helpers';
import { apiGetJadwal, apiGetJadwalGuru, apiGetJadwalMapel, apiGetRuangan, apiPostJadwal, apiDeleteJadwal } from '../../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios';
import Select from 'react-select';
import swal from 'sweetalert';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faPlus, faTrashAlt, faUserEdit } from '@fortawesome/fontawesome-free-solid';

class ListRombel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      rombelID: localStorage.getItem('rombel_id'),
      params: localStorage.getItem('rombel_id'),
      list_guru: [],
      isLoadJadwal: true
    };

    if (!this.state.rombelID) {
        this.props.history.push('/manajemen_sekolah/jadwal/list_jurusan');
    }
  }

  componentDidMount() {
    this.getAllData();
  }

  getAllData(){
    this.getListJadwal();
    this.dataGuru();
    this.dataRuangan();  
  }

    getListJadwal() {
        userService.getData(apiGetJadwal + this.state.params)
        .then(
            user => {
                this.setState({ list_jadwal: user.data.data, isLoadJadwal: false })
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'NPSN Tidak Di Input'
                        })
                    }
                }
            }
        );
    }

    async dataGuru() {
        await userService.getData(apiGetJadwalGuru + this.state.params)
        .then(
            user => {
                var datas = user.data.data.map(item => {
                    return(
                        { value: item.ptk_terdaftar_id, label: item.nama_pengajar }
                    )
                })
              this.setState({ list_guru: datas, isLoadingGuru: false })
            },

            error => {
                if (error.response) {
                    if(error.response.status == 502){
                        swal("502", "Kesalahan pada Server", "error");
                    }
                }
            }
        );
    }

    async getSelectGuru(value){
        this.setState({ ptk_terdaftar_id: value });

        try {
            userService.getData(apiGetJadwalMapel + value + '/' + this.state.params )
            .then(
                user => {
                    var datas = user.data.data.map(item => {
                        return(
                            { value: item.pembelajaran_id, label: item.nama_matpel }
                        )
                    })
                  this.setState({ data_mapel: datas, isLoadingRombelGuru: false })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
          }
          catch (error) {
            console.log("try Catch" +error);
          }
    }

    getSelectMapel(value){
        this.setState({ pembelajaran_id: value });
    }

    getSelectHari(value){
        this.setState({ day: value });
    }

    getSelectRuangan(value){
        this.setState({ id_ruang: value });
    }

    async dataRuangan() {
        await userService.getData(apiGetRuangan + '1')
        .then(
            user => {
                var datas = user.data.data.map(item => {
                    return(
                        { value: item.id_ruang, label: item.nm_ruang }
                    )
                })
              this.setState({ list_ruangan: datas, isLoadingRuangan: false })
            },

            error => {
                if (error.response) {
                    if(error.response.status == 502){
                        swal("502", "Kesalahan pada Server", "error");
                    }
                }
            }
        );
    }

    handleInputModal(){
        this.setState({ modalInput: true });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const {ptk_terdaftar_id, params, pembelajaran_id, day, id_ruang} = this.state;
        const start = event.target.elements.start.value;
        const end = event.target.elements.end.value;
        if(ptk_terdaftar_id != null && params != null && pembelajaran_id != null && day != null && id_ruang != null && start != '' && end != ''){
        var dataGet = {
            ptk_terdaftar_id: ptk_terdaftar_id,
            rombongan_belajar_id: params,
            pembelajaran_id: pembelajaran_id,
            day: day,
            id_ruang: id_ruang,
            start: start,
            end: end,
        }

        axios.post(apiPostJadwal, dataGet, {
          headers: {
              'Authorization' : 'token=' + this.state.currentUser.token
          }
        })

        .then(res => {
            swal("Sukses!", "Berhasil Input Jadwal", "success");
            this.setState({ modalInput: false });
            setTimeout(() => {
                this.getAllData();
            }, 1000);
        })
        .catch(error => {
            if (error.response) {
                if(error.response.status == 422){
                  swal("Gagal!", "Input ada yang salah", "error");
                }else if(error.response.status == 401){
                  swal("Gagal!", "Unauthorized", "error");
                }else if(error.response.status == 409){
                  swal("Gagal!", "Duplikat Data", "error");
                }
            }
        });
        }else{
            swal("Gagal!", "Data kurang lengkap", "error");
        }
    }

    handleDelete(value){
        swal({
            title: "Apakah Anda Yakin ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                // const formData = {
                //     jadwal_kurikulum_id: value
                // }

                axios.delete(apiDeleteJadwal + value, {
                    headers: {
                      Authorization: 'token=' + this.state.currentUser.token
                    },
                    data: {
                        jadwal_kurikulum_id: value
                    }
                })
                .then(
                    res => {
                        swal("Berhasil!", "Berhasil Hapus Jadwal", {
                            icon: "success",
                        });
                        this.getAllData();
                    },
                    error => {
                        if (error.response) {
                            if(error.response.status == 422){
                                swal("Gagal", "Input Ada yang Salah", "error");
                            }else if(error.response.status == 401){
                                swal("Gagal", "Unauthorized", "error");
                            }
                        }
                    }
                );
            }else {
              swal("Data tidak jadi dihapus");
            }
        });
    }

    handleSessData(cond){
        localStorage.removeItem('jadwal');
        var data = JSON.stringify(cond);
        localStorage.setItem('jadwal', data);
        //sessionStorage.setItem('idjurusan', cond)
    }

  render() {
    const { currentUser, users, modalInput } = this.state;
    const hari = [
        { value: 'Senin', label: 'Senin' },
        { value: 'Selasa', label: 'Selasa' },
        { value: 'Rabu', label: 'Rabu' },
        { value: 'Kamis', label: 'Kamis' },
        { value: 'Jumat', label: 'Jumat' },
        { value: 'Sabtu', label: 'Sabtu' }
    ]
    return (
      <>
        <CRow>
            <CCol sm="12" className="text-center">
                <CCard>
                    <CCardBody>
                        <h3 id="traffic" className="card-title mb-0">Jadwal Mata Pelajaran</h3>
                    </CCardBody>
                </CCard>
            </CCol>
          <CCol>
            <CCard>
              <CCardBody>
                <CRow>    
                    <CCol>
                        <CButton color="info" onClick={() => this.handleInputModal() }><FontAwesomeIcon icon={faPlus} /></CButton>
                    </CCol>
                </CRow>
                <CDataTable
                    items={this.state.list_jadwal}
                    fields={[{key: 'hari' }, {key: 'mata_pelajaran'}, {key: 'pengajar'}, {key: 'rombel'}, {key: 'ruangan'}, {key: 'jam'}, {key: 'aksi'}]}
                    hover
                    sorter
                    border
                    itemsPerPage={10}
                    pagination
                    loading={this.state.isLoadJadwal}
                    scopedSlots = {{
                        'hari':
                            (item)=>(
                            <td>
                                {item.day}
                            </td>
                        ),
                        'mata_pelajaran':
                            (item)=>(
                            <td>
                                { item.nama_matpel }
                            </td>
                        ),
                        'pengajar':
                        (item)=>(
                            <td>
                                {item.nama_pengajar}
                            </td>
                        ),
                        'rombel':
                        (item)=>(
                            <td>
                                {item.nama_rombel}
                            </td>
                        ),
                        'ruangan':
                        (item)=>(
                            <td>
                                {item.nm_ruang}
                            </td>
                        ),
                        'jam':
                        (item)=>(
                            <td>
                                {item.start} - {item.end}
                            </td>
                        ),
                        'aksi':
                        (item)=>(
                            <td>
                                <CButton style={{marginRight: '10px'}} color="success" to={{ pathname: `/manajemen_sekolah/jadwal/list_jurusan/list_rombel/list_jadwal/edit_jadwal`, edit_data: item }} onClick={() => this.handleSessData(item)}>
                                    <FontAwesomeIcon icon={faPencilAlt} />
                                </CButton>
                                <CButton color="danger" onClick={() => this.handleDelete(item.jadwal_kurikulum_id)} >
                                    <FontAwesomeIcon icon={faTrashAlt} />
                                </CButton>
                            </td>
                        ),
                    }}
                />

              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

        <CModal 
            show={modalInput} 
            onClose={() => this.setState( { modalInput: false }) }
            color="info"
            size="lg"
        >
            <CModalHeader>
                <CModalTitle>Input Jadwal</CModalTitle>
            </CModalHeader>
            <CForm onSubmit={ this.handleSubmit }>
            <CModalBody>

                <CFormGroup>
                    <CLabel htmlFor="appendedInput">Pilih Guru : </CLabel>
                    <div className="controls">
                    <Select
                        isLoading={ this.state.isLoadingGuru }
                        options={ this.state.list_guru }
                        isSearchable={false}
                        placeholder="Pilih Guru..."
                        onChange={(value) => this.getSelectGuru(value.value)}
                    />
                    </div>
                </CFormGroup>

                <CFormGroup>
                    <CLabel htmlFor="appendedInput">Pilih Mata Pelajaran : </CLabel>
                    <div className="controls">
                    <Select
                        isLoading={ this.state.isLoadingGuru }
                        isSearchable={false}
                        options={ this.state.data_mapel }
                        placeholder="Pilih Mata Pelajaran..."
                        onChange={(value) => this.getSelectMapel(value.value)}
                    />
                    </div>
                </CFormGroup>

                <CFormGroup>
                    <CLabel htmlFor="appendedInput">Pilih Hari : </CLabel>
                    <div className="controls">
                    <Select
                        isLoading={ this.state.isLoadingGuru }
                        isSearchable={false}
                        options={ hari }
                        placeholder="Pilih Hari..."
                        onChange={(value) => this.getSelectHari(value.value)}
                    />
                    </div>
                </CFormGroup>

                <CFormGroup>
                    <CLabel htmlFor="appendedInput">Pilih Ruangan : </CLabel>
                    <div className="controls">
                    <Select
                        isLoading={ this.state.isLoadingRuangan }
                        options={ this.state.list_ruangan }
                        placeholder="Pilih Ruangan..."
                        onChange={(value) => this.getSelectRuangan(value.value)}
                    />
                    </div>
                </CFormGroup>

                <CFormGroup>
                    <CLabel htmlFor="appendedInput">Jam Mulai : </CLabel>
                    <div className="controls">
                        <CInputGroup>
                            <CInput type="text" id="start" name="start" placeholder="example: 10:00:00" />
                        </CInputGroup>
                    </div>
                </CFormGroup>

                <CFormGroup>
                    <CLabel htmlFor="appendedInput">Jam Selesai : </CLabel>
                    <div className="controls">
                        <CInputGroup>
                            <CInput type="text" id="end" name="end" placeholder="example: 12:00:00" />
                        </CInputGroup>
                    </div>
                </CFormGroup>
                    
            </CModalBody>
            <CModalFooter>
                <CButton color="danger" onClick={() => this.setState( { modalInput: false }) }>Tutup</CButton>
                <CButton color="success" type="submit">Simpan</CButton>
            </CModalFooter>
            </CForm>
        </CModal>
      </>
    )
  }
}

export default ListRombel
