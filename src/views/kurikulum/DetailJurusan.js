import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CAlert,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CSelect,
  CInput,
  CLabel
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { history } from '../../_helpers';
import { apiJurusan } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'

class DetailJurusan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue,
            params: this.props.location.query,
            paramsName: this.props.location.name,
            jurusan_induk: [],
            jurusan_subinduk: [],
            statusRes: false
        };

        if (!this.props.location.query) {
            this.props.history.push('/manajemen_sekolah/jurusan');
        }
    }

    UNSAFE_componentWillMount() {
        this.getRevJurusan();
    }

    async getRevJurusan() {
        await axios.get(apiJurusan + '2&jurusan_induk=' + this.state.params)
            .then(response => {
                this.setState({ jurusan_subinduk: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            });
    }

    render() {
        const { currentUser } = this.state;
        return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <h3 id="traffic" className="card-title mb-0 text-center">Update Jurusan</h3>
                        </CCardHeader>
                        <CCardBody>
                            <table className="table table-striped">
                                <tbody>
                                <tr>
                                    <td>Id Jurusan</td>
                                    <td>:</td>
                                    <td>{this.state.params}</td>
                                </tr>
                                <tr>
                                    <td>Nama Jurusan</td>
                                    <td>:</td>
                                    <td>{this.state.paramsName}</td>
                                </tr>
                                </tbody>
                            </table>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="12">
                    <CCard>
                    <CForm>
                        <CCardBody>
                            <CFormGroup className="pr-1" row>
                                <CCol md="12">
                                    <CSelect custom name="jurusan_induk" id="ccyear">
                                            <option value={this.state.params}>
                                                {this.state.paramsName}
                                            </option>
                                    </CSelect>
                                </CCol>
                            </CFormGroup>
                            <CFormGroup className="pr-1" row>
                                <CCol md="1" className="text-sm-left text-md-right">
                                    <MdSubdirectoryArrowRight size="2em"/>
                                </CCol>
                                <CCol md="11">
                                    <CSelect custom name="jurusan_subinduk" id="ccyear">
                                        {this.state.jurusan_subinduk.map((item, i) => {
                                            return(
                                                <option key={i} value={item.jurusan_id}>
                                                {item.nama_jurusan}
                                                </option>
                                            )
                                        })}
                                    </CSelect>
                                </CCol>
                            </CFormGroup>
                            <CFormGroup>
                                
                            </CFormGroup>
                        </CCardBody>
                        <CCardFooter>
                        <CButton type="submit" size="sm" color="primary"><CIcon name="cil-scrubber" /> Submit</CButton>
                        </CCardFooter>
                    </CForm>
                    </CCard>
                </CCol>
            </CRow>
        </>
        )
    }
}

export default DetailJurusan
