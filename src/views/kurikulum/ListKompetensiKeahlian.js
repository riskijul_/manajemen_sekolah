import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CLabel,
  CSelect,
  CFormGroup,
  CDataTable,
  CInput,
  CForm,
  CAlert,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import Select from 'react-select';
import { apiRombelList, apiPostRombel, apiGetSemester, apiGetWaliKelas, apiPostWaliKelas } from '../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios'
import swal from 'sweetalert';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
  ];

class ListKompetensiKeahlian extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      users: null,
      paramsName: this.props.location.name,
      params: this.props.location.query,
      data_kompetensi: [],
      isLoadData: true,
      idJurusan: localStorage.getItem('idjurusan'),
      conditionAlert: '',
      statusRes: false,
      messageRes: '',
      modalWalikelas: false,
      rombel_id: '',
      dataWali: [],
      isLoadDataWali: true,
      ptk_IdConst: null
    };

    if (!localStorage.getItem('idjurusan')) {
        this.props.history.push('/manajemen_sekolah/pengelolaan');
    }

    //console.log(this.state.currentUser.token)
  }

  componentDidMount() {
    this.getSemester()
    this.getRombongan()
  }

  async getRombongan(){
    try {
      await userService.getData(apiRombelList + this.state.idJurusan + '&semester_id=20201')//sessionStorage.getItem('idjurusan');this.props.location.query
      .then(
        user => {
          this.setState({ data_kompetensi: user.data.data, isLoadData: false })
        },
        error => {
          if (error.response) {
            if(error.response.status == 422){
                this.setState({
                  statusRes: true,
                  conditionAlert: 'danger',
                  messageRes: error.data.message,
                  modalWalikelas: false
                })
            }
          }
      });
    }catch (error) {
      console.log("try Catch" +error);
    }
  }

  handleClickWalikelas(value){
    this.setState({ modalWalikelas: true, rombel_id: value})
      userService.getData(apiGetWaliKelas)
        .then(
          user => {
            var datas = user.data.data.map(item => {
              return(
                { value: item.ptk_id, label: item.nama }
              )
            })
            this.setState({ dataWali: datas, isLoadDataWali: false })
        },
          error => {
            if (error.response) {
              if(error.response.status == 422){
                this.setState({ 
                  statusRes: true,
                  conditionAlert: 'danger',
                  messageRes: 'NPSN Tidak Di Input'
                })
              }
            }
          }
      )
  }

  getList(value){
    try {
      userService.getData(apiRombelList + this.state.idJurusan + '&semester_id=' + value)
      .then(
          user => {
            this.setState({ data_kompetensi: user.data.data, isLoadData: false })
          },
          error => {
              if (error.response) {
                  if(error.response.status == 422){
                      this.setState({ statusRes: true })
                  }
              }
          }
      );
    }catch (error) {
      console.log("try Catch" +error);
    }
  }

  getSemester(){
    try {
      userService.getData(apiGetSemester)//sessionStorage.getItem('idjurusan');this.props.location.query
      .then(
        user => {
          var datas = user.data.data.map(item => {
              return(
                { value: item, label: item.substring(0, 4) + ' Smt. ' + item.substring(4, 5)}
              )
            })
          this.setState({ data_semester: datas })
        },
        error => {
          if (error.response) {
            if(error.response.status == 422){
                this.setState({ statusRes: true })
            }
          }
        });
    }catch (error) {
      console.log("try Catch" +error);
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    try{
      var data = {
        rombongan_belajar_id: this.state.rombel_id,
        ptk_id: this.state.ptk_IdConst
      }
      userService.postData(apiPostWaliKelas, data)
      .then(
        user => {
          swal("Sukses!", "Berhasil Mengubah Wali Kelas", "success");
          this.setState({ modalWalikelas: false });
          this.getRombongan();
        },
        error => {
          if (error.response) {
              swal("Gagal!", "Gagal Mengubah Wali Kelas", "error");
              this.setState({ modalWalikelas: false });
              this.getRombongan();
          }
        }
      )
    }catch(error){
      console.log(error)
    }
  }

  render() {
    const { currentUser, users, modalWalikelas } = this.state;
    return (
      <>
        <CRow>
            <CCol sm="12">
                <CCard>
                <CCardHeader>
                <CRow>
                    <CCol sm="9">
                      <h4>List Kompetensi Keahlian</h4>
                    </CCol>
                    <CCol sm="3" className="text-lg-right">
                      <Select
                        options={this.state.data_semester}
                        placeholder="Pilih Semester"
                        onChange={(value) => this.getList(value.value)}
                      />
                    </CCol>
                  </CRow>
                </CCardHeader>
                <CCardBody>
                  <CAlert color={this.state.conditionAlert} show={this.state.statusRes}>
                    {this.state.messageRes}
                  </CAlert>
                  <CDataTable
                      items={this.state.data_kompetensi}
                      fields={[{ key: 'tingkat_pendidikan' }, {key: 'nama' }, { key: 'semester' }, { key: 'status_wali_kelas' }, { key: 'aksi' }, ]}
                      hover
                      border
                      hover
                      sorter
                      itemsPerPage={10}
                      pagination
                      loading={this.state.isLoadData}
                      scopedSlots = {{
                      'tingkat_pendidikan':
                          (item)=>(
                          <td>
                              {item.tingkat_pendidikan_id}
                          </td>
                          )
                      ,
                      'semester':
                          (item)=>(
                          <td>
                              {item.semester_id.substring(0, 4) + ' Smt. ' + item.semester_id.substring(4, 5)}
                          </td>
                          )
                      ,
                      'status_wali_kelas':
                        (item)=>(
                        <td>
                            {item.ptk_id != null ? item.nama_guru : <>Belum Ada</>}
                        </td>
                        ),
                      'aksi':
                          (item)=>(
                          <td>
                              <CButton color="success" onClick={() => this.handleClickWalikelas(item.rombongan_belajar_id)}>Ubah</CButton>
                          </td>
                          )
                      }}
                  />
                </CCardBody>
                </CCard>
            </CCol>
        </CRow>

        <CModal 
            show={modalWalikelas} 
            onClose={() => this.setState({ modalWalikelas: false})}
            color="success"
        >
            <CModalHeader>
                <CModalTitle>Ubah Wali Kelas</CModalTitle>
            </CModalHeader>
            <CForm onSubmit={this.handleSubmit}>
              <CModalBody>
                    <CFormGroup>
                        <Select
                            className="basic-single"
                            placeholder="Pilih Wali Kelas..."
                            isLoading={this.state.isLoadDataWali}
                            isSearchable={true}
                            name="ptkId"
                            options={this.state.dataWali}
                            onChange={(value) => this.setState({ ptk_IdConst : value.value })}
                        />
                    </CFormGroup>
              </CModalBody>
              <CModalFooter>
                  <CButton color="danger" onClick={() => this.setState({ modalWalikelas: false })}>Cancel</CButton>
                  <CButton color="success" type="submit">Simpan</CButton>{' '}
              </CModalFooter>
            </CForm>
        </CModal>
      </>
    )
  }
}

export default ListKompetensiKeahlian