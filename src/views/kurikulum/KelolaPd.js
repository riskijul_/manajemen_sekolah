import React, { lazy, useState} from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CForm,
  CFormGroup,
  CFormText,
  CInputCheckbox,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { Route, Link } from 'react-router-dom';
import { apiJurusan } from '../../_components';
import { history } from '../../_helpers';
// import MainChartExample from '../charts/MainChartExample.js'
import axios from 'axios'

const usersData = [
    {id: 0, nama_siswa: 'Ahmad Rivaiy', tingkat: '10', jurusan: 'RPL', status: 'Naik Kelas', status_naik: '1'},
    {id: 1, nama_siswa: 'Udin', tingkat: '10', jurusan: 'RPL', status: 'Tinggal Kelas', status_naik: '0'}
  ]

const fields = ['nama_siswa', 'tingkat', 'jurusan', 'status', 'Action']

class KelolaPD extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        currentUser: authenticationService.currentUserValue.data,
        users: null,
        jurusanApi: apiJurusan,
        data_jurusan: [],
        statusRes: false,
        modalInfo: false,
        pesdik_id: []
    };
    
    //this.handleInputChange = this.handleInputChange.bind(this);

  }

  componentDidMount() {
    // userService.getAll().then(users => this.setState({ users }));
    // try {
    //     axios.get(this.state.jurusanApi + '20219175')
    //     .then(response => {
    //         this.setState({ data_jurusan: response.data.data })
    //         //console.log(response);
    //     })
    //     .catch(error => {
    //         if (error.response) {
    //             if(error.response.status == 422){
    //                 this.setState({ statusRes: true })
    //             }
    //         }
    //     });
    //   } catch (error) {
    //     console.log("try Catch" +error);
    //   }
  }

  handleInputChange = checked => {
    const target = checked.target;
    var value = target.value;
    
    if(target.checked){
        this.state.pesdik_id[value] = value;   
    }else{
        this.state.pesdik_id.splice(value, 1);
    }
    console.log(value)
  }

  handleChecked(){
    this.setState({ modalInfo: true})
  }

  render() {
    const { currentUser, users, modalInfo } = this.state;
    return (
      <>
        <CRow>
            <CCol sm="12" className="text-center">
                <CCard>
                    <CCardBody>
                        <h3 id="traffic" className="card-title mb-0">Kelola Peserta Didik</h3>
                    </CCardBody>
                </CCard>
            </CCol>
            <CCol sm="12">
                <CCard>
                    <CForm>
                        <CCardHeader className="text-right">
                            <CButton className="btn btn-info" onClick={() => this.handleChecked()}>
                                Submit
                            </CButton>
                        </CCardHeader>
                        <CCardBody>
                        <CAlert color="primary" show={this.state.statusRes} closeButton>
                            NSPN Tidak Di Input
                        </CAlert>
                            <CDataTable
                                items={usersData}
                                fields={fields}
                                itemsPerPage={10}
                                pagination
                                scopedSlots = {{
                                    'Nama_Jurusan':
                                    (item)=>(
                                        <td>
                                            {item.nama_jurusan_sp}
                                        </td>
                                    ),
                                    'Action':
                                    (item)=>(
                                        <td>
                                            <CFormGroup className="checkbox text-center">
                                                <CInputCheckbox id="checkbox" name="id_pd" value={item.id} onChange={this.handleInputChange} sync="true"/>
                                            </CFormGroup>
                                        </td>
                                    )
                                }}
                            />
                        </CCardBody>
                    </CForm>
                </CCard>
            </CCol>
        </CRow>

        <CModal 
              show={modalInfo} 
              onClose={() => !this.state.modalInfo}
              color="info"
            >
              <CModalHeader closeButton>
                <CModalTitle>List Siswa Yang Akan Naik Kelas</CModalTitle>
              </CModalHeader>
              <CModalBody>
                    <CRow>
                        Dengan Id Siswa:
                        {this.state.pesdik_id.map((item, i) => {
                            return(
                                <CCol key={i} className="col-md-12">
                                    {item}
                                </CCol>
                            )
                        })}
                    </CRow>
              </CModalBody>
              <CModalFooter>
                <CButton color="secondary" onClick={() => this.setState({ modalInfo: false })}>Cancel</CButton>
                <CButton color="info" onClick={() => this.setState({ modalInfo: true })}>Konfirmasi</CButton>{' '}
              </CModalFooter>
            </CModal>
      </>
    )
  }
}

export default KelolaPD
