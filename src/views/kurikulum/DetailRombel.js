import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CInputRadio,
  CLabel,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCollapse,
  CFade,
  CInput,
  CSelect,
  CBadge
} from '@coreui/react'
import { userService, authenticationService } from '../../_services';
import { apiRombelDetail, apiRombelList, apiPostRombel, apiCheckEmpatThn, apiGetSemester, apiGetPdNaik, apiPatchPd } from '../../_components';
// import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';

class DetailRombel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            params: this.props.location.query,
            paramsName: this.props.location.name,
            tp_id: this.props.location.tp_id,
            smstr_id: this.props.location.smstr_id,
            arrSmstr: [],
            idJurusan: localStorage.getItem('idjurusan'),
            statusRes: false,
            modalWali: false,
            isLoadData: true,
            list_pd: [],
            pesdik_id: [],
            pesdik_name: [],
            row_id: [],
            modalInfo: false,
            collapseOne: true,
            collapseTwo: false,
            opsiChange: '1',
            conditionAlert: '',
            messageRes: '',
            valueNameClass:'',
            selectedRows: [],
            condEmpatThn: '',
            collapseNon: false,
            tp_idNaik: parseInt(this.props.location.tp_id) + 1,
            rombelNaik: [],
            jurusanIdNaik: null,
            PdArr: []
        };

        if (!this.props.location.query) {
            this.props.history.push('/manajemen_sekolah/pengelolaan');
        }
        //console.log(this.state.arrSmstr)
    }

    componentDidMount() {
        this.getRevJurusan();
        this.checkEmpatTahun();
        this.getSmstr();
    }

    componentWillUnmount(){
        //this.getRevJurusan();
    }

    async getRevJurusan() {
        await axios.get(apiRombelDetail + this.state.params, {
            headers: {
                'Authorization' : 'token=' + this.state.currentUser.token
              }
            })
            .then(response => {
                this.setState({ list_pd: response.data.data, isLoadData: false, jurusanIdNaik: response.data.data[0].jurusan_id})
                this.getRombelNaikNOW(response.data.data[0].jurusan_id);
            })
            .catch(error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'NPSN Tidak Di Input'
                        })
                    }
                }
            });
    }

    async checkEmpatTahun() {
        await axios.get(apiCheckEmpatThn + this.state.idJurusan, {
            headers: {
                'Authorization' : 'token=' + this.state.currentUser.token
              }
        })
            .then(response => {
                this.setState({ condEmpatThn: response.data.data.empat_tahun })
            })
            .catch(error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'NPSN Tidak Di Input'
                        })
                    }
                }
            });
    }

    async getSmstr() {
        await userService.getData(apiGetSemester)
            .then(
                user => {
                    this.setState({ arrSmstr: user.data.data })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ 
                                statusRes: true,
                                conditionAlert: 'danger',
                                messageRes: 'NPSN Tidak Di Input'
                            })
                        }
                    }
                }
            );
    }

    handleInputChange = checked => {
        const target = checked.target;
        const id = target.getAttribute("data-valuethree");
        var value = target.value;
        var valuetwo = target.getAttribute("data-valuetwo");

        try{
            if(target.checked){
                this.setState({ pesdik_id: [...this.state.pesdik_id, value], pesdik_name: [...this.state.pesdik_name, valuetwo], row_id: [...this.state.row_id, id] })
            }else{
                //this.state.pesdik_id.splice(value, 1);
                this.removeItem(this.state.pesdik_id, value)
                this.removeItemName(this.state.pesdik_name, valuetwo)
                this.removeRowId(this.state.row_id, id)
            }
        }catch(error){
            console.log(error)
        }
    }

    removeItem(array, item){
        for(var i in array){
            if(array[i]==item){
                array.splice(i,1);
                break;
            }
        }
    }

    removeItemName(array, item){
        for(var i in array){
            if(array[i]==item){
                array.splice(i,1);
                break;
            }
        }
    }

    removeRowId(array, item){
        for(var i in array){
            if(array[i]==item){
                array.splice(i,1);
                break;
            }
        }
    }

    handleChange = (state) => {
        // You can use setState or dispatch with something like Redux so we can use the retrieved data
        this.setState({ selectedRows: state.selectedRows });
    };

    handleOpsiNaik = checked => {
        const targetCheck = checked.target.value;
        
        if(targetCheck == '1'){
            var tp_idNew = parseInt(this.state.tp_id) + 1
            this.setState({ opsiChange: '1', collapseOne: false, collapseTwo: true})
            this.getRombelNaik(tp_idNew)
        }else{
            var tp_idNew = parseInt(this.state.tp_idNaik) - 1
            this.setState({ opsiChange: '2', collapseOne: false, collapseTwo: true})
            this.getRombelNaik(tp_idNew)
        }
    }

    getRombelNaikNOW(jurusanIdNaik) {
        userService.getData(apiGetPdNaik + jurusanIdNaik + '&tingkat_pendidikan_id=' + this.state.tp_idNaik)
            .then(
            user => {
                if(Object.keys(user.data.data).length != 0){
                    // var dataq = user.data.data.map(item => {
                    //     return(
                    //         { value: item.jurusan_sp_id, label: item.nama }
                    //     )
                    // })
                    this.setState({ rombelNaik: user.data.data })
                }else{
                    var dataw = [{ value: null, label: 'Tidak Ada Kelas' }]
                    this.setState({ rombelNaik: dataw })
                }
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'NPSN Tidak Di Input'
                        })
                    }
                }
            }
        );
    }

    getRombelNaik(tpId) {
        userService.getData(apiGetPdNaik + this.state.jurusanIdNaik + '&tingkat_pendidikan_id=' + tpId)
            .then(
            user => {
                if(Object.keys(user.data.data).length != 0){
                    // var dataq = user.data.data.map(item => {
                    //     return(
                    //         { value: item.jurusan_sp_id, label: item.nama }
                    //     )
                    // })
                    this.setState({ rombelNaik: user.data.data })
                }else{
                    var dataw = [{ value: null, label: 'Tidak Ada Kelas' }]
                    this.setState({ rombelNaik: dataw })
                }
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'NPSN Tidak Di Input'
                        })
                    }
                }
            }
        );
    }

    handleInputKelas = val => {
        const targetCheck = val.target.value;
        this.setState({ valueNameClass: targetCheck })
    }
    
    handleChecked(){
        const PdArr = this.state.selectedRows.map((item, i) => {
                return(
                    item.peserta_didik_id
                )
            })

        this.setState({ modalInfo: true, PdIdArr: PdArr})
    }

    handlePage = () => {
        this.setState({ pesdik_name: [], pesdik_id: []})
    }

    handleSubmit = (event) =>{
        event.preventDefault();
        try{//{rombongan_belajar_id, peserta_didik_id} (Ahmed)
            const data = {
                rombongan_belajar_id: event.target.elements.rombel.value,
                peserta_didik_id: this.state.PdIdArr
            }

            console.log(data)
            axios.patch(apiPatchPd, data, {
                headers: {
                    'Authorization' : 'token=' + this.state.currentUser.token
                  }
                })
            .then(response => {
                // if(response.response){
                //     if([201, 200].includes(response.response.status)){
                //         this.getRevJurusan()
                //         this.setState({ modalInfo: false })
                //     }
                // }
                //console.log(response);
                swal("Sukses!", "Siswa Naik Kelas", "success");
                this.getRevJurusan()
                this.setState({ modalInfo: false })
            })
            .catch(error => {
                swal("Gagal!", "Gagal Tambah Siswa Baru", "error");
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: 'NPSN Tidak Di Input'
                         })
                    }else{
                        this.setState({ 
                            statusRes: true,
                            conditionAlert: 'danger',
                            messageRes: error.response.data.message,
                            modalInfo: false
                         })
                    }
                }
            });
        }catch(error){
            console.log('Err Submit : ' + error)
        }
    }

    render() {
        const { modalInfo, collapseOne, collapseTwo, modalWali, collapseNon} = this.state;
        
        return (
            <>
            <CRow>
                <CCol sm="12" className="text-center">
                    <CCard>
                        <CCardBody >
                            <h3 id="traffic" className="card-title mb-0">Kelola Peserta Didik</h3>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="12">
                    <CCard>
                        <CForm>
                            <CCardHeader className="text-right">
                                {this.state.arrSmstr[2] == this.state.smstr_id ? 
                                <>
                                    <CButton color="info" onClick={() => this.handleChecked()} >
                                        Naikan Siswa?
                                    </CButton>
                                </>
                                :
                                <>

                                </>
                                }
                            </CCardHeader>
                            <CCardBody>
                            <CAlert color={this.state.conditionAlert} show={this.state.statusRes} closeButton>
                                {this.state.messageRes}
                            </CAlert>
                                <DataTable
                                    columns={[
                                        {
                                            name: 'Nama',
                                            selector: 'nama',
                                            sortable: true,
                                        },
                                        {
                                            name: 'Nama Rombel',
                                            selector: 'nama_rombel',
                                            sortable: true
                                        },
                                        {
                                            name: 'Status',
                                            cell: row => <div>{row.soft_delete_anggota == '1' ? <><CBadge color="info">Sudah Naik Kelas</CBadge></> : <><CBadge color="danger">Belum Naik Kelas</CBadge></>}</div>,
                                        }
                                    ]}
                                    data={this.state.list_pd}
                                    Clicked
                                    pagination
                                    striped
                                    paginationPerPage={10}
                                    responsive
                                    onSelectedRowsChange={this.handleChange}
                                    selectableRows
                                />
                            </CCardBody>
                        </CForm>
                    </CCard>
                </CCol>
            </CRow>
    
            <CModal 
                show={modalInfo} 
                onClose={() => this.setState({ modalInfo: false})}
                color="info"
                >
                <CForm onSubmit={this.handleSubmit}>
                    <CModalHeader>
                        <CModalTitle>Siswa yang dipilih akan : </CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                            {this.state.tp_id == '12' ?
                                this.state.condEmpatThn == '1' ?
                                    <>
                                        <CFormGroup variant="custom-radio">
                                            <CInputRadio className="form-check-input" id="option1" name="radios" value="1" defaultChecked={true} onChange={this.handleOpsiNaik}/>
                                            <CLabel htmlFor="radio1">Naik Kelas</CLabel>
                                        </CFormGroup>
                                        <CFormGroup variant="custom-radio">
                                            <CInputRadio className="form-check-input" id="option2" name="radios" value="2" onChange={this.handleOpsiNaik}/>
                                            <CLabel htmlFor="radio2">Tidak Naik Kelas</CLabel>
                                        </CFormGroup>
                                    </> :
                                    <>
                                        <CFormGroup variant="custom-radio">
                                            <CInputRadio className="form-check-input" id="option1" name="radios" value="1" defaultChecked={true} onChange={this.handleOpsiNaik, () => this.setState({ collapseNon: false })}/>
                                            <CLabel htmlFor="radio1">Lulus</CLabel>
                                        </CFormGroup>
                                        <CFormGroup variant="custom-radio">
                                            <CInputRadio className="form-check-input" id="option2" name="radios" value="2" onChange={this.handleOpsiNaik, () => this.setState({ collapseNon: true })}/>
                                            <CLabel htmlFor="radio2">Tidak Lulus</CLabel>
                                        </CFormGroup>
                                    </>
                                : this.state.tp_id == '11' ?
                                <>
                                    <CFormGroup variant="custom-radio">
                                        <CInputRadio className="form-check-input" id="option1" name="radios" value="1" defaultChecked={true} onChange={this.handleOpsiNaik}/>
                                        <CLabel htmlFor="radio1">Naik Kelas</CLabel>
                                    </CFormGroup>
                                    <CFormGroup variant="custom-radio">
                                        <CInputRadio className="form-check-input" id="option2" name="radios" value="2" onChange={this.handleOpsiNaik}/>
                                        <CLabel htmlFor="radio2">Tidak Naik Kelas</CLabel>
                                    </CFormGroup>
                                </>
                                : this.state.tp_id == '10' ?
                                <>
                                    <CFormGroup variant="custom-radio">
                                        <CInputRadio className="form-check-input" id="option1" name="radios" value="1" defaultChecked={true} onChange={this.handleOpsiNaik}/>
                                        <CLabel htmlFor="radio1">Naik Kelas</CLabel>
                                    </CFormGroup>
                                    <CFormGroup variant="custom-radio">
                                        <CInputRadio className="form-check-input" id="option2" name="radios" value="2" onChange={this.handleOpsiNaik}/>
                                        <CLabel htmlFor="radio2">Tidak Naik Kelas</CLabel>
                                    </CFormGroup>
                                </>
                                :
                                <>
                                    <CFormGroup variant="custom-radio">
                                        <CInputRadio className="form-check-input" id="option1" name="radios" value="1" defaultChecked={true} onChange={this.handleOpsiNaik, () => this.setState({ collapseNon: false })}/>
                                        <CLabel htmlFor="radio1">Lulus</CLabel>
                                    </CFormGroup>
                                    <CFormGroup variant="custom-radio">
                                        <CInputRadio className="form-check-input" id="option2" name="radios" value="2" onChange={this.handleOpsiNaik, () => this.setState({ collapseNon: true })}/>
                                        <CLabel htmlFor="radio2">Tidak Lulus</CLabel>
                                    </CFormGroup>
                                </>
                            }
                            <CCard>
                                <CCardBody style={{ overflowY: 'scroll', height: 200}}>
                                    <ul>
                                    {this.state.selectedRows.map((item, i) => {
                                            return(
                                                <li key={i}>
                                                    {item.nama}
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </CCardBody>
                            </CCard>
                            {this.state.tp_id == '12' ?
                                this.state.condEmpatThn == '1' ?
                                    <>
                                        <hr></hr>
                                        <CFormGroup style={{ marginTop: 10 }}>
                                        <CLabel>Pilih Rombel :</CLabel>
                                        <CSelect custom name="rombel" id="rombel" required>
                                            {this.state.rombelNaik.map((item, i) => {
                                                return(
                                                    <option value={item.rombongan_belajar_id} key={i}>
                                                        {item.nama}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                        </CFormGroup>
                                    </>
                                :
                                    <>

                                    </>
                            : this.state.tp_id == '11' ?
                                <>
                                    <hr></hr>
                                    <CFormGroup style={{ marginTop: 10 }}>
                                    <CLabel>Pilih Rombel :</CLabel>
                                    <CSelect custom name="rombel" id="rombel" required>
                                        {this.state.rombelNaik.map((item, i) => {
                                            return(
                                                <option value={item.rombongan_belajar_id} key={i}>
                                                    {item.nama}
                                                </option>
                                            )
                                        })}
                                    </CSelect>
                                    </CFormGroup>
                                </>
                            : this.state.tp_id == '10' ?
                                <>
                                    <hr></hr>
                                    <CFormGroup style={{ marginTop: 10 }}>
                                    <CLabel>Pilih Rombel : </CLabel>
                                    <CSelect custom name="rombel" id="rombel" required>
                                        {this.state.rombelNaik.map((item, i) => {
                                            return(
                                                <option value={item.rombongan_belajar_id} key={i}>
                                                    {item.nama}
                                                </option>
                                            )
                                        })}
                                    </CSelect>
                                    </CFormGroup>
                                </>
                            :
                            <>

                            </>
                            }

                            <CCollapse show={collapseNon}>
                            {collapseNon ?
                                <>
                                    <hr></hr>
                                    <CFormGroup>
                                        <CLabel>Pilih Rombel :</CLabel>
                                            <CSelect custom name="rombel" id="rombel" required>
                                                {this.state.rombelNaik.map((item, i) => {
                                                    return(
                                                        <option value={item.rombongan_belajar_id} key={i}>
                                                            {item.nama}
                                                        </option>
                                                    )
                                                })}
                                            </CSelect>
                                    </CFormGroup>
                                </>
                                :
                                <></>
                            }
                            </CCollapse>     
                    </CModalBody>
                    <CModalFooter>
                        <CButton color="secondary" onClick={() => this.setState({ modalInfo: false })}>Cancel</CButton>
                        <CButton color="info" type="submit">Konfirmasi</CButton>
                    </CModalFooter>
                </CForm>
            </CModal>
          </>
        )
    }
}

export default DetailRombel
