import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CAlert,
  CCollapse,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { Link } from 'react-router-dom';
import { history } from '../../_helpers';
import { apiRombel, apiJurusan } from '../../_components';
// import usersData from '../users/UsersData'

class Pengelolaan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      users: null,
      statusRes: false,
      rombel_api: apiRombel,
      bidang_keahlian: [],
      program_keahlian: [],
      details: [],
      setDetails: [],
      isLoadData: true
    };
  }

  componentDidMount() {
    try {
        userService.getData(apiJurusan + '1')
        .then(
            user => {
                this.setState({ bidang_keahlian: user.data.data, isLoadData: false })
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            }
        );
    } catch (error) {
      console.log("try Catch" +error);
    }
  }

  getProgramKeahlian(cond){
    try {
        userService.getData(apiJurusan + '2&jurusan_induk=' + cond)
        .then(
            user => {
                this.setState({ program_keahlian: user.data.data })
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            }
        );
    } catch (error) {
        console.log("try Catch" +error);
    }
}

toggleDetails(index, jurusan_id) {
    const position = this.state.setDetails.indexOf(index)
    let newDetails = this.state.details.slice()

    if (position != -1) {
        newDetails.splice(position, 1)
    } else {
        newDetails = [...this.state.details, index]
        this.getProgramKeahlian(jurusan_id);
    }

    this.setState({ setDetails: newDetails })
}

handleSessData(cond){
  localStorage.removeItem('idjurusan');
  localStorage.setItem('idjurusan', cond);
  //sessionStorage.setItem('idjurusan', cond)
}

  render() {
    const { currentUser, users, setDetails } = this.state;
    return (
      <>
        <CRow>
            <CCol sm="12" className="text-center">
                <CCard>
                    <CCardBody>
                        <h3 id="traffic" className="card-title mb-0">Pengelolaan Peserta Didik</h3>
                    </CCardBody>
                </CCard>
            </CCol>
            <CCol sm="12">
                <CCard>
                    <CCardBody>
                    <CAlert color="primary" show={this.state.statusRes} closeButton>
                        NSPN Tidak Di Input
                    </CAlert>
                        <CDataTable
                            items={this.state.bidang_keahlian}
                            fields={[{ key: 'Nama_Jurusan' }, { key: 'show_details', label: '', sorter: false }]}
                            pagination
                            hover
                            loading={this.state.isLoadData}
                            itemsPerPage={10}
                            scopedSlots = {{
                                'show_details':
                                (item, index)=>{
                                    return (
                                        <td className="py-2">
                                            <CButton
                                                color="success"
                                                variant="outline"
                                                size="sm"
                                                style={{ marginRight: "10px" }}
                                                to={{pathname: `/manajemen_sekolah/pengelolaan/list_rombel`, query: item.jurusan_id, name: item.nama_jurusan}}
                                                onClick={() => this.handleSessData(item.jurusan_id)}
                                            >
                                                Kelas 10
                                            </CButton>
                                            <CButton
                                                color="info"
                                                variant="outline"
                                                size="sm"
                                                onClick={() => this.toggleDetails(index, item.jurusan_id)}
                                                >
                                                {setDetails.includes(index) ? 'Tutup' : 'Kelas 11, 12 dan 13'}
                                            </CButton>
                                        </td>
                                    )
                                },
                                'details':
                                    (item, index)=>{
                                    return (
                                        <CCollapse show={setDetails.includes(index)}>
                                            <CCardBody>
                                              <table>
                                                <tbody>
                                                    {this.state.program_keahlian.map((itema, i) => {
                                                        return(
                                                          <tr key={i}>
                                                            <td>-</td>
                                                            <td>
                                                              <h6>{itema.nama_jurusan}</h6>
                                                            </td>
                                                            <td>
                                                        <h6><b>[ <Link to={{pathname: `/manajemen_sekolah/pengelolaan/list_rombel`, query: itema.jurusan_id, name: itema.nama_jurusan}} onClick={() => this.handleSessData(itema.jurusan_id)}>PILIH</Link> ]</b></h6>
                                                            </td>
                                                        </tr>
                                                        )
                                                    })}
                                                </tbody>
                                              </table>
                                            {/* <p className="text-muted">User since: </p>
                                            <CButton size="sm" color="info">
                                                User Settings
                                            </CButton>
                                            <CButton size="sm" color="danger" className="ml-1">
                                                Delete
                                            </CButton> */}
                                            </CCardBody>
                                        </CCollapse>
                                    )
                                },
                                'Nama_Jurusan':
                                (item)=>(
                                    <td>
                                        <CCol className="col-sm-12">
                                            {item.nama_jurusan}
                                        </CCol>
                                    </td>
                                )
                            }}
                        />
                    </CCardBody>
                </CCard>
            </CCol>
        </CRow>
      </>
    )
  }
}

export default Pengelolaan
