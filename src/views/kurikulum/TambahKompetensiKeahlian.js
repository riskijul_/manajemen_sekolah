import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CAlert,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CForm,
  CFormGroup,
  CFormText,
  CInputFile,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CSelect,
  CInput,
  CLabel,
  CInputGroup
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { history } from '../../_helpers';
import { apiJurusanSp, apiPostJurusan } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'

class TambahKompetensiKeahlian extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            params: this.props.location.query,
            paramsName: this.props.location.name,
            currentUser: authenticationService.currentUserValue.data,
            jurusan_induk: [],
            jurusan_subinduk: [],
            jurusan_subsubinduk: [],
            statusRes: false,
            idJurusanFinal: null,
            jurusanId: null,
            file: null,
            conditionAlert: 'success',
            messageRes: null,
            valueSK: null,
            fileName: 'Ambil File...'
        };

        if (!this.props.location.query) {
            this.props.history.push('/manajemen_sekolah/jurusan');
        }
    }

    UNSAFE_componentWillMount() {
        // this.getRevJurusan();
        this.handleChangeSubSub();
    }

    // async getRevJurusan() {
    //     await axios.get(apiJurusanSp, {
    //         headers: {
    //             'Authorization' : 'token=' + this.state.currentUser.token
    //           }
    //         })
    //         .then(response => {
    //             this.setState({ jurusan_induk: response.data.data })
    //         })
    //         .catch(error => {
    //             if (error.response) {
    //                 if(error.response.status == 422){
    //                     this.setState({ statusRes: true })
    //                 }
    //             }
    //         });
    // }

    handleChangeSub = value => {
        this.setState({ jurusan_subinduk: [], jurusan_subsubinduk: [] })
        axios.get(apiJurusanSp + '?jurusan_induk=' + value.target.value, {
            headers: {
                'Authorization' : 'token=' + this.state.currentUser.token
              }
            })
        .then(response => {
            this.setState({ jurusan_subinduk: response.data.data, jurusan_subsubinduk: [] })
        })
        .catch(error => {
            if (error.response) {
                if(error.response.status == 422){
                        this.setState({ statusRes: true })
                }
            }
        });
    };

    handleChangeSubSub (){
        axios.get(apiJurusanSp + '?jurusan_induk=' + this.state.params, {
            headers: {
                'Authorization' : 'token=' + this.state.currentUser.token
              }
            })
        .then(response => {
            this.setState({ jurusan_subsubinduk: response.data.data })
        })
        .catch(error => {
            if (error.response) {
                if(error.response.status == 422){
                        this.setState({ statusRes: true })
                }
            }
        });
    };

    handleGetInduk = value => {
        this.setState({ idJurusanFinal: value.target.value })
    };

    /*
        Handles a change on the file upload
      */
    handleFileUpload = e => {
        this.setState({ file: e.target.files[0], fileName: e.target.files[0].name })
        //this.htmlFor(e.target.files[0].name)
        //console.log(e.target.files[0].name)
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({ valueSK: event.target.elements.sk_izin.value})

        if(event.target.elements.jurusan_subsubinduk.value != '' ){
            this.submitData(event.target.elements.jurusan_subsubinduk.value, event.target.elements.sk_izin.value, event.target.elements.tgl_sk_izin.value)
            console.log('SUBSUB Null : ' + event.target.elements.jurusan_subsubinduk.value)
        }else{
            this.submitData(event.target.elements.jurusan_subinduk.value, event.target.elements.sk_izin.value, event.target.elements.tgl_sk_izin.value)
            console.log('SUB Null')
        }
    }

    submitData(jurId, skIzin, tglIzin){
        const  formData = new FormData();
        formData.append('jurusan_id', jurId);
        formData.append('sk_izin', skIzin);
        formData.append('tanggal_sk_izin', tglIzin);
        formData.append('attachment_sk_izin ', this.state.file);

        axios.post(apiPostJurusan, formData, {
            headers: {
                'Authorization' : 'token=' + this.state.currentUser.token
            }
        })
        .then(res => {
            this.setState({ 
                statusRes: true,
                conditionAlert: 'success',
                messageRes: 'Berhasil Input'
            })
        })
        .catch(error => {
            if (error.response) {
                if(error.response.status == 422){
                    this.setState({ 
                        statusRes: true,
                        conditionAlert: 'warning',
                        messageRes: 'Input Ada Yang Salah'
                    })
                }else if(error.response.status == 401){
                    this.setState({ 
                        statusRes: true,
                        conditionAlert: 'danger',
                        messageRes: 'Unauthorized!'
                    })
                }else if(error.response.status == 409){
                    this.setState({ 
                        statusRes: true,
                        conditionAlert: 'warning',
                        messageRes: 'Duplicate Jurusan!'
                    })
                }
            }
        });
    }

    render() {
        const { currentUser } = this.state;
        return (
        <>
            <CRow>
                <CCol sm="12">
                    <CCard>
                        <CCardHeader>
                            <h3 id="traffic" className="card-title mb-0 text-center">Lembar Isian Tambah Kompetensi Keahlian Baru</h3>
                        </CCardHeader>
                        <CCardBody>
                        <CAlert color={this.state.conditionAlert} show={this.state.statusRes} closebutton={() => this.setState({ statusRes: false })}>
                            {this.state.messageRes} <strong>{this.state.valueSK}</strong>
                        </CAlert>
                        <CForm onSubmit={this.handleSubmit}>
                            <CFormGroup className="pr-1">
                                <CLabel htmlFor="jurusanSubInduk">Program Keahlian</CLabel>
                                <CInput readOnly value={ this.state.paramsName } />
                            </CFormGroup>

                            <CFormGroup className="pr-1">
                                <CLabel htmlFor="jurusanSubSubInduk">Kompetensi keahlian</CLabel>
                                <CSelect custom name="jurusan_subsubinduk" id="jurusanSubSubInduk" onChange={this.handleGetInduk} required>
                                            <option defaultValue readOnly>
                                                Pilih Kompetensi Keahlian...
                                            </option>
                                        {this.state.jurusan_subsubinduk.map((item, i) => {
                                        return(
                                            <option key={i} value={item.jurusan_id}>
                                                {item.nama_jurusan}
                                            </option>
                                        )
                                    })}
                                </CSelect>
                            </CFormGroup>

                            <hr/>

                            <CFormGroup>
                                <CLabel htmlFor="skIzin">Nomor Surat Keterangan Izin</CLabel>
                                <CInput type="text" name="sk_izin" id="skIzin" autoComplete="off" required></CInput>
                            </CFormGroup>
                            <CFormGroup>
                                <CLabel htmlFor="TanggalskIzin">Tanggal Surat Keterangan Izin</CLabel>
                                <CInput type="date" name="tgl_sk_izin" id="TanggalskIzin" required></CInput>
                            </CFormGroup>
                            <br/>
                            <CFormGroup>
                                <CLabel htmlFor="file-input">Unggah Berkas Surat Keterangan Izin</CLabel>
                                <CInputGroup>
                                    <CInputFile
                                        custom
                                        type="file"
                                        id="file"
                                        forwardref="file"
                                        onChange={this.handleFileUpload}
                                        accept="application/pdf"
                                        required/>
                                        <CLabel htmlFor="file" variant="custom-file">{this.state.fileName}</CLabel>
                                    </CInputGroup>
                            </CFormGroup>

                            <CCardFooter>
                                <CButton type="submit" color="info">Konfirmasi</CButton>
                            </CCardFooter>
                            </CForm>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
        )
    }
}

export default TambahKompetensiKeahlian
