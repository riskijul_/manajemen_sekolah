import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CInputRadio,
  CLabel,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCollapse,
  CFade,
  CInput,
  CInputGroup,
  CInputFile,
  CSelect
} from '@coreui/react'
import { userService, authenticationService } from '../../_services';
import { apiGetMapel, apiGetMapelGuru, apiGetGuru, apiPutPGuru, apiRombelList, stringSemester, apiGetRombelGuru } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';

class AssignMapelGuru extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            params: this.props.location.query,
            paramsName: this.props.location.name,
            data_guru: [],
            isLoadingGuru: true,
            file: null,
            file_name: 'Pilih File ...',
            data_get_select_rombel: [],
            parameter: JSON.parse(localStorage.getItem('mapelid'))
        };

        if (!localStorage.getItem('mapelid')){
          this.props.history.push('/manajemen_sekolah/kelola_mapel_guru');
        }
    }

    async componentDidMount() {
      try{
        await this.getGuru();
      }catch(error){
        console.log(error)
      }
    }

    async getGuru() {
        await userService.getData(apiGetGuru + this.state.parameter.id)
        .then(
            user => {
                var datas = user.data.data.map(item => {
                    return(
                        { value: item.ptk_terdaftar_id, label: item.nama }
                    )
                })
              this.setState({ data_guru: datas, isLoadingGuru: false })
            },

            error => {
                if (error.response) {
                    if(error.response.status == 502){
                        swal("502", "Kesalahan pada Server", "error");
                    }
                }
            }
        );
    }

    async getSelectGuru(value){
        this.setState( { data_get_select_guru: value } );

        try {
            userService.getData(apiGetRombelGuru + value + '&mata_pelajaran_id=' + this.state.parameter.id)
            .then(
                user => {
                    var datas = user.data.data.map(item => {
                        return(
                            { value: item.rombongan_belajar_id, label: item.nama_kelas }
                        )
                    })
                  this.setState({ data_rombel_guru: datas, isLoadingRombelGuru: false })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
          }
          catch (error) {
            console.log("try Catch" +error);
          }
    }

    getSelectRombel = (selectedOptions) => {
      if(selectedOptions != null){
        var data = selectedOptions.map(item => {
            return(
                item.value
            )
        })
        this.setState({ data_get_select_rombel: data });
      }else{
        this.setState({ data_get_select_rombel: [] });
      }      
    }

    handleFile = value => {
      this.setState({ file: value.target.files[0], file_name: value.target.files[0].name })
      console.log( value.target.files[0] )
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('mata_pelajaran_id', this.state.parameter.id);
        formData.append('tanggal_sk_mengajar', event.target.elements.tanggal_sk_mengajar.value);
        formData.append('sk_mengajar', event.target.elements.surat_keterangan_izin.value);
        formData.append('attachment', this.state.file);
        formData.append('ptk_terdaftar_id', this.state.data_get_select_guru);
        formData.append('rombongan_belajar_id', this.state.data_get_select_rombel);

        // console.log(event.target.elements.surat_keterangan_izin.value);
        axios.put(apiPutPGuru, formData, {
          headers: {
              'Authorization' : 'token=' + this.state.currentUser.token
          }
        })

        .then(res => {
            swal("Sukses!", "Berhasil Menugaskan Guru", "success");
            setTimeout(() => {
              this.props.history.push('/manajemen_sekolah/kelola_mapel_guru');
            }, 3000);
        })
        .catch(error => {
            if (error.response) {
                if(error.response.status == 422){
                  swal("Gagal!", "Input ada yang salah", "error");
                }else if(error.response.status == 401){
                  swal("Gagal!", "Unauthorized", "error");
                }else if(error.response.status == 409){
                  swal("Gagal!", "Duplikat Data", "error");
                }
            }
        });
    }
    
    render() {
        const { modalAssign, collapseOne, collapseTwo, modalWali, collapseNon} = this.state;
        
        return (
            <>
            <CRow>

        <CCol xs="12">
            <CCard>
              <CCardHeader>
                Lembar Isian Penugasan Guru
              </CCardHeader>
                <CCardBody>
                  <CForm className="form-horizontal" onSubmit={ this.handleSubmit }>
                    <CFormGroup>
                      <CLabel htmlFor="appendedPrependedInput">Materi Pelajaran</CLabel>
                      <div className="controls">
                          <CInput id="appendedPrependedInput" size="16" value={ this.state.parameter.nama } readOnly type="text" />
                      </div>
                    </CFormGroup>

                    <CFormGroup>
                      <CLabel htmlFor="appendedPrependedInput">Nomor Surat SOTK</CLabel>
                      <div className="controls">
                          <CInput id="appendedPrependedInput" id="surat_keterangan_izin" name="surat_keterangan_izin" size="16" type="text" autoComplete="off" />
                      </div>
                    </CFormGroup>

                    <CFormGroup>
                      <CLabel htmlFor="appendedInputButton">Tanggal Surat SOTK</CLabel>
                      <div className="controls">
                        <CInputGroup>
                            <CInput type="date" id="tanggal_sk_mengajar" name="tanggal_sk_mengajar" placeholder="date" />
                        </CInputGroup>
                      </div>
                    </CFormGroup>

                    <CFormGroup>
                      <CLabel htmlFor="appendedInputButtons">Unggah Surat SOTK</CLabel>
                      <div className="controls">
                        <CInputGroup>
                        <CInputFile 
                          id="sk_mengajar" 
                          name="sk_mengajar"
                          onChange={ this.handleFile }
                          accept="application/pdf"
                          type="file"
                          forwardref="file"
                          custom
                        />
                        <CLabel htmlFor="sk_mengajar" variant="custom-file"> { this.state.file_name } </CLabel>
                        </CInputGroup>
                      </div>
                    </CFormGroup>
                    <br/>
                    <hr/>
                    <CFormGroup>
                      <CLabel htmlFor="appendedInput">Pilih Guru</CLabel>
                      <div className="controls">
                      <Select
                        isLoading={ this.state.isLoadingGuru }
                        options={ this.state.data_guru }
                        placeholder="Cari Guru..."
                        onChange={(value) => this.getSelectGuru(value.value)}
                      />
                      </div>
                    </CFormGroup>

                    <CFormGroup>
                      <CLabel htmlFor="appendedInput">Pilih Rombel</CLabel>
                      <div className="controls">
                      <Select
                        isLoading={ this.state.isLoadingRombelGuru }
                        options={ this.state.data_rombel_guru }
                        placeholder="Cari Rombel..."
                        onChange={ this.getSelectRombel }
                        isMulti
                      />
                      </div>
                    </CFormGroup>
                    <hr/>
                    <br/>
                    <div className="form-actions">
                      <CButton type="submit" color="info">Simpan</CButton> <CButton to={{ pathname: `/manajemen_sekolah/kelola_mapel_guru`}} color="danger">Batal</CButton>
                    </div>
                  </CForm>
                </CCardBody>
            </CCard>
        </CCol>
      </CRow>
          </>
        )
    }
}

export default AssignMapelGuru