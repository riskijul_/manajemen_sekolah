import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CInputRadio,
  CLabel,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCollapse,
  CFade,
  CInput,
  CInputGroup,
  CInputFile,
  CSelect,
  CBadge,
  CCardFooter
} from '@coreui/react'
import { userService, authenticationService } from '../../_services';
import { apiGetJawaban, apiPatchSoal, apiPatchJawaban } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';

class RincianSoal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            paramsID: this.props.location.query,
            paramsName: this.props.location.name,
            parameter: JSON.parse(localStorage.getItem('soal_id')),
            soalID: '',
            jawabanID: ''
        };

        if (!localStorage.getItem('soal_id')){
            this.props.history.push('/manajemen_sekolah/validasi_soal');
        }
    }

    componentDidMount() {
        this.getJawaban();
    }

    async getJawaban(){
        await userService.getData(apiGetJawaban + this.state.parameter.id_soal)
        .then(
            user => {
                this.setState({ list_jawaban: user.data.data, isLoadData: false })
            },
            error => {
                if (error.response) {
                    if(error.response.status == 500){
                        swal('Ups!', 'Terjadi Error pada Server', 'error', {timer: 3000} );
                    }
                }
            }
        );
    }

    handleValidasi = (event) => {
        event.preventDefault();
        const formData = {
            id_soal: this.state.soalID,
            is_valid: event.target.elements.is_valid.value
        };

        try {
            axios.patch(apiPatchSoal, formData,{
                headers: {
                    'Authorization' : 'token=' + this.state.currentUser.token
                  }
                })
            .then(response => {
                if(response.status == 201){
                    this.setState({ modalValidasi: false });
                    swal("Sukses!", "Berhasil mengubah status soal", "success", {timer: 2000} );
                    setTimeout(() => {
                        this.props.history.push('/manajemen_sekolah/validasi_soal');
                    }, 2000);
                }
            })
            .catch(error => {
                if (error.response) {
                    if(error.response.status == 422){
                        swal("Gagal!", "Permintaan tidak dapat diproses", "error");
                        this.setState({ modalValidasi: false });
                    }
                }
            });
          } catch (error) {
            console.log("try Catch" +error);
          };
    }

    handleStatusJawaban = (event) => {
        event.preventDefault();
        const formData = {
            id_soal_jawaban: this.state.jawabanID,
            is_valid: event.target.elements.is_valid.value
        };

        try {
            axios.patch(apiPatchJawaban, formData,{
                headers: {
                    'Authorization' : 'token=' + this.state.currentUser.token
                  }
                })
            .then(response => {
                if(response.status == 201){
                    this.setState({ modalStatusJawaban: false });
                    swal("Sukses!", "Berhasil mengubah status jawaban", "success", {timer: 2000} );
                    setTimeout(() => {
                        this.props.history.push('/manajemen_sekolah/validasi_soal');
                    }, 2000);
                }
            })
            .catch(error => {
                if (error.response) {
                    if(error.response.status == 422){
                        swal("Gagal!", "Permintaan tidak dapat diproses", "error");
                        this.setState({ modalStatusJawaban: false });
                    }
                }
            });
          } catch (error) {
            console.log("try Catch" +error);
          };
    }

    render() {
        const { modalValidasi, modalStatusJawaban } = this.state;
        
        return (
        <>
            <CRow>
                {/* <CCol sm="12" className="text-center">
                    <CCard>
                        <CCardBody>
                            <h3 id="traffic" className="card-title mb-0">Rincian Soal</h3>
                        </CCardBody>
                    </CCard>
                </CCol> */}

                <CCol sm="12">
                    <CCard borderColor="success">
                        <CCardBody>
                            <h4 className="text-center"> { this.state.parameter.nama_mata_pelajaran } </h4> <br/>
                            <CLabel><b>Indikator Soal : </b></CLabel><br/>
                            <CFormGroup>
                                <CCard variant="outline" borderColor="success" style={{ padding: "10px" }}>
                                    { this.state.parameter.desk }
                                </CCard>
                            </CFormGroup>
                        </CCardBody>
                        <CCardFooter>
                            <CBadge color="danger" className="float-right">Kelas { this.state.parameter.tingkat_pendidikan_id }</CBadge>
                        </CCardFooter>
                    </CCard>
                </CCol>

                <CCol sm="12">
                    <CCard>
                        <CCardBody>

                        <CRow>
                            <CCol sm="10">
                            <CLabel><b>Soal : </b></CLabel>
                                <CFormGroup>
                                    <CCard variant="outline" borderColor="info" style={{padding: "10px", height: "100px" }}>
                                        <CCardBody >
                                            { this.state.parameter.soal }
                                        </CCardBody>            
                                    </CCard>
                                </CFormGroup>
                            </CCol>
                            <CCol sm="2">
                            <CLabel><b>Status Soal : </b></CLabel>
                                <CFormGroup>
                                    { this.state.parameter.is_valid == "0" ?
                                    <>
                                        <CCard variant="outline" borderColor="warning" className="text-center" style={{ padding: "10px", height: "100px" }}>
                                            <CCardBody >
                                                <CButton
                                                    color="warning"
                                                    style={{ color: "white", border: "1px solid black" }}
                                                    onClick={ () => this.setState({ modalValidasi: true, soalID: this.state.parameter.id_soal }) }
                                                >
                                                    Perlu Aksi
                                                </CButton>
                                            </CCardBody>
                                        </CCard>
                                    </>
                                    : this.state.parameter.is_valid == "1" ?
                                    <>
                                        <CCard variant="outline" borderColor="info" className="text-center" style={{ padding: "10px", height: "100px" }}>
                                            <CCardBody >
                                                <CButton
                                                    color="info"
                                                    onClick={ () => this.setState({ modalValidasi: true, soalID: this.state.parameter.id_soal }) }
                                                >
                                                    Disetujui
                                                </CButton>
                                            </CCardBody>
                                        </CCard>
                                    </>
                                    :
                                    <>
                                        <CCard variant="outline" borderColor="danger" className="text-center" style={{ padding: "10px", height: "100px" }}>
                                            <CCardBody >
                                                <CButton
                                                    color="danger"
                                                    onClick={ () => this.setState({ modalValidasi: true, soalID: this.state.parameter.id_soal }) }
                                                >
                                                    Tidak Disetujui
                                                </CButton>
                                            </CCardBody>
                                        </CCard>
                                    </>
                                    }
                                </CFormGroup>
                            </CCol>
                        </CRow>

                        <CDataTable
                            responsive
                            hover
                            items={ this.state.list_jawaban }
                            fields={ [ { key: "jawaban", label: "Pilihan" }, { key: "is_jawaban", label: "Jawaban" } ] }
                            itemsPerPage={10}
                            pagination
                            loading={ this.state.isLoadData }
                            // tableFilter={{label: 'Cari :', placeholder: 'Cari Mata Pelajaran...'}}
                            scopedSlots = {{
                                'jawaban':
                                (item)=>(
                                    <td>
                                        { item.jawaban }
                                    </td>
                                ),
                                'is_jawaban':
                                (item)=>(
                                    <td>
                                        { item.is_jawaban != "0" ?
                                        <>
                                            <CBadge color="info">Benar</CBadge>
                                        </>
                                        :
                                        <>
                                            <CBadge color="danger">Salah</CBadge>
                                        </>
                                        }
                                    </td>
                                )
                            }}
                            />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>

            <CModal 
              show={modalValidasi} 
              onClose={() => this.setState( { modalValidasi: false }) }
              color="success"
            >
            <CForm className="form-horizontal" onSubmit={this.handleValidasi}>
              <CModalHeader>
                <CModalTitle>Ubah Status Soal</CModalTitle>
              </CModalHeader>
              <CModalBody>
                    <CFormGroup>
                        <CLabel>Pilih Status : </CLabel> <br/>
                        <CFormGroup variant="custom-radio" inline>
                            <CInputRadio custom id="valid" name="is_valid" value="1" />
                            <CLabel variant="custom-checkbox" htmlFor="valid">Setuju</CLabel>
                        </CFormGroup>
                        <CFormGroup variant="custom-radio" inline>
                            <CInputRadio custom id="tidak_valid" name="is_valid" value="-1" />
                            <CLabel variant="custom-checkbox" htmlFor="tidak_valid">Tidak Setuju</CLabel>
                        </CFormGroup>
                        {/* <CSelect name="is_valid" id="is_valid">
                            <option defaultValue disabled>Pilih...</option>
                            <option value="1" className="bg-info">Valid</option>
                            <option value="-1" className="bg-danger">Tidak Valid</option>
                        </CSelect> */}
                    </CFormGroup>
              </CModalBody>
              <CModalFooter>
                <CButton color="danger" onClick={() => this.setState( { modalValidasi: false }) }>Batal</CButton>
                <CButton type="submit" color="success">Simpan</CButton>
              </CModalFooter>
              </CForm>
            </CModal>

            <CModal 
              show={modalStatusJawaban} 
              onClose={() => this.setState( { modalStatusJawaban: false }) }
              color="success"
            >
            <CForm className="form-horizontal" onSubmit={this.handleStatusJawaban}>
              <CModalHeader>
                <CModalTitle>Ubah Status Jawaban</CModalTitle>
              </CModalHeader>
              <CModalBody>
                    <CFormGroup>
                        <CLabel>Pilih Status : </CLabel> <br/>
                        <CFormGroup variant="custom-radio" inline>
                            <CInputRadio custom id="jawaban_valid" name="is_valid" value="1" />
                            <CLabel variant="custom-checkbox" htmlFor="jawaban_valid">Setuju</CLabel>
                        </CFormGroup>
                        <CFormGroup variant="custom-radio" inline>
                            <CInputRadio custom id="jawaban_tidak_valid" name="is_valid" value="-1" />
                            <CLabel variant="custom-checkbox" htmlFor="jawaban_tidak_valid">Tidak Setuju</CLabel>
                        </CFormGroup>
                        {/* <CSelect name="is_valid" id="is_valid">
                            <option defaultValue disabled>Pilih...</option>
                            <option value="1" className="bg-info">Valid</option>
                            <option value="-1" className="bg-danger">Tidak Valid</option>
                        </CSelect> */}
                    </CFormGroup>
              </CModalBody>
              <CModalFooter>
                <CButton color="danger" onClick={() => this.setState( { modalStatusJawaban: false }) }>Batal</CButton>
                <CButton type="submit" color="success">Simpan</CButton>
              </CModalFooter>
              </CForm>
            </CModal>
        </>
        )
    }
}

export default RincianSoal