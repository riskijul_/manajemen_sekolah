import React, { lazy, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CFormGroup,
  CFormText,
  CInputFile,
  CSelect,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CLabel,
  CAlert,
  CCollapse,
  CFade,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CTooltip,
  CBadge,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { apiJurusan, apiPatchJurusan } from '../../_components';
// import MainChartExample from '../charts/MainChartExample.js'
import axios from 'axios'

// const WidgetsDropdown = lazy(() => import('../widgets/WidgetsDropdown.js'))
// const WidgetsBrand = lazy(() => import('../widgets/WidgetsBrand.js'))

class JurusanSP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        params: this.props.location.query,
        paramsName: this.props.location.name,
        paramsNama: this.props.location.nama,
        currentUser: authenticationService.currentUserValue.data,
        users: null,
        data_jurusan: [],
        data_subjurusan: [],
        statusRes: false,
        details: [],
        setDetails: [],
        modalInfo: false
    };
  }

  componentDidMount() {
    //userService.getAll().then(users => this.setState({ users }));
    try {
        userService.getData(apiJurusan + '1')
        .then(
            user => {
                this.setState({ data_jurusan: user.data.data })
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            }
        );
        } catch (error) {
            console.log("try Catch" +error);
        }
    }

    refreshTable(){
        try {
            userService.getData(apiJurusan + '1')
            .then(
                user => {
                    this.setState({ data_jurusan: user.data.data })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
        } catch (error) {
            console.log("try Catch" +error);
        }
    }

    getSubJurusan(cond){
        try {
            userService.getData(apiJurusan + '2&jurusan_induk=' + cond)
            .then(
                user => {
                    this.setState({ data_subjurusan: user.data.data })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
        } catch (error) {
            console.log("try Catch" +error);
        }
    }

    toggleDetails(index, jurusan_id) {
        const position = this.state.setDetails.indexOf(index)
        let newDetails = this.state.details.slice()

        if (position != -1) {
            newDetails.splice(position, 1)
        } else {
            newDetails = [...this.state.details, index]
            this.getSubJurusan(jurusan_id);
        }

        this.setState({ setDetails: newDetails })
    }

    handleUpdateBtn(value){
        try {
            axios.patch(apiPatchJurusan + value, {},{
                headers: {
                    'Authorization' : 'token=' + this.state.currentUser.token
                  }
                })
            .then(response => {
                //this.setState({ data_jurusan: response.data.data })
                console.log(response);
                if(response.status == 201){
                    this.refreshTable()
                }
            })
            .catch(error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            });
          } catch (error) {
            console.log("try Catch" +error);
          }
    }

  render() {
    const { currentUser, users, details, setDetails, modalInfo } = this.state;
    return (
      <>
        <CRow>
            <CCol sm="12" className="text-center">
                <CCard>
                    <CCardBody>
                        <h3 id="traffic" className="card-title mb-0">Kelola Kompetensi Keahlian</h3>
                    </CCardBody>
                </CCard>
            </CCol>
            <CCol sm="12">
                <CCard>
                    <CCardBody>
                        <CAlert color="primary" show={this.state.statusRes} closebutton={() => this.setState({ statusRes: false })}>
                            NSPN Tidak Di Input
                        </CAlert>
                        <CCol className="text-right" style={{ margin: 10}}>
                            <CButton className="btn btn-info" to={{pathname: `/manajemen_sekolah/jurusan/input_sk`}}>Tambah Jurusan Baru</CButton>
                        </CCol>
                        <CDataTable
                            items={this.state.data_jurusan}
                            fields={[{ key: 'Program_Keahlian' }, {key: 'Status' }, { key: 'Aksi', sorter: false } ]}
                            pagination
                            hover
                            sorter
                            itemsPerPage={10}
                            scopedSlots = {{
                                'details':
                                    (item, index)=>{
                                    return (
                                        <CCollapse show={setDetails.includes(index)}>
                                            <CCardBody>
                                                <table>
                                                    <tbody>
                                                    {this.state.data_subjurusan.map((itema, i) => {
                                                        return(
                                                            <tr key={i}>
                                                                <td>
                                                                    <h6>{itema.nama_jurusan}</h6>
                                                                </td>
                                                                {/* <td>
                                                                    <CButton size="sm" color="danger">Del</CButton>
                                                                </td> */}
                                                            </tr>
                                                        )   
                                                    })}
                                                    <CButton color="success" size="sm" variant="outline" to={{ pathname: `/manajemen_sekolah/jurusan/tambah_kompetensi_keahlian`, query: item.jurusan_id, name: item.nama_jurusan }}> + Tambah Kompetensi Keahlian</CButton>
                                                    </tbody>
                                                </table>
                                            {/* <p className="text-muted">User since: </p>
                                            <CButton size="sm" color="info">
                                                User Settings
                                            </CButton>
                                            <CButton size="sm" color="danger" className="ml-1">
                                                Delete
                                            </CButton> */}
                                            </CCardBody>
                                        </CCollapse>
                                    )
                                },
                                'Program_Keahlian':
                                (item)=>(
                                    <td>
                                        <CCol className="col-sm-12">
                                            {item.nama_jurusan}
                                        </CCol>
                                    </td>
                                ),
                                'Status':
                                (item)=>(
                                    <td>
                                        {/* {item.status_jurusan == '0' ? 'Perlu Update' : 'Kur.2013/Rev.2018'} */}
                                        {item.status_jurusan == '0' ?
                                        // <Link to={{pathname: `/jurusan/detail`, query: item.jurusan_id, name: item.nama_jurusan}}>
                                            <CTooltip placement="right-end" content={ 'Ketuk untuk Sinkron' }>
                                                <CButton size="sm" style={{ border: '1px solid black', color: 'white' }} color="warning" className="" onClick={() => { if (window.confirm('Apakah Anda Yakin melakukan Sinkron Jurusan Ini?\n' + item.nama_jurusan)) this.handleUpdateBtn(item.jurusan_id)}}>
                                                    Perlu Sinkron
                                                </CButton>
                                            </CTooltip>
                                             :
                                             <CBadge color="success">Sudah Sinkron</CBadge>
                                            //  <CTooltip placement="right-end" content={ 'Sudah Sinkron' }>
                                            //     <CButton disabled color="warning" className="">Perlu Sinkron</CButton>
                                            // </CTooltip>
                                        }
                                    </td>
                                ),
                                'Aksi':
                                (item, index)=>(
                                    <td>
                                        <CButton
                                            color="info"
                                            variant="outline"
                                            size="sm"
                                            style={{ marginRight: '10px' }}
                                            onClick={() => this.toggleDetails(index, item.jurusan_id)}
                                            
                                        >
                                            {setDetails.includes(index) ? 'Tutup' : 'Lihat Kompetensi Keahlian'}
                                        </CButton>
                                        
                                    </td>
                                )
                            }}
                        />
                    </CCardBody>
                </CCard>
            </CCol>
        </CRow>
      </>
    )
  }
}

export default JurusanSP
