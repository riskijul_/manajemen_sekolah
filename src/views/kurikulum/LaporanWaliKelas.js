import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { history } from '../../_helpers';

// import usersData from '../users/UsersData'

const siswaData = [
  {Tingkat:'12', JumlahSiswa:'1000', JumlahSiswi:'200', status:'Action'},
]

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}

const fields = ['Tingkat', 'JumlahSiswa', 'JumlahSiswi', 'status']

class LaporanWaliKelas extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        currentUser: authenticationService.currentUserValue,
        users: null
    };
  }

  render() {
    const { currentUser, users } = this.state;
    return (
      <>
        <CRow>
          <CCol>
            <CCard>
              <CCardHeader>
                <h4>Pilih Tingkat</h4>
              </CCardHeader>
              <CCardBody>
              <CDataTable
                items={siswaData}
                fields={fields}
                hover
                bordered
                size="sm"
                itemsPerPage={10}
                pagination
                scopedSlots = {{
                  'status':
                    (item)=>(
                      <td>
                          <CButton variant="outline" color="success" to="/manajemen_sekolah/dashboard_tata_usaha">Pilih</CButton>
                      </td>
                    )
                }}
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    )
  }
}

export default LaporanWaliKelas
