import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CInputRadio,
  CLabel,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCollapse,
  CFade,
  CInput,
  CSelect,
  CTooltip, 
  CBadge
} from '@coreui/react'
import { userService, authenticationService } from '../../_services';
import { apiGetSoal, apiPatchSoal } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';

class ValidasiSoal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            paramsID: this.props.location.query,
            paramsName: this.props.location.name,
            modalValidasi: false,
            soalID: '',
            isLoadData: true
        };
        //console.log(this.state.arrSmstr)
    }

    async componentDidMount() {
        await this.getPembelajaran();        
    }

    componentWillUnmount(){
        //this.getRevJurusan();
    }

    async getPembelajaran() {
        await userService.getData(apiGetSoal)
        .then(
            user => {
                this.setState({ list_soal: user.data.data, isLoadData: false })
            },
            error => {
                if (error.response) {
                    if(error.response.status == 500){
                        swal('Ups!', 'Terjadi Error pada Server', 'error', {timer: 3000} );
                    }
                }
            }
        );
    }

    handleValidasi = (event) => {
        event.preventDefault();
        const formData = {
            id_soal: this.state.soalID,
            is_valid: event.target.elements.is_valid.value
        };

        try {
            axios.patch(apiPatchSoal, formData,{
                headers: {
                    'Authorization' : 'token=' + this.state.currentUser.token
                  }
                })
            .then(response => {
                if(response.status == 201){
                    swal("Sukses!", "Berhasil mengubah status", "success");
                    this.setState({ modalValidasi: false });
                    this.getPembelajaran();
                }
            })
            .catch(error => {
                if (error.response) {
                    if(error.response.status == 422){
                        swal("Gagal!", "Permintaan tidak dapat diproses", "error");
                        this.setState({ modalValidasi: false });
                        this.getPembelajaran();
                    }
                }
            });
          } catch (error) {
            console.log("try Catch" +error);
          };
    }

    handleSessData(cond, nama, deskripsi, ptk_nama, isi_soal, tingkat_pendidikan, status_valid){
        const data = {
            "id_soal"               : cond,
            "nama_mata_pelajaran"   : nama,
            "desk"                  : deskripsi,
            "nama_ptk"              : ptk_nama,
            "soal"                  : isi_soal,
            "tingkat_pendidikan_id" : tingkat_pendidikan,
            "is_valid"              : status_valid
        }
        localStorage.setItem('soal_id', JSON.stringify(data));
        //sessionStorage.setItem('idjurusan', cond)
    }
    
    render() {
        const { modalValidasi } = this.state;
        
        return (
            <>
            <CRow>
                <CCol sm="12" className="text-center">
                    <CCard>
                        <CCardBody >
                            <h3 id="traffic" className="card-title mb-0">List Soal</h3>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="12">
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                responsive
                                hover
                                items={ this.state.list_soal }
                                fields={ [{key: 'nama_mata_pelajaran' }, {key: 'tingkat_pendidikan_id', label: 'Untuk Kelas'}, {key: 'nama_ptk', label: 'Dibuat Oleh' }, 'Status', 'Aksi'] }
                                itemsPerPage={10}
                                pagination
                                sorter
                                loading={ this.state.isLoadData }
                                tableFilter={{label: 'Cari :', placeholder: 'Cari Mata Pelajaran...'}}
                                scopedSlots = {{
                                    'Status':
                                    (item)=>(
                                        <td>
                                            { item.is_valid == '0' ?
                                            <>
                                                <CBadge color="warning" style={{ color: "white" }}>Belum Disetujui</CBadge>
                                            </>
                                            : item.is_valid == '1' ?
                                            <>
                                                <CBadge color="info">Disetujui</CBadge>
                                            </>
                                            :
                                            <>
                                                <CBadge color="danger">Tidak Disetujui</CBadge>
                                            </>
                                            }
                                        </td>
                                    ),
                                    'Aksi':
                                    (item)=>(
                                        <td>
                                            {/* <CButton color="success" size="sm" style={{ marginRight: '10px'}} onClick={ () => this.setState({ modalValidasi: true, soalID: item.id_soal }) }>Ubah Status</CButton> */}
                                            <CButton
                                                color="info"
                                                onClick={() => this.handleSessData(item.id_soal, item.nama_mata_pelajaran, item.desk, item.nama_ptk, item.soal, item.tingkat_pendidikan_id, item.is_valid)}
                                                to={{ pathname: `/manajemen_sekolah/validasi_soal/rincian_soal`, query: item.id_soal, name: item.nama_mata_pelajaran }}
                                                size="sm"
                                            >
                                                Lihat Soal
                                            </CButton>
                                        </td>
                                    )
                                }}
                                />
                            </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
          </>
        )
    }
}

export default ValidasiSoal