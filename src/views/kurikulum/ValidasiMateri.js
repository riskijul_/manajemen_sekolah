import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CInputRadio,
  CLabel,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCollapse,
  CFade,
  CInput,
  CSelect,
  CTooltip, 
  CBadge
} from '@coreui/react'
import { userService, authenticationService } from '../../_services';
import { apiGetMateri, apiPatchMateri } from '../../_components';
import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';

class ValidasiMateri extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            paramsID: this.props.location.query,
            paramsName: this.props.location.name,
            modalValidasi: false,
            materiID: '',
            isLoadData: true
        };
        //console.log(this.state.arrSmstr)
    }

    componentDidMount() {
        this.getMateri();        
    }

    componentWillUnmount(){
        //this.getRevJurusan();
    }

    async getMateri() {
        await userService.getData(apiGetMateri)
        .then(
            user => {
                this.setState({ list_materi: user.data.data, isLoadData: false })
            },
            error => {
                if (error.response) {
                    if(error.response.status == 500){
                        swal('Ups!', 'Terjadi Error pada Server', 'error', {timer: 3000} );
                    }
                }
            }
        );
    }

    handleValidasi = (event) => {
        event.preventDefault();
        const formData = {
            id_materi: this.state.materiID,
            is_valid: event.target.elements.is_valid.value
        };

        console.log(this.state.materiID, event.target.elements.is_valid.value);

        try {
            axios.patch(apiPatchMateri, formData,{
                headers: {
                    'Authorization' : 'token=' + this.state.currentUser.token
                  }
                })
            .then(response => {
                if(response.status == 201){
                    swal("Sukses!", "Berhasil mengubah status", "success");
                    this.setState({ modalValidasi: false });
                    this.getMateri();
                }
            })
            .catch(error => {
                if (error.response) {
                    if(error.response.status == 422){
                        swal("Gagal!", "Permintaan tidak dapat diproses", "error");
                        this.setState({ modalValidasi: false });
                        this.getMateri();
                    }
                }
            });
          } catch (error) {
            console.log("try Catch" +error);
          };
    }
    
    render() {
        const { modalValidasi } = this.state;
        
        return (
            <>
            <CRow>
                <CCol sm="12" className="text-center">
                    <CCard>
                        <CCardBody >
                            <h3 id="traffic" className="card-title mb-0">Materi</h3>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="12">
                    <CCard>
                        <CCardBody>
                            <CDataTable
                                responsive
                                hover
                                items={ this.state.list_materi }
                                fields={ [{key: 'mata_pelajaran'}, {key: 'judul' }, {key: 'konten', style: 'backgroundColor: black'}, {key: 'file' }, 'Status', 'Aksi'] }
                                itemsPerPage={10}
                                pagination
                                sorter
                                loading={ this.state.isLoadData }
                                tableFilter={{label: 'Cari :', placeholder: 'Cari Mata Pelajaran...'}}
                                scopedSlots = {{
                                    'mata_pelajaran':
                                    (item)=>(
                                        <td>
                                            { item.nama_mata_pelajaran }
                                        </td>
                                    ),
                                    'judul':
                                    (item)=>(
                                        <td>
                                            { item.title }
                                        </td>
                                    ),
                                    'konten':
                                    (item)=>(
                                        <td>
                                            { item.konten }
                                        </td>
                                    ),
                                    'file':
                                    (item)=>(
                                        <td>
                                            { item.attachment }
                                        </td>
                                    ),
                                    'Status':
                                    (item)=>(
                                        <td>
                                            { item.is_valid == '0' ?
                                            <>
                                                <CBadge color="warning" style={{ color: "white" }}>Belum Disetujui</CBadge>
                                            </>
                                            : item.is_valid == '1' ?
                                            <>
                                                <CBadge color="info">Disetujui</CBadge>
                                            </>
                                            :
                                            <>
                                                <CBadge color="danger">Tidak Disetujui</CBadge>
                                            </>
                                            }
                                        </td>
                                    ),
                                    'Aksi':
                                    (item)=>(
                                        <td>
                                            {/* <CButton color="success" size="sm" style={{ marginRight: '10px'}} onClick={ () => this.setState({ modalValidasi: true, soalID: item.id_soal }) }>Ubah Status</CButton> */}
                                            { item.is_valid == '0' ?
                                            <>
                                                <CButton size="sm" color="warning" style={{ color: 'white', border: '1px solid black' }} onClick={() => this.setState({modalValidasi: true, materiID: item.id_materi}) }>Perlu Aksi</CButton>
                                            </>
                                            : item.is_valid == '1' ?
                                            <>
                                            <CButton size="sm" color="success" style={{ border: '1px solid black' }} onClick={() => this.setState({modalValidasi: true, materiID: item.id_materi}) }>Ubah</CButton>
                                            </>
                                            :
                                            <>
                                                <CButton size="sm" color="success" style={{ border: '1px solid black' }} onClick={() => this.setState({modalValidasi: true, materiID: item.id_materi}) }>Ubah</CButton>
                                            </>
                                            }
                                        </td>
                                    )
                                }}
                                />
                            </CCardBody>
                    </CCard>
                </CCol>
            </CRow>

            <CModal 
              show={modalValidasi} 
              onClose={() => this.setState( { modalValidasi: false }) }
              color="success"
            >
            <CForm className="form-horizontal" onSubmit={this.handleValidasi}>
              <CModalHeader>
                <CModalTitle></CModalTitle>
              </CModalHeader>
              <CModalBody>
                    <CFormGroup>
                        <CLabel>Pilih Status : </CLabel> <br/>
                        <CFormGroup variant="custom-radio" inline>
                            <CInputRadio custom id="valid" name="is_valid" value="1" />
                            <CLabel variant="custom-checkbox" htmlFor="valid">Setuju</CLabel>
                        </CFormGroup>
                        <CFormGroup variant="custom-radio" inline>
                            <CInputRadio custom id="tidak_valid" name="is_valid" value="-1" />
                            <CLabel variant="custom-checkbox" htmlFor="tidak_valid">Tidak Setuju</CLabel>
                        </CFormGroup>
                    </CFormGroup>
              </CModalBody>
              <CModalFooter>
                <CButton color="danger" onClick={() => this.setState( { modalValidasi: false }) }>Batal</CButton>
                <CButton type="submit" color="success">Simpan</CButton>
              </CModalFooter>
              </CForm>
            </CModal>
          </>
        )
    }
}

export default ValidasiMateri