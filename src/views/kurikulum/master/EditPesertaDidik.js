import React, { lazy } from 'react'
import {
    CButton,
    CCard,
    CAlert,
    CCardBody,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CDataTable,
    CForm,
    CInputRadio,
    CLabel,
    CFormGroup,
    CInput,
    CSelect,
    CTextarea,
    CCardFooter
} from '@coreui/react'
import { userService, authenticationService } from '../../../_services';
import { apiGetPesertaDidik, apiPutPesertaDidik, apiGetAgama } from '../../../_components';
// import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';
import Moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from '@fortawesome/fontawesome-free-solid';
import DatePicker from 'react-date-picker';
import { formatDate } from '../../../_helpers/date_format'


class EditPesertaDidik extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            params: JSON.parse(localStorage.getItem('peserta_didik')),
            nisn: JSON.parse(localStorage.getItem('peserta_didik')).nisn,
            nama: JSON.parse(localStorage.getItem('peserta_didik')).nama,
            nik: JSON.parse(localStorage.getItem('peserta_didik')).nik,
            tempat_lahir: JSON.parse(localStorage.getItem('peserta_didik')).tempat_lahir,
            tanggal_lahir: JSON.parse(localStorage.getItem('peserta_didik')).tanggal_lahir,
            agama_id: JSON.parse(localStorage.getItem('peserta_didik')).agama_id,
            agama: JSON.parse(localStorage.getItem('peserta_didik')).agama,
            jenis_kelamin: JSON.parse(localStorage.getItem('peserta_didik')).jenis_kelamin,
            alamat_jalan: JSON.parse(localStorage.getItem('peserta_didik')).alamat_jalan,
            rt: JSON.parse(localStorage.getItem('peserta_didik')).rt,
            rw: JSON.parse(localStorage.getItem('peserta_didik')).rw,
            nama_dusun: JSON.parse(localStorage.getItem('peserta_didik')).nama_dusun,
            desa_kelurahan: JSON.parse(localStorage.getItem('peserta_didik')).desa_kelurahan,
            kode_pos: JSON.parse(localStorage.getItem('peserta_didik')).kode_pos,
            nomor_telepon_rumah: JSON.parse(localStorage.getItem('peserta_didik')).nomor_telepon_rumah,
            nomor_telepon_seluler: JSON.parse(localStorage.getItem('peserta_didik')).nomor_telepon_seluler,
            email: JSON.parse(localStorage.getItem('peserta_didik')).email,
            nama_ayah: JSON.parse(localStorage.getItem('peserta_didik')).nama_ayah,
            nama_ibu_kandung: JSON.parse(localStorage.getItem('peserta_didik')).nama_ibu_kandung,
            nama_wali: JSON.parse(localStorage.getItem('peserta_didik')).nama_wali,
            list_agama: [],
            date: new Date()
        };
    }

    componentDidMount() {
        // this.getListPD();
        this.getAgama();
    }

    componentWillUnmount() {
        //this.getRevJurusan();
    }

    getAgama() {
        userService.getData(apiGetAgama)
            .then(response => {
                this.setState({ list_agama: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    handleDate(event) {
        const date_format = formatDate(new Date(event), 'yyyy-MM-dd');
        this.setState({ tanggal_lahir: date_format });
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const id = target.id;

        this.setState({
            [id]: value,
        })

    }

    handleSubmit = (event) => {
        event.preventDefault();
        var formData = {
            peserta_didik_id: JSON.parse(localStorage.getItem('peserta_didik')).peserta_didik_id,
            nisn: event.target.elements.nisn.value,
            nama: event.target.elements.nama.value,
            nik: event.target.elements.nik.value,
            tempat_lahir: event.target.elements.tempat_lahir.value,
            tanggal_lahir: this.state.tanggal_lahir,
            agama_id: event.target.elements.agama_id.value,
            jenis_kelamin: event.target.elements.jenis_kelamin.value,
            alamat_jalan: event.target.elements.alamat_jalan.value,
            rt: event.target.elements.rt.value,
            rw: event.target.elements.rw.value,
            nama_dusun: event.target.elements.nama_dusun.value,
            desa_kelurahan: event.target.elements.desa_kelurahan.value,
            kode_pos: event.target.elements.kode_pos.value,
            nomor_telepon_rumah: event.target.elements.nomor_telepon_rumah.value,
            nomor_telepon_seluler: event.target.elements.nomor_telepon_seluler.value,
            email: event.target.elements.email.value,
            nama_ayah: event.target.elements.nama_ayah.value,
            nama_ibu_kandung: event.target.elements.nama_ibu_kandung.value,
            nama_wali: event.target.elements.nama_wali.value
        }

        axios.put(apiPutPesertaDidik, formData, {
            headers: {
                'Authorization': 'token=' + this.state.currentUser.token
            }
        })

            .then(res => {
                swal("Sukses!", "Berhasil Melakukan Perubahan", "success");
                this.props.history.push('/manajemen_sekolah/master/list_jurusan/list_rombel/list_peserta_didik');
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                        swal("Gagal!", "Input ada yang salah", "error");
                    } else if (error.response.status == 401) {
                        swal("Gagal!", "Unauthorized", "error");
                    } else if (error.response.status == 409) {
                        swal("Gagal!", "Duplikat Data", "error");
                    }
                }
            });
    }

    render() {
        const { } = this.state;

        return (
            <>
                <CForm onSubmit={this.handleSubmit}>
                    <CCard style={{ boxShadow: "3px 3px 3px gray" }}>
                        <CCardBody>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">NISN : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nisn} id="nisn" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">NIK : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nik} id="nik" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                        </CCardBody>
                    </CCard>
                    <CCard style={{ boxShadow: "3px 3px 3px gray" }}>
                        <CCardBody>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama Siswa : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama} id="nama" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Tempat Lahir : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.tempat_lahir} id="tempat_lahir" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Tanggal Lahir : </CLabel>
                                    <div className="controls">
                                        <DatePicker
                                            id="tanggal_lahir"
                                            onChange={(event) => this.handleDate(event)}
                                            value={new Date(this.state.tanggal_lahir)}
                                            clearIcon={null}
                                        />
                                    </div>
                                    <small style={{ color: 'green' }}>Format : Bulan/Tanggal/Tahun</small>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Agama : </CLabel>
                                    <div className="controls">
                                        <CSelect id="agama_id">
                                            <option defaultValue={this.state.agama_id} value={this.state.agama_id} onChange={(event) => this.handleChange(event)}>{this.state.agama} (Agama Asal)</option>
                                            {this.state.list_agama.map((item, i) => {
                                                return (
                                                    <option key={i} value={item.agama_id} onChange={(event) => this.handleChange(event)}>
                                                        {item.nama}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Jenis Kelamin : </CLabel>
                                    <div className="controls">
                                        <CSelect id="jenis_kelamin">
                                            {this.state.jenis_kelamin == 'L' ?
                                                <>
                                                    <option value={this.state.jenis_kelamin}>{this.state.jenis_kelamin}</option>
                                                    <option value="P">P</option>
                                                </>
                                                : this.state.jenis_kelamin == 'P' ?
                                                    <>
                                                        <option value={this.state.jenis_kelamin}>{this.state.jenis_kelamin}</option>
                                                        <option value="L">L</option>
                                                    </>
                                                    : <></>
                                            }
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Alamat : </CLabel>
                                    <div className="controls">
                                        {/* <CInput name="" value={this.state.alamat_jalan} id="alamat_jalan" onChange={(event)=>this.handleChange(event)}/> */}
                                        <CTextarea id="alamat_jalan" value={this.state.alamat_jalan} onChange={(event) => this.handleChange(event)}>{this.state.alamat_jalan}</CTextarea>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">RT/RW : </CLabel>
                                    <div className="controls">
                                        <CRow>
                                            <CCol sm="3">
                                                <CInput name="" value={this.state.rt} id="rt" onChange={(event) => this.handleChange(event)} />
                                            </CCol>
                                            <CCol sm="3">
                                                <CInput className="number" name="" value={this.state.rw} id="rw" onChange={(event) => this.handleChange(event)} />
                                            </CCol>
                                        </CRow>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama Dusun : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama_dusun} id="nama_dusun" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Desa Kelurahan : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.desa_kelurahan} id="desa_kelurahan" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Kode Pos : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.kode_pos} id="kode_pos" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">No. Telepon Rumah : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nomor_telepon_rumah} id="nomor_telepon_rumah" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">No. Telepon Seluler (HP) : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nomor_telepon_seluler} id="nomor_telepon_seluler" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Email : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.email} id="email" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama Ayah : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama_ayah} id="nama_ayah" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama Ibu : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama_ibu_kandung} id="nama_ibu_kandung" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama Wali : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama_wali} id="nama_wali" onChange={(event) => this.handleChange(event)} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CButton color="success" type="submit"><FontAwesomeIcon icon={faSave} /> Simpan Perubahan</CButton>
                                </CCol>
                            </CFormGroup>

                        </CCardBody>
                    </CCard>
                </CForm>
            </>
        )
    }
}

export default EditPesertaDidik
