import React, { lazy } from 'react'
import {
    CButton,
    CCard,
    CAlert,
    CCardBody,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CDataTable,
    CForm,
    CInputRadio,
    CLabel,
    CFormGroup,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
    CCollapse,
    CFade,
    CInput,
    CSelect,
    CBadge
} from '@coreui/react'
import { userService, authenticationService } from '../../../_services';
import { apiGetPtk } from '../../../_components';
// import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';
import Moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserEdit, faEye } from '@fortawesome/fontawesome-free-solid'
import { formatDate } from '../../../_helpers/date_format';

class ListGuru extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            params: this.props.location.query,
            loadData: true,
            detailGuru: null
        };

        localStorage.removeItem('guru');
    }

    componentDidMount() {
        this.getListGuru();
    }

    componentWillUnmount() {
        //this.getRevJurusan();
    }

    async getListGuru() {
        await userService.getData(apiGetPtk)
            .then(
                user => {
                    this.setState({ list_guru: user.data.data, loadData: false })
                },
                error => {
                    if (error.response) {
                        if (error.response.status == 422) {
                            this.setState({
                                statusRes: true,
                                conditionAlert: 'danger',
                                messageRes: 'NPSN Tidak Di Input'
                            })
                        }
                    }
                }
            );
    }

    async handleDetail(value) {
        this.setState({ modalDetail: true, detailGuru: value });
    }

    handleEdit(value) {
        this.setState({ modalEdit: true, detailGuru: value });
    }

    handleSessData(cond) {
        localStorage.removeItem('guru');
        var data = JSON.stringify(cond);
        localStorage.setItem('guru', data);
        //sessionStorage.setItem('idjurusan', cond)
    }

    render() {
        const { modalDetail, modalEdit, detailGuru } = this.state;

        return (
            <>
                <CRow>
                    <CCol sm="12" className="text-center">
                        <CCard>
                            <CCardBody>
                                <h3 id="traffic" className="card-title mb-0">List Guru</h3>
                            </CCardBody>
                        </CCard>
                    </CCol>
                    <CCol>
                        <CCard>
                            <CCardBody>
                                <CDataTable
                                    items={this.state.list_guru}
                                    fields={[{ key: 'nama' }, { key: 'NIP' }, { key: 'jenis_kelamin' }, { key: 'aksi' }]}
                                    hover
                                    sorter
                                    border
                                    itemsPerPage={10}
                                    pagination
                                    loading={this.state.loadData}
                                    scopedSlots={{
                                        'nama':
                                            (item) => (
                                                <td>
                                                    {item.nama}
                                                </td>
                                            ),
                                        'NIP':
                                            (item) => (
                                                <td>
                                                    {item.nip == null ?
                                                        <> - </>
                                                        :
                                                        <> { item.nip} </>
                                                    }
                                                </td>
                                            ),
                                        'jenis_kelamin':
                                            (item) => (
                                                <td>
                                                    {item.jenis_kelamin}
                                                </td>
                                            ),
                                        'aksi':
                                            (item) => (
                                                <td>
                                                    <CButton style={{ marginRight: "10px" }} color="success" to={{ pathname: `/manajemen_sekolah/master/list_guru/edit_guru`, edit_data: item }} onClick={() => this.handleSessData(item)}>
                                                        <FontAwesomeIcon icon={faUserEdit} />
                                                    </CButton>
                                                    <CButton color="primary" onClick={() => this.handleDetail(item)}>
                                                        <FontAwesomeIcon icon={faEye} />
                                                    </CButton>
                                                </td>
                                            )
                                    }}
                                />
                            </CCardBody>
                        </CCard>
                    </CCol>
                </CRow>

                <CModal
                    show={modalDetail}
                    onClose={() => this.setState({ modalDetail: false })}
                    color="primary"
                    size="lg"
                >
                    <CModalHeader>
                        <CModalTitle></CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                        <CRow>
                            {detailGuru != null ?
                                <>
                                    <CCol sm="3"></CCol>
                                    <CCol sm="4"></CCol>
                                    <CCol sm="5">Perubahan Terakhir : {formatDate(new Date(detailGuru.last_update), 'dd MMM yyyy HH:mm:ss')}</CCol>

                                    

                                    <CCol sm="12"> <hr /> </CCol>

                                    <CCol sm="3">NIP</CCol>
                                    <CCol sm="7">: {detailGuru.nip}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Jenis Kelamin</CCol>
                                    <CCol sm="7">: {detailGuru.jenis_kelamin}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Tempat, Tanggal Lahir</CCol>
                                    <CCol sm="7">: {detailGuru.tempat_lahir}, {Moment(detailGuru.tanggal_lahir).format('DD-MM-YYYY')}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">No. KK</CCol>
                                    <CCol sm="7">: {detailGuru.no_kk}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">NIK</CCol>
                                    <CCol sm="7">: {detailGuru.nik}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">NUPTK</CCol>
                                    <CCol sm="7">: {detailGuru.nuptk}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">NUKS</CCol>
                                    <CCol sm="7">: {detailGuru.nuks}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Status Kepegawaian</CCol>
                                    <CCol sm="7">: {detailGuru.status_kepegawaian}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Jenis PTK</CCol>
                                    <CCol sm="7">: {detailGuru.jenis_ptk}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Agama</CCol>
                                    <CCol sm="7">: {detailGuru.agama}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Lembaga Pengangkat</CCol>
                                    <CCol sm="7">: {detailGuru.lembaga_pengangkat}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Pangkat/Golongan</CCol>
                                    <CCol sm="7">: {detailGuru.pangkat_golongan}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Alamat</CCol>
                                    <CCol sm="7">: {detailGuru.alamat_jalan}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">RT/RW</CCol>
                                    <CCol sm="7">: {detailGuru.rt}/{detailGuru.rw}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Nama Dusun</CCol>
                                    <CCol sm="7">: {detailGuru.nama_dusun}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Kelurahan</CCol>
                                    <CCol sm="7">: {detailGuru.desa_kelurahan}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Kode Pos</CCol>
                                    <CCol sm="7">: {detailGuru.kode_pos}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">No. Telepon Rumah</CCol>
                                    <CCol sm="7">: {detailGuru.no_telepon_rumah}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">No. Telepon Seluler</CCol>
                                    <CCol sm="7">: {detailGuru.no_hp}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Email</CCol>
                                    <CCol sm="7">: {detailGuru.email}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">SK CPNS</CCol>
                                    <CCol sm="7">: {detailGuru.sk_cpns}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Tanggal CPNS</CCol>
                                    <CCol sm="7">: {detailGuru.tgl_cpns}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">SK Pengangkatan</CCol>
                                    <CCol sm="7">: {detailGuru.sk_pengangkatan}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Tamat Pengangkatan</CCol>
                                    <CCol sm="7">: {detailGuru.tmt_pengangkatan}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Nama Ibu Kandung</CCol>
                                    <CCol sm="7">: {detailGuru.nama_ibu_kandung}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Nama Suami/Istri</CCol>
                                    <CCol sm="7">: {detailGuru.nama_suami_istri}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">NIP Suami/Istri</CCol>
                                    <CCol sm="7">: {detailGuru.nip_suami_istri}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Pekerjaan Suami/Istri</CCol>
                                    <CCol sm="7">: {detailGuru.pekerjaan_suami_istri}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">Tamat PNS</CCol>
                                    <CCol sm="7">: {detailGuru.tmt_pns}</CCol>
                                    <CCol sm="2"></CCol>

                                    <CCol sm="3">NPWP</CCol>
                                    <CCol sm="7">: {detailGuru.npwp}</CCol>
                                    <CCol sm="2"></CCol>
                                </>
                                :
                                '-'
                            }
                        </CRow>
                        <br></br>
                    </CModalBody>
                    <CModalFooter>
                        <CButton color="danger" onClick={() => this.setState({ modalDetail: false })}>Tutup</CButton>
                    </CModalFooter>
                </CModal>
            </>
        )
    }
}

export default ListGuru
