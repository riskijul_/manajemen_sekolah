import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../../_services';
import { history } from '../../../_helpers';
import { apiGetPrasarana } from '../../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios';
import Select from 'react-select';

class ListRombel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
    };
  }

  componentDidMount() {
    this.getPrasarana();
  }

  async getPrasarana() {
    await userService.getData(apiGetPrasarana)
        .then(
            user => {
                this.setState({ list_prasarana: user.data.data, loadData: false })
            },
            error => {
                if (error.response) {
                    // if(error.response.status == 422){
                    //     this.setState({ 
                    //         statusRes: true,
                    //         conditionAlert: 'danger',
                    //         messageRes: 'NPSN Tidak Di Input'
                    //     })
                    // }
                }
            }
        );
}

  render() {
    const {  } = this.state;
    return (
      <>
        <CRow>
            <CCol sm="12" className="text-center">
                <CCard>
                    <CCardBody>
                        <h3 id="traffic" className="card-title mb-0">List Ruangan</h3>
                    </CCardBody>
                </CCard>
            </CCol>
          <CCol>
            <CCard>
                {/* <CCol sm="3" className="text-lg-right">
                  <Select
                    options={this.state.data_semester}
                    placeholder="Pilih Semester..."
                    onChange={(value) => this.getList(value.value)}
                  />
                </CCol> */}
              <CCardBody>  
                <hr style={{ border: "1px solid black" }}/>
                <h4 style={{ margin: "auto", marginLeft: "10px" }}>Pilih Jenis Prasarana</h4>
                <hr style={{ marginBottom: "20px", border: "1px solid black" }}/>

                <CDataTable
                    items={this.state.list_prasarana}
                    fields={[{key: 'jenis_prasarana' }, { key: 'jumlah' }, { key: 'aksi', sorter: false }]}
                    hover
                    border
                    itemsPerPage={10}
                    pagination
                    loading={this.state.isLoadData}
                    scopedSlots = {{
                    'jenis_prasarana':
                        (item)=>(
                        <td>
                            {item.jenis_prasarana}
                        </td>
                    ),
                    'jumlah':
                        (item)=>(
                        <td>
                            {item.jumlah_prasarana}
                        </td>
                    ),
                    'aksi':
                        (item)=>(
                        <td>
                            <CButton color="success" size="sm" to={{ pathname: `/manajemen_sekolah/master/list_prasarana/list_ruangan`, query: item.jenis_prasarana_id }}>PILIH</CButton>
                        </td>
                        )
                    }}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    )
  }
}

export default ListRombel
