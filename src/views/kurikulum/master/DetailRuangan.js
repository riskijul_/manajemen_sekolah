import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CInput
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../../_services';
import { history } from '../../../_helpers';
import { apiGetDetailRuangan } from '../../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios';
import Select from 'react-select';

class ListRombel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      params: this.props.location.query,
    };

    if (!this.state.params){
      this.props.history.push('/manajemen_sekolah/master/list_prasarana');
    }
  }

  

  componentDidMount() {
    this.getPrasarana();
  }

  async getPrasarana() {
    await userService.getData(apiGetDetailRuangan + this.state.params)
        .then(
            user => {
                this.setState({ list_ruangan: user.data.data, loadData: false })
            },
            error => {
                if (error.response) {
                    // if(error.response.status == 422){
                    //     this.setState({ 
                    //         statusRes: true,
                    //         conditionAlert: 'danger',
                    //         messageRes: 'NPSN Tidak Di Input'
                    //     })
                    // }
                }
            }
        );
}

  render() {
    const {  } = this.state;
    return (
      <>
        <CRow>
            <CCol sm="12" className="text-center">
                <CCard>
                    <CCardBody>
                        <h3 id="traffic" className="card-title mb-0">Detail Ruangan</h3>
                    </CCardBody>
                </CCard>
            </CCol>
          <CCol>
            <CCard>
                {/* <CCol sm="3" className="text-lg-right">
                  <Select
                    options={this.state.data_semester}
                    placeholder="Pilih Semester..."
                    onChange={(value) => this.getList(value.value)}
                  />
                </CCol> */}
              <CCardBody>

                <CDataTable
                    items={this.state.list_ruangan}
                    fields={[{key: 'nama_ruangan' }, { key: 'lantai' }, { key: 'aksi', sorter: false }]}
                    hover
                    border
                    itemsPerPage={10}
                    pagination
                    loading={this.state.isLoadData}
                    scopedSlots = {{
                    'nama_ruangan':
                      (item)=>(
                      <td>
                        <CInput value={ item.nm_ruang }/>
                      </td>
                    ),
                    'lantai':
                      (item)=>(
                      <td>
                        {item.lantai}
                      </td>
                    ),
                    'aksi':
                        (item)=>(
                        <td>

                        </td>
                        )
                    }}
                />

              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    )
  }
}

export default ListRombel
