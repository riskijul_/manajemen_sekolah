import React, { lazy } from 'react'
import {
  CButton,
  CCard,
  CAlert,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CDataTable,
  CForm,
  CInputRadio,
  CLabel,
  CFormGroup,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCollapse,
  CFade,
  CInput,
  CSelect,
  CBadge
} from '@coreui/react'
import { userService, authenticationService } from '../../../_services';
import { apiGetPesertaDidik, apiPutPesertaDidik } from '../../../_components';
// import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';
import Moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserEdit, faEye } from '@fortawesome/fontawesome-free-solid'

class ListPesertaDidik extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            params: this.props.location.query,
            paramsName: this.props.location.name,
            tp_id: this.props.location.tp_id,
            smstr_id: this.props.location.smstr_id,
            arrSmstr: [],
            idJurusan: localStorage.getItem('idjurusan'),
            idRombel: localStorage.getItem('id_rombel'),
            statusRes: false,
            modalWali: false,
            isLoadData: true,
            list_pd: [],
            pesdik_id: [],
            pesdik_name: [],
            row_id: [],
            modalInfo: false,
            collapseOne: true,
            collapseTwo: false,
            opsiChange: '1',
            conditionAlert: '',
            messageRes: '',
            valueNameClass:'',
            selectedRows: [],
            condEmpatThn: '',
            collapseNon: false,
            tp_idNaik: parseInt(this.props.location.tp_id) + 1,
            rombelNaik: [],
            jurusanIdNaik: null,
            PdArr: [],
            detailPD: null,
            data_pd: []
        };

        if (!localStorage.getItem('id_rombel')) {
            this.props.history.push('/manajemen_sekolah/master/list_rombel');
        }
        //console.log(this.state.arrSmstr)
    }

    componentDidMount() {
        this.getListPD();
    }

    componentWillUnmount(){
        //this.getRevJurusan();
    }

    async getListPD() {
        await userService.getData(apiGetPesertaDidik + this.state.idRombel)
            .then(
                user => {
                    this.setState({ list_peserta_didik: user.data.data })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ 
                                statusRes: true,
                                conditionAlert: 'danger',
                                messageRes: 'NPSN Tidak Di Input'
                            })
                        }
                    }
                }
            );
    }

    handleEdit(item){
        this.setState({ modalEdit: true, data_pd: item });
        console.log(this.state.data_pd);
    }

    async handleDetail(value){
        this.setState({modalDetail: true, detailPD: value});
    }

    handleEditInput(name,value){
        this.setState({...this.state.detailPD, id: value.target.value});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('nisn', event.target.elements.nisn.value);
        formData.append('nama', event.target.elements.nama.value);
        formData.append('nik', event.target.elements.nik.value);
        formData.append('tempat_lahir', event.target.elements.tempat_lahir.value);
        formData.append('tanggal_lahir', event.target.elements.tanggal_lahir.value);
        formData.append('agama', event.target.elements.agama.value);
        formData.append('jenis_kelamin', event.target.elements.jenis_kelamin.value);
        formData.append('alamat', event.target.elements.alamat.value);
        formData.append('rt', event.target.elements.rt.value);
        formData.append('rw', event.target.elements.rw.value);
        formData.append('nama_dusun', event.target.elements.nama_dusun.value);
        formData.append('desa_kelurahan', event.target.elements.desa_kelurahan.value);
        formData.append('kode_pos', event.target.elements.kode_pos.value);
        formData.append('nomor_telepon_rumah', event.target.elements.nomor_telepon_rumah.value);
        formData.append('nomor_telepon_seluler', event.target.elements.nomor_telepon_seluler.value);
        formData.append('email', event.target.elements.email.value);
        formData.append('nama_ayah', event.target.elements.nama_ayah.value);
        formData.append('nama_ibu', event.target.elements.nama_ibu.value);
        formData.append('nama_wali', event.target.elements.nama_wali.value);

        axios.put(apiPutPesertaDidik, formData, {
          headers: {
              'Authorization' : 'token=' + this.state.currentUser.token
          }
        })

        .then(res => {
            swal("Sukses!", "Berhasil Edit Data", "success");
            setTimeout(() => {
              this.props.history.push('/manajemen_sekolah/master/list_jurusan');
            }, 3000);
        })
        .catch(error => {
            if (error.response) {
                if(error.response.status == 422){
                  swal("Gagal!", "Input ada yang salah", "error");
                }else if(error.response.status == 401){
                  swal("Gagal!", "Unauthorized", "error");
                }else if(error.response.status == 409){
                  swal("Gagal!", "Duplikat Data", "error");
                }
            }
        });
    }

    handleSessData(cond){
        localStorage.removeItem('peserta_didik');
        var data = JSON.stringify(cond);
        localStorage.setItem('peserta_didik', data);
        //sessionStorage.setItem('idjurusan', cond)
    }

    render() {
        const { modalDetail, modalEdit, detailPD, data_pd} = this.state;
        
        return (
            <>
                <CRow>
                    <CCol sm="12" className="text-center">
                        <CCard>
                            <CCardBody>
                                <h3 id="traffic" className="card-title mb-0">Peserta Didik</h3>
                            </CCardBody>
                        </CCard>
                    </CCol>
                    <CCol>
                    <CCard>
                    <CCardBody>
                        <CDataTable
                            items={this.state.list_peserta_didik}
                            fields={[{key: 'NISN'}, {key: 'nama' }, {key: 'jenis_kelamin'}, {key: 'aksi'}]}
                            hover
                            sorter
                            border
                            itemsPerPage={10}
                            pagination
                            scopedSlots = {{
                            'NISN':
                                (item)=>(
                                    <td>
                                        {item.nisn}
                                    </td>
                            ),
                            'nama_rombel':
                                (item)=>(
                                <td>
                                    {item.nama}
                                </td>
                            ),
                            'jenis_kelamin':
                            (item)=>(
                                <td>
                                    {item.jenis_kelamin}
                                </td>
                            ),
                            // 'tempat, tanggal_lahir':
                            // (item)=>(
                            //     <td>
                            //         {item.tempat_lahir}, {Moment(item.tanggal_lahir).format("DD/MM/YYYY")}
                            //     </td>
                            // ),
                            'aksi':
                                (item)=>(
                                <td>
                                    <CButton color="success" style={{ marginRight: '10px' }} to={{ pathname: `/manajemen_sekolah/master/list_jurusan/list_rombel/list_peserta_didik/edit_peserta_didik`, edit_data: item }} onClick={() => this.handleSessData(item)} >
                                        <FontAwesomeIcon icon={faUserEdit}/>
                                    </CButton>
                                    <CButton color="primary" onClick={() => this.handleDetail(item) }>
                                        <FontAwesomeIcon icon={faEye}/>
                                    </CButton>
                                </td>
                                )
                            }}
                        />
                    </CCardBody>
                    </CCard>
                </CCol>
                </CRow>

                <CModal 
                    show={modalDetail} 
                    onClose={() => this.setState( { modalDetail: false }) }
                    color="primary"
                    size="lg"
                    >
                    <CModalHeader>
                        <CModalTitle>Detail Peserta Didik</CModalTitle>
                    </CModalHeader>
                    <CModalBody>
                        <CRow>
                            {detailPD != null ?
                            <>
                                <CCol sm="3">NISN</CCol>
                                <CCol sm="7">: { detailPD.nisn }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Nama</CCol>
                                <CCol sm="7">: { detailPD.nama }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">NIK</CCol>
                                <CCol sm="7">: { detailPD.nik }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Tempat, Tanggal Lahir</CCol>
                                <CCol sm="7">: { detailPD.tempat_lahir }, { Moment(detailPD.tanggal_lahir).format("DD-MM-YYYY") }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Agama</CCol>
                                <CCol sm="7">: { detailPD.agama }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Jenis Kelamin</CCol>
                                <CCol sm="7">: { detailPD.jenis_kelamin }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Alamat</CCol>
                                <CCol sm="7">: { detailPD.alamat_jalan }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">RT/RW</CCol>
                                <CCol sm="7">: { detailPD.rt, '/', detailPD.rw }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Nama Dusun</CCol>
                                <CCol sm="7">: { detailPD.nama_dusun }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Desa Kelurahan</CCol>
                                <CCol sm="7">: { detailPD.desa_kelurahan }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Kode Pos</CCol>
                                <CCol sm="7">: { detailPD.kode_pos }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">No. Telepon Rumah</CCol>
                                <CCol sm="7">: { detailPD.nomor_telepon_rumah,'/',detailPD.nomor_telepon_seluler }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">No. Telepon Seluler (HP)</CCol>
                                <CCol sm="7">: { detailPD.nomor_telepon_seluler }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Email</CCol>
                                <CCol sm="7">: { detailPD.email }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Nama Ayah</CCol>
                                <CCol sm="7">: { detailPD.nama_ayah }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Nama Ibu</CCol>
                                <CCol sm="7">: { detailPD.nama_ibu_kandung }</CCol>
                                <CCol sm="2"></CCol>

                                <CCol sm="3">Nama Wali</CCol>
                                <CCol sm="7">: { detailPD.nama_wali }</CCol>
                                <CCol sm="2"></CCol>
                            </>
                            :
                                '-'
                            }
                        </CRow>
                    </CModalBody>
                    <CModalFooter>
                        <CButton color="danger" onClick={() => this.setState( { modalDetail: false }) }>Tutup</CButton>
                    </CModalFooter>
                </CModal>
            </>
        )
    }
}

export default ListPesertaDidik
