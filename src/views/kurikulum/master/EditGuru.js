import React, { lazy } from 'react'
import {
    CButton,
    CCard,
    CAlert,
    CCardBody,
    CCardHeader,
    CCol,
    CProgress,
    CRow,
    CDataTable,
    CForm,
    CInputRadio,
    CLabel,
    CFormGroup,
    CInput,
    CSelect,
    CTextarea,
    CCardFooter,
    CInvalidFeedback
} from '@coreui/react'
import { userService, authenticationService } from '../../../_services';
import { apiPutGuru, apiGetPangkatGol, apiGetAgama, apiGetJenisPTK, apiGetLembagaPengangkat, apiGetStatusKepegawaian, apiGetPekerjaan, apiGetPerkawinan } from '../../../_components';
// import { MdSubdirectoryArrowRight } from 'react-icons/md';
import axios from 'axios'
import Select from 'react-select';
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';
import Moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from '@fortawesome/fontawesome-free-solid';
import DatePicker from 'react-date-picker';
import { formatDate } from '../../../_helpers/date_format';

class EditGuru extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue.data,
            params: JSON.parse(localStorage.getItem('guru')),
            list_pangkat_golongan: [],
            list_agama: [],
            list_ptk: [],
            list_lembaga_pengangkat: [],
            list_status_kepegawaian: [],
            list_pekerjaan: [],
            list_status_perkawinan: [],
            nama: JSON.parse(localStorage.getItem('guru')).nama,
            nip: JSON.parse(localStorage.getItem('guru')).nip,
            jenis_kelamin: JSON.parse(localStorage.getItem('guru')).jenis_kelamin,
            tempat_lahir: JSON.parse(localStorage.getItem('guru')).tempat_lahir,
            tanggal_lahir: JSON.parse(localStorage.getItem('guru')).tanggal_lahir,
            nik: JSON.parse(localStorage.getItem('guru')).nik,
            no_kk: JSON.parse(localStorage.getItem('guru')).no_kk,
            nuptk: JSON.parse(localStorage.getItem('guru')).nuptk,
            nuks: JSON.parse(localStorage.getItem('guru')).nuptk,
            status_kepegawaian_id: JSON.parse(localStorage.getItem('guru')).status_kepegawaian_id,
            status_kepegawaian: JSON.parse(localStorage.getItem('guru')).status_kepegawaian,
            jenis_ptk_id: JSON.parse(localStorage.getItem('guru')).jenis_ptk_id,
            jenis_ptk: JSON.parse(localStorage.getItem('guru')).jenis_ptk,
            agama_id: JSON.parse(localStorage.getItem('guru')).agama_id,
            agama: JSON.parse(localStorage.getItem('guru')).agama,
            lembaga_pengangkat_id: JSON.parse(localStorage.getItem('guru')).lembaga_pengangkat_id,
            lembaga_pengangkat: JSON.parse(localStorage.getItem('guru')).lembaga_pengangkat,
            pangkat_golongan_id: JSON.parse(localStorage.getItem('guru')).pangkat_golongan_id,
            pangkat_golongan: JSON.parse(localStorage.getItem('guru')).pangkat_golongan,
            alamat_jalan: JSON.parse(localStorage.getItem('guru')).alamat_jalan,
            rt: JSON.parse(localStorage.getItem('guru')).rt,
            rw: JSON.parse(localStorage.getItem('guru')).rw,
            nama_dusun: JSON.parse(localStorage.getItem('guru')).nama_dusun,
            desa_kelurahan: JSON.parse(localStorage.getItem('guru')).desa_kelurahan,
            kode_pos: JSON.parse(localStorage.getItem('guru')).kode_pos,
            nomor_telepon_rumah: JSON.parse(localStorage.getItem('guru')).nomor_telepon_rumah,
            no_hp: JSON.parse(localStorage.getItem('guru')).no_hp,
            email: JSON.parse(localStorage.getItem('guru')).email,
            sk_cpns: JSON.parse(localStorage.getItem('guru')).sk_cpns,
            tgl_cpns: JSON.parse(localStorage.getItem('guru')).tgl_cpns,
            sk_pengangkatan: JSON.parse(localStorage.getItem('guru')).sk_pengangkatan,
            tmt_pengangkatan: JSON.parse(localStorage.getItem('guru')).tmt_pengangkatan,
            nama_ibu_kandung: JSON.parse(localStorage.getItem('guru')).nama_ibu_kandung,
            status_perkawinan_id: JSON.parse(localStorage.getItem('guru')).status_perkawinan_id,
            status_perkawinan: JSON.parse(localStorage.getItem('guru')).status_perkawinan,
            nama_suami_istri: JSON.parse(localStorage.getItem('guru')).nama_suami_istri,
            nip_suami_istri: JSON.parse(localStorage.getItem('guru')).nip_suami_istri,
            pekerjaan_id: JSON.parse(localStorage.getItem('guru')).pekerjaan_id,
            pekerjaan_suami_istri: JSON.parse(localStorage.getItem('guru')).pekerjaan_suami_istri,
            tmt_pns: JSON.parse(localStorage.getItem('guru')).tmt_pns,
            npwp: JSON.parse(localStorage.getItem('guru')).npwp
        };
    }

    componentDidMount() {
        // this.getListPD();
        this.getPangkatGol();
        this.getAgama();
        this.getLembagaPengangkat();
        this.getJenisPTK();
        this.getStatusKepegawaian();
        this.getPekerjaan();
        this.getStatusPerkawinan();
    }

    componentWillUnmount() {
        //this.getRevJurusan();
    }

    getAgama() {
        userService.getData(apiGetAgama)
            .then(response => {
                this.setState({ list_agama: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    getLembagaPengangkat() {
        userService.getData(apiGetLembagaPengangkat)
            .then(response => {
                this.setState({ list_lembaga_pengangkat: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    getJenisPTK() {
        userService.getData(apiGetJenisPTK)
            .then(response => {
                this.setState({ list_ptk: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    getPangkatGol() {
        userService.getData(apiGetPangkatGol)
            .then(response => {
                this.setState({ list_pangkat_golongan: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    getStatusKepegawaian() {
        userService.getData(apiGetStatusKepegawaian)
            .then(response => {
                this.setState({ list_status_kepegawaian: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    getPekerjaan() {
        userService.getData(apiGetPekerjaan)
            .then(response => {
                this.setState({ list_pekerjaan: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    getStatusPerkawinan() {
        userService.getData(apiGetPerkawinan)
            .then(response => {
                this.setState({ list_status_perkawinan: response.data.data })
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                    }
                }
            });
    }

    handleTanggalLahir(event) {
        const date_format = formatDate(new Date(event), 'yyyy-MM-dd');
        this.setState({ tanggal_lahir: date_format });
    }

    handleTanggalCPNS(event) {
        const date_format = formatDate(new Date(event), 'yyyy-MM-dd');
        this.setState({ tgl_cpns: date_format });
    }

    handleTamatPengangkatan(event) {
        const date_format = formatDate(new Date(event), 'yyyy-MM-dd');
        this.setState({ tmt_pengangkatan: date_format });
    }

    handleTamatPNS(event) {
        const date_format = formatDate(new Date(event), 'yyyy-MM-dd');
        this.setState({ tmt_pns: date_format });
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const id = target.id;

        this.setState({
            [id]: value,
        })

        // switch(id){
        //   case 'nip': 
        //       // if(value.length > 18){
        //       //   console.log('anjim');
        //       // }
        //       this.validate_input(id);
        //     break;
        // }

    }

    validate_input(value) {
        // check if input is bigger than 3
        if (value.length > 18) {
            return false; // keep form from submitting
        }

        // else form is good let it submit, of course you will 
        // probably want to alert the user WHAT went wrong.

        return true;
    }

    handleSubmit = (event) => {
        event.preventDefault();
        var formData = {
            ptk_id: JSON.parse(localStorage.getItem('guru')).ptk_id,
            nama: event.target.elements.nama.value,
            nip: event.target.elements.nip.value,
            jenis_kelamin: event.target.elements.jenis_kelamin.value,
            tempat_lahir: event.target.elements.tempat_lahir.value,
            tanggal_lahir: this.state.tanggal_lahir,
            nik: event.target.elements.nik.value,
            no_kk: event.target.elements.no_kk.value,
            nuptk: event.target.elements.nuptk.value,
            nuks: event.target.elements.nuptk.value,
            status_kepegawaian: event.target.elements.status_kepegawaian.value,
            jenis_ptk: event.target.elements.jenis_ptk.value,
            agama: event.target.elements.agama.value,
            lembaga_pengangkat: event.target.elements.lembaga_pengangkat.value,
            pangkat_golongan: event.target.elements.pangkat_golongan.value,
            alamat_jalan: event.target.elements.alamat_jalan.value,
            rt: event.target.elements.rt.value,
            rw: event.target.elements.rw.value,
            nama_dusun: event.target.elements.nama_dusun.value,
            desa_kelurahan: event.target.elements.desa_kelurahan.value,
            kode_pos: event.target.elements.kode_pos.value,
            nomor_telepon_rumah: event.target.elements.nomor_telepon_rumah.value,
            no_hp: event.target.elements.no_hp.value,
            email: event.target.elements.email.value,
            sk_cpns: event.target.elements.sk_cpns.value,
            tgl_cpns: this.state.tgl_cpns,
            sk_pengangkatan: event.target.elements.sk_pengangkatan.value,
            tmt_pengangkatan: this.state.tmt_pengangkatan,
            nama_ibu_kandung: event.target.elements.nama_ibu_kandung.value,
            status_perkawinan: event.target.elements.status_perkawinan.value,
            nama_suami_istri: event.target.elements.nama_suami_istri.value,
            nip_suami_istri: event.target.elements.nip_suami_istri.value,
            pekerjaan_suami_istri: event.target.elements.pekerjaan_suami_istri.value,
            tmt_pns: this.state.tmt_pns,
            npwp: event.target.elements.npwp.value
        }

        console.log(formData);

        axios.put(apiPutGuru, formData, {
            headers: {
                'Authorization': 'token=' + this.state.currentUser.token
            }
        })

            .then(res => {
                swal("Sukses!", "Berhasil Melakukan Perubahan", "success");
                this.props.history.push('/manajemen_sekolah/master/list_guru');
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422) {
                        swal("Gagal!", "Input ada yang salah", "error");
                        error.response.data.errors.map((x, key) => {
                            switch (x.param) {
                                case 'nip': {
                                    return (
                                        this.setState({ nip_err: x.msg, color_nip: '#d93025', color: '#d93025', invalid: false })
                                    )
                                }
                                case 'nik': {
                                    return (
                                        this.setState({ nik_err: x.msg, color_nik: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'no_kk': {
                                    return (
                                        this.setState({ no_kk_err: x.msg, color_no_kk: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'nuptk': {
                                    return (
                                        this.setState({ nuptk_err: x.msg, color_nuptk: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'nuks': {
                                    return (
                                        this.setState({ nuks_err: x.msg, color_nuks: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'nama': {
                                    return (
                                        this.setState({ nama_err: x.msg, color_nama: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'tanggal_lahir': {
                                    return (
                                        this.setState({ tanggal_lahir_err: x.msg, color_tanggal_lahir: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'tempat_lahir': {
                                    return (
                                        this.setState({ tempat_lahir_err: x.msg, color_tempat_lahir: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'alamat_jalan': {
                                    return (
                                        this.setState({ alamat_jalan_err: x.msg, color_alamat_jalan: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'rt': {
                                    return (
                                        this.setState({ rt_err: x.msg, color_rt: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'rw': {
                                    return (
                                        this.setState({ rw_err: x.msg, color_rw: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'nama_dusun': {
                                    return (
                                        this.setState({ nama_dusun_err: x.msg, color_nama_dusun: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'desa_kelurahan': {
                                    return (
                                        this.setState({ desa_kelurahan_err: x.msg, color_desa_kelurahan: '#d93025', color: '#d93025',invalid: false })
                                    )
                                }
                                case 'kode_pos': {
                                    return (
                                        this.setState({ kode_pos_err: x.msg, color_kode_pos: '#d93025', color: '#d93025', invalid: false })
                                    )
                                }
                                case 'npwp': {
                                    return (
                                        this.setState({ npwp_err: x.msg, color_npwp: '#d93025', color: '#d93025', invalid: false })
                                    )
                                }
                                case 'email': {
                                    return (
                                        this.setState({ email_err: x.msg, color_email: '#d93025', color: '#d93025', invalid: false })
                                    )
                                }
                                case 'no_hp': {
                                    return (
                                        this.setState({ no_hp_err: x.msg, color_no_hp: '#d93025', color: '#d93025', invalid: false })
                                    )
                                }
                            }
                        });
                    } else if (error.response.status == 401) {
                        swal("Gagal!", "Unauthorized", "error");
                    } else if (error.response.status == 409) {
                        swal("Gagal!", "Duplikat Data", "error");
                    }
                }
            });
    }

    render() {
        const { } = this.state;

        return (
            <>
                <CForm onSubmit={this.handleSubmit}>
                    <CCard style={{ boxShadow: "3px 3px 3px gray" }}>
                        <CCardBody>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">NIP : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nip} id="nip" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_nip }} />
                                        <small style={{ color: this.state.color }}>{this.state.nip_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">No. KK : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.no_kk} id="no_kk" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_no_kk }} />
                                        <small style={{ color: this.state.color }}>{this.state.no_kk_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">NIK : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nik} id="nik" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_nik }} />
                                        <small style={{ color: this.state.color }}>{this.state.nik_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">NUPTK : </CLabel>
                                    <div className="controls">
                                        <CInput invalid={this.state.invalid} name="" value={this.state.nuptk} id="nuptk" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_nuptk }} />
                                        <small style={{ color: this.state.color }}>{this.state.nuptk_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">NUKS : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nuks} id="nuks" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_nuks }} />
                                        <small style={{ color: this.state.color }}>{this.state.nuks_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">NPWP : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.npwp} id="npwp" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_npwp }} />
                                        <small style={{ color: this.state.color }}>{this.state.npwp_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                        </CCardBody>
                    </CCard>
                    <CCard style={{ boxShadow: "3px 3px 3px gray" }}>
                        <CCardBody>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama} id="nama" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_nama }}/>
                                        <small style={{ color: this.state.color }}>{this.state.nama_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Jenis Kelamin : </CLabel>
                                    <div className="controls">
                                        <CSelect id="jenis_kelamin" style={{ borderColor: this.state.color_jenis_kelamin }}>
                                            {this.state.jenis_kelamin == 'L' ?
                                                <>
                                                    <option value={this.state.jenis_kelamin}>{this.state.jenis_kelamin}</option>
                                                    <option value="P">P</option>
                                                </>
                                                : this.state.jenis_kelamin == 'P' ?
                                                    <>
                                                        <option value={this.state.jenis_kelamin}>{this.state.jenis_kelamin}</option>
                                                        <option value="L">L</option>
                                                    </>
                                                    : <></>
                                            }
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Tempat Lahir : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.tempat_lahir} id="tempat_lahir" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_tempat_lahir }} />
                                        <small style={{ color: this.state.color }}>{this.state.tempat_lahir_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Tanggal Lahir : </CLabel>
                                    <div className="controls">
                                        <DatePicker
                                            id="tanggal_lahir"
                                            onChange={(event) => this.handleTanggalLahir(event)}
                                            value={new Date(this.state.tanggal_lahir)}
                                            clearIcon={null}
                                            style={{ borderColor: this.state.color_tanggal_lahir }}
                                        />
                                    </div>
                                    <small style={{ color: 'green' }}>Format : Bulan/Tanggal/Tahun</small>
                                    <small style={{ color: this.state.color }}>{this.state.tanggal_lahir_err}</small>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Status Kepegawaian : </CLabel>
                                    <div className="controls">
                                        <CSelect id="status_kepegawaian" style={{ borderColor: this.state.color_status_kepegawaian }}>
                                            <option defaultValue={this.state.status_kepegawaian_id} value={this.state.status_kepegawaian_id} onChange={(event) => this.handleChange(event)}>{this.state.status_kepegawaian} (Status Kepegawaian Asal)</option>
                                            {this.state.list_status_kepegawaian.map((item, i) => {
                                                return (
                                                    <option key={i} value={item.status_kepegawaian_id} onChange={(event) => this.handleChange(event)}>
                                                        {item.nama}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Jenis PTK : </CLabel>
                                    <div className="controls">
                                        <CSelect id="jenis_ptk" style={{ borderColor: this.state.color_jenis_ptk }}>
                                            <option defaultValue={this.state.jenis_ptk_id} value={this.state.jenis_ptk_id} onChange={(event) => this.handleChange(event)}>{this.state.jenis_ptk} (Jenis PTK Asal)</option>
                                            {this.state.list_ptk.map((item, i) => {
                                                return (
                                                    <option key={i} value={item.jenis_ptk_id} onChange={(event) => this.handleChange(event)}>
                                                        {item.jenis_ptk}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Agama : </CLabel>
                                    <div className="controls">
                                        <CSelect id="agama" style={{ borderColor: this.state.color_agama }}>
                                            <option defaultValue={this.state.agama_id} value={this.state.agama_id} onChange={(event) => this.handleChange(event)}>{this.state.agama} (Agama Asal)</option>
                                            {this.state.list_agama.map((item, i) => {
                                                return (
                                                    <option key={i} value={item.agama_id} onChange={(event) => this.handleChange(event)}>
                                                        {item.nama}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Lembaga Pengangkat : </CLabel>
                                    <div className="controls">
                                        <CSelect id="lembaga_pengangkat" style={{ borderColor: this.state.color_lembaga_pengangkat }}>
                                            <option defaultValue={this.state.lembaga_pengangkat_id} value={this.state.lembaga_pengangkat_id} onChange={(event) => this.handleChange(event)}>{this.state.lembaga_pengangkat} (Lembaga Pengangkat Asal)</option>
                                            {this.state.list_lembaga_pengangkat.map((item, i) => {
                                                return (
                                                    <option key={i} value={item.lembaga_pengangkat_id} onChange={(event) => this.handleChange(event)}>
                                                        {item.nama}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Pangkat/Golongan : </CLabel>
                                    <div className="controls">
                                        <CSelect id="pangkat_golongan" style={{ borderColor: this.state.color_pangkat_golongan }}>
                                            <option defaultValue={this.state.pangkat_golongan_id} value={this.state.pangkat_golongan_id} onChange={(event) => this.handleChange(event)}>{this.state.pangkat_golongan} (Pangkat/Golongan Asal)</option>
                                            {this.state.list_pangkat_golongan.map((item, i) => {
                                                return (
                                                    <option key={i} value={item.pangkat_golongan_id} onChange={(event) => this.handleChange(event)}>
                                                        {item.nama}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Alamat : </CLabel>
                                    <div className="controls">
                                        <CTextarea id="alamat_jalan" value={this.state.alamat_jalan} onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_alamat_jalan }}>{this.state.alamat_jalan}</CTextarea>
                                        <small style={{ color: this.state.color }}>{this.state.alamat_jalan_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">RT/RW : </CLabel>
                                    <div className="controls">
                                        <CRow>
                                            <CCol sm="3">
                                                <CInput name="" value={this.state.rt} id="rt" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_rt }}/>
                                                <small style={{ color: this.state.color }}>{this.state.rt_err}</small>
                                            </CCol>
                                            <CCol sm="3">
                                                <CInput className="number" name="" value={this.state.rw} id="rw" onChange={(event) => this.handleChange(event)} style={{ borderColor: this.state.color_rw }} />
                                                <small style={{ color: this.state.color }}>{this.state.rw_err}</small>
                                            </CCol>
                                        </CRow>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama Dusun : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama_dusun} id="nama_dusun" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_nama_dusun }}/>
                                        <small style={{ color: this.state.color }}>{this.state.nama_dusun_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Desa Kelurahan : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.desa_kelurahan} id="desa_kelurahan" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_desa_kelurahan }}/>
                                        <small style={{ color: this.state.color }}>{this.state.desa_kelurahan_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Kode Pos : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.kode_pos} id="kode_pos" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_kode_pos }}/>
                                        <small style={{ color: this.state.color }}>{this.state.kode_pos_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">No. Telepon Rumah : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nomor_telepon_rumah} id="nomor_telepon_rumah" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_nomor_telepon_rumah }}/>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">No. Telepon Seluler (HP) : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.no_hp} id="no_hp" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_no_hp }}/>
                                        <small style={{ color: this.state.color }}>{this.state.no_hp_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Email : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.email} id="email" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_email }}/>
                                        <small style={{ color: this.state.color }}>{this.state.email_err}</small>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">SK CPNS : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.sk_cpns} id="sk_cpns" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_sk_cpns }}/>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Tanggal CPNS : </CLabel>
                                    <div className="controls">
                                        <DatePicker
                                            id="tgl_cpns"
                                            onChange={(event) => this.handleTanggalCPNS(event)}
                                            value={new Date(this.state.tgl_cpns)}
                                            clearIcon={null}
                                            style={{ borderColor: this.state.color_tgl_cpns }}
                                        />
                                    </div>
                                    <small style={{ color: 'green' }}>Format : Bulan/Tanggal/Tahun</small>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">SK Pengangkatan : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.sk_pengangkatan} id="sk_pengangkatan" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_sk_pengangkatan }}/>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Tamat Pengangkatan : </CLabel>
                                    <div className="controls">
                                        <DatePicker
                                            id="tmt_pengangkatan"
                                            onChange={(event) => this.handleTamatPengangkatan(event)}
                                            value={new Date(this.state.tmt_pengangkatan)}
                                            clearIcon={null}
                                            style={{ borderColor: this.state.color_tmt_pengangkatan }}
                                        />
                                    </div>
                                    <small style={{ color: 'green' }}>Format : Bulan/Tanggal/Tahun</small>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama Ibu Kandung : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama_ibu_kandung} id="nama_ibu_kandung" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_ }}/>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Status Perkawinan : </CLabel>
                                    <div className="controls">
                                        <CSelect id="status_perkawinan" style={{ borderColor: this.state.color_ }}>
                                            <option defaultValue={this.state.status_perkawinan_id} value={this.state.status_perkawinan_id} onChange={(event) => this.handleChange(event)}>{this.state.status_perkawinan} (Status Perkawinan Asal)</option>
                                            {this.state.list_status_perkawinan.map((item, i) => {
                                                return (
                                                    <option key={i} value={item.status_perkawinan_id} onChange={(event) => this.handleChange(event)}>
                                                        {item.status_perkawinan}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Nama Suami/Istri : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nama_suami_istri} id="nama_suami_istri" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_ }}/>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">NIP Suami/Istri : </CLabel>
                                    <div className="controls">
                                        <CInput name="" value={this.state.nip_suami_istri} id="nip_suami_istri" onChange={(event) => this.handleChange(event)} autoComplete="off" style={{ borderColor: this.state.color_ }} />
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Pekerjaan Suami/Istri : </CLabel>
                                    <div className="controls">
                                        <CSelect id="pekerjaan_suami_istri" style={{ borderColor: this.state.color_ }}>
                                            <option defaultValue={this.state.pekerjaan_id} value={this.state.pekerjaan_id} onChange={(event) => this.handleChange(event)}>{this.state.pekerjaan_suami_istri} (Pekerjaan Asal)</option>
                                            {this.state.list_pekerjaan.map((item, i) => {
                                                return (
                                                    <option key={i} value={item.pekerjaan_id} onChange={(event) => this.handleChange(event)}>
                                                        {item.nama}
                                                    </option>
                                                )
                                            })}
                                        </CSelect>
                                    </div>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <CLabel htmlFor="appendedInput">Tamat PNS : </CLabel>
                                    <div className="controls">
                                        <DatePicker
                                            id="tmt_pns"
                                            onChange={(event) => this.handleTamatPNS(event)}
                                            value={new Date(this.state.tmt_pns)}
                                            clearIcon={null}
                                            style={{ borderColor: this.state.color_ }}
                                        />
                                    </div>
                                    <small style={{ color: 'green' }}>Format : Bulan/Tanggal/Tahun</small>
                                </CCol>
                            </CFormGroup>

                            <CFormGroup>
                                <CCol sm="12" md="12" lg="5">
                                    <div className="text-right">
                                        <CButton color="success" style={{ marginRight: '10px' }} type="submit"><FontAwesomeIcon icon={faSave} /> Simpan</CButton>
                                        <CButton color="danger" to={{ pathname: `/manajemen_sekolah/master/list_guru/` }}>Batal</CButton>
                                    </div>
                                </CCol>
                            </CFormGroup>

                        </CCardBody>
                    </CCard>
                </CForm>
            </>
        )
    }
}

export default EditGuru
