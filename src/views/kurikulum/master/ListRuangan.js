import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CModal,
  CModalTitle,
  CModalHeader,
  CModalBody,
  CModalFooter
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../../_services';
import { history } from '../../../_helpers';
import { apiGetRuangan } from '../../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios';
import Select from 'react-select';

class ListRombel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      params: this.props.location.query,
      detailRuangan: null
    };

    if (!this.state.params){
      this.props.history.push('/manajemen_sekolah/master/list_prasarana');
    }
  }

  

  componentDidMount() {
    this.getPrasarana();
  }

  async getPrasarana() {
    await userService.getData(apiGetRuangan + this.state.params)
        .then(
            user => {
                this.setState({ list_ruangan: user.data.data, loadData: false })
            },
            error => {
                if (error.response) {
                    // if(error.response.status == 422){
                    //     this.setState({ 
                    //         statusRes: true,
                    //         conditionAlert: 'danger',
                    //         messageRes: 'NPSN Tidak Di Input'
                    //     })
                    // }
                }
            }
        );
  }

  handleDetail(value){
    this.setState({ modalDetail: true, detailRuangan: value });
  }

  render() {
    const { modalDetail, detailRuangan } = this.state;
    return (
      <>
        <CRow>
            <CCol sm="12" className="text-center">
                <CCard>
                    <CCardBody>
                        <h3 id="traffic" className="card-title mb-0">List Siswa</h3>
                    </CCardBody>
                </CCard>
            </CCol>
          <CCol>
            <CCard>
                {/* <CCol sm="3" className="text-lg-right">
                  <Select
                    options={this.state.data_semester}
                    placeholder="Pilih Semester..."
                    onChange={(value) => this.getList(value.value)}
                  />
                </CCol> */}
              <CCardBody>

                <CDataTable
                    items={this.state.list_ruangan}
                    fields={[{key: 'nama_ruangan' }, { key: 'lantai' }, { key: 'aksi', sorter: false }]}
                    hover
                    border
                    itemsPerPage={10}
                    pagination
                    loading={this.state.isLoadData}
                    scopedSlots = {{
                    'nama_ruangan':
                      (item)=>(
                      <td>
                        {item.nm_ruang}
                      </td>
                    ),
                    'lantai':
                      (item)=>(
                      <td>
                        {item.lantai}
                      </td>
                    ),
                    'aksi':
                        (item)=>(
                        <td>
                          {/* <CButton color="success" size="sm" style={{marginRight: '10px' }} to={{ pathname: `/manajemen_sekolah/master/list_prasarana/list_ruangan/detail_ruangan`, query: item.id_ruang }}>EDIT</CButton> */}
                          <CButton color="info" size="sm" onClick={() => this.handleDetail(item) }>DETAIL</CButton>
                        </td>
                        )
                    }}
                />

              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

        <CModal 
          show={modalDetail} 
          onClose={() => this.setState( { modalDetail: false }) }
          color="info"
          size="lg"
        >
          <CModalHeader>
            <CModalTitle>Detail Ruangan</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CRow>
              {detailRuangan != null ?
                <>
                  <CCol sm="3">Jenis Prasarana</CCol>
                  <CCol sm="7">:
                    { detailRuangan.jenis_prasarana != null ?
                    <> { detailRuangan.jenis_prasarana } </>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Nama Ruangan</CCol>
                  <CCol sm="7">: { detailRuangan.nm_ruang }</CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Lantai</CCol>
                  <CCol sm="7">: { detailRuangan.lantai }</CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Panjang/Lebar Ruangan</CCol>
                  <CCol sm="7">: { detailRuangan.panjang }/{ detailRuangan.lebar } m</CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Registrasi Prasarana</CCol>
                  <CCol sm="7">:
                    { detailRuangan.reg_pras != null ?
                    <> { detailRuangan.reg_pras } </>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Kapasitas</CCol>
                  <CCol sm="7">:
                    { detailRuangan.kapasitas != null ?
                    <> { detailRuangan.kapasitas } </> 
                    :
                    <> - </>
                  }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Ruangan</CCol>
                  <CCol sm="7">:
                    { detailRuangan.luas_ruang != null ?
                    <> { detailRuangan.luas_ruang } </>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>
                  
                  <CCol sm="3">Luas Plester</CCol>
                  <CCol sm="7">:
                    { detailRuangan.luas_plester_m2 != null ?
                    <> { detailRuangan.luas_plester_m2 } m<sup>2</sup> </>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Plafon</CCol>
                  <CCol sm="7">:
                    { detailRuangan.luas_plafon_m2 != null ?
                    <> { detailRuangan.luas_plafon_m2 } m<sup>2</sup> </>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Dinding</CCol>
                  <CCol sm="7">:
                    { detailRuangan.luas_dinding_m2 != null ?
                    <> { detailRuangan.luas_dinding_m2 } m<sup>2</sup> </>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Daun Jendela</CCol>
                  <CCol sm="7">: 
                    { detailRuangan.luas_daun_jendela_m2 != null ?
                    <> { detailRuangan.luas_daun_jendela_m2 } m<sup>2</sup> </>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Daun Pintu</CCol>
                  <CCol sm="7">:
                    { detailRuangan.luas_daun_pintu_m2 != null ?
                    <> { detailRuangan.luas_daun_pintu_m2 } m<sup>2</sup></>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Panjang Kusen</CCol>
                  <CCol sm="7">:
                    { detailRuangan.panj_kusen_m != null ?
                    <> { detailRuangan.panj_kusen_m } m</>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Tutup Lantai</CCol>
                  <CCol sm="7">:
                    { detailRuangan.luas_tutup_lantai_m2 != null ?
                    <> { detailRuangan.luas_tutup_lantai_m2 } m<sup>2</sup></>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Panjang Instalasi Listrik</CCol>
                  <CCol sm="7">:
                    { detailRuangan.panj_inst_listrik_m != null ?
                    <> { detailRuangan.panj_inst_listrik_m } m</>
                    :
                    <> - </>
                    }  
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Jumlah Instalasi Listrik</CCol>
                  <CCol sm="7">:
                    { detailRuangan.jml_inst_listrik != null ?
                    <> { detailRuangan.jml_inst_listrik } </>
                    :
                    <> - </>
                    }  
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Panjang Instalasi Air</CCol>
                  <CCol sm="7">:
                    { detailRuangan.panj_inst_air_m != null ?
                    <> { detailRuangan.panj_inst_air_m } m</>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Jumlah Instalasi Air</CCol>
                  <CCol sm="7">:
                    { detailRuangan.jml_inst_air != null ?
                    <> { detailRuangan.jml_inst_air } </>
                    :
                    <> - </>
                    }
                  </CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Panjang Drainase</CCol>
                  <CCol sm="7">: { detailRuangan.panj_drainase_m } m</CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Keseluruhan Struktur</CCol>
                  <CCol sm="7">: { detailRuangan.luas_finish_struktur_m2 } m<sup>2</sup></CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Keseluruhan Plafon </CCol>
                  <CCol sm="7">: { detailRuangan.luas_finish_plafon_m2 } m<sup>2</sup></CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Keseluruhan Dinding</CCol>
                  <CCol sm="7">: { detailRuangan.luas_finish_dinding } m<sup>2</sup></CCol>
                  <CCol sm="2"></CCol>

                  <CCol sm="3">Luas Keseluruhan Kusen Pintu Jendela </CCol>
                  <CCol sm="7">: { detailRuangan.luas_finish_kpj_m2 } m<sup>2</sup></CCol>
                  <CCol sm="2"></CCol>
                </>
                  : '-'
              }
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton color="danger" onClick={() => this.setState( { modalDetail: false }) }>Tutup</CButton>
          </CModalFooter>
        </CModal>
      </>
    )
  }
}

export default ListRombel
