import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CLabel,
  CSelect,
  CFormGroup,
  CDataTable,
  CInput,
  CForm,
  CAlert,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import Select from 'react-select';
import { apiRombelList, apiPostRombel, apiGetSemester, apiGetJenisRombel } from '../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios'
import DataTable from 'react-data-table-component';
import swal from 'sweetalert';

class ListKeahlianBidang extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      users: null,
      paramsName: this.props.location.name,
      data_kompetensi: [],
      data_semester: [],
      isLoadData: true,
      idJurusan: this.props.location.query,
      conditionAlert: '',
      statusRes: false,
      messageRes: '',
      smstr: '20192',
      dataFilterJenisRombel: [],
      dataJenisRombel: [],
      semester: '',
      jenis_rombel: '',
      selectedRows: [],
      modalInfo: false,
      RombelIDArr: [],
      clearSelected: false
    };

    if (!this.props.location.query) {
      this.props.history.push('/manajemen_sekolah/kelola_rombel');
    }

    console.log(this.state.smstr)
  }

  componentDidMount() {
    this.getRombel();
    this.getFilterJenisRombel();
    this.getSemester();
  }

  getRombel(){
    try {
      userService.getData(apiRombelList + this.state.idJurusan + '&semester_id=20192' + '&jenis_rombel=1')//sessionStorage.getItem('idjurusan');this.props.location.query
          .then(
            user => {
              this.setState({ data_kompetensi: user.data.data, isLoadData: false, 
                statusRes: false,
                conditionAlert: 'warning',
                messageRes: ''})
            },
            error => {
              if (error.response) {
                if (error.response.status == 422) {
                  this.setState({ statusRes: true })
                }
              }
            }
          );
    } catch (error) {
      console.log("try Catch" + error);
    }
  }

  getSemester() {
    try {
      userService.getData(apiGetSemester)//sessionStorage.getItem('idjurusan');this.props.location.query
        .then(
          user => {
            var datas = user.data.data.map(item => {
              return (
                { value: item, label: item.substring(0, 4) + ' Smt. ' + item.substring(4, 5)}
              )
            })

            this.setState({ data_semester: datas })
          },
          error => {
            if (error.response) {
              if (error.response.status == 422) {
                this.setState({ statusRes: true })
              }
            }
          }
        );
    } catch (error) {
      console.log("try Catch" + error);
    }
  }

  getFilterJenisRombel() {
    try {
      userService.getData(apiGetJenisRombel)//sessionStorage.getItem('idjurusan');this.props.location.query
        .then(
          user => {
            var datas = user.data.data.map(item => {
              return (
                { value: item.jenis_rombel, label: item.nama_jenis_rombel }
              )
            })
            this.setState({ dataFilterJenisRombel: datas, dataJenisRombel: user.data.data })
          },
          error => {
            if (error.response) {
              if (error.response.status == 422) {
                this.setState({ statusRes: true })
              }
            }
          }
        );
    } catch (error) {
      console.log("try Catch" + error);
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const formData = {
      jurusan_id: this.state.idJurusan,
      nama_rombel: event.target.elements.nama_rombel.value,
      tingkat_pendidikan_id: event.target.elements.tingkat_pendidikan.value,
      jenis_rombel: event.target.elements.jenis_rombel.value
    }

    axios.post(apiPostRombel, formData, {
      headers: {
        'Authorization': 'token=' + this.state.currentUser.token
      }
    })
      .then(res => {
        swal("Sukses!", "Berhasil Ditambahkan", "success");
        this.setState({
          statusRes: true,
          conditionAlert: 'success',
          messageRes: 'Berhasil Input',
          clearSelected: true,
          modalInfo: false
        })
        this.getRombel();
      })
      .catch(error => {
        if (error.response) {
          if (error.response.status == 422) {
            swal("Gagal Ditambahkan!", "Input ada yang salah", "error");
            this.setState({
              modalInfo: false
            })
          } else if (error.response.status == 401) {
            swal("Gagal Ditambahkan!", "Unauthorized!", "error");
            this.setState({
              modalInfo: false
            })
          } else if (error.response.status == 409) {
            swal("Gagal Ditambahkan!", "Duplikat Jurusan!", "error");
            this.setState({
              modalInfo: false
            })
          }
        }
      });
  }

  btnSyncRombel() {
    const formData = {
      jurusan_id: this.state.idJurusan,
      rombongan_belajar_id: this.state.RombelIDArr
    }

    axios.post(apiPostRombel, formData, {
      headers: {
        'Authorization': 'token=' + this.state.currentUser.token
      }
    })
      .then(res => {
        swal("Sukses!", "Berhasil Ditambahkan", "success");
        this.setState({
          statusRes: true,
          conditionAlert: 'success',
          messageRes: 'Berhasil Input',
          clearSelected: true,
          modalInfo: false
        })
      })
      .catch(error => {
        if (error.response) {
          if (error.response.status == 422) {
            swal("Gagal Ditambahkan!", "Input ada yang salah", "error");
            this.setState({
              modalInfo: false
            })
          } else if (error.response.status == 401) {
            swal("Gagal Ditambahkan!", "Unauthorized!", "error");
            this.setState({
              modalInfo: false
            })
          } else if (error.response.status == 409) {
            swal("Gagal Ditambahkan!", "Duplikat Jurusan!", "error");
            this.setState({
              modalInfo: false
            })
          }
        }
      });
  }

  setJenisRombel(value) {
    this.setState({ jenis_rombel: value });
  }

  getListFilter(value) {
    if (this.state.jenis_rombel != '') {
      try {
        userService.getData(apiRombelList + this.state.idJurusan + '&semester_id=' + value + '&jenis_rombel=' + this.state.jenis_rombel)//sessionStorage.getItem('idjurusan');this.props.location.query
          .then(
            user => {
              this.setState({ data_kompetensi: user.data.data, isLoadData: false, 
                statusRes: false,
                conditionAlert: 'warning',
                messageRes: ''})
            },
            error => {
              if (error.response) {
                if (error.response.status == 422) {
                  this.setState({ statusRes: true })
                }
              }
            }
          );
      } catch (error) {
        console.log("try Catch" + error);
      }
    }else{
      this.setState({
        statusRes: true,
        conditionAlert: 'warning',
        messageRes: 'Mohon Pilih Jenis Rombongan Belajar Dahulu!'
      })
    }
  }

  handleSelectedRow = (state) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
    this.setState({ selectedRows: state.selectedRows });
  }

  handleModalCheck() {
    if (this.state.selectedRows.length > 0) {
      const PdArr = this.state.selectedRows.map((item, i) => {
        return (
          item.rombongan_belajar_id
        )
      })
      this.setState({
        modalInfo: true,
        RombelIDArr: PdArr,
        statusRes: false,
        conditionAlert: 'warning',
        messageRes: ''
      })
    }else{
      this.setState({
        statusRes: true,
        conditionAlert: 'warning',
        messageRes: 'Mohon Pilih Jenis Rombongan Belajar Dahulu!'
      })
    }
  }

  render() {
    const { currentUser, users, modalInfo } = this.state;
    return (
      <>
        <CRow>
          <CCol sm="12">
            <CCard>
              <CCardHeader>
                <CRow>
                <CCol sm="12" xl="4">
                      <h4>List Rombongan Belajar</h4>
                    </CCol>
                    <CCol sm="12" xl="3" className="text-lg-right">
                    <Select
                      options={this.state.dataFilterJenisRombel}
                      placeholder="Pilih Jenis Rombel"
                      onChange={(value) => this.setJenisRombel(value.value)}
                    />
                  </CCol>
                  <CCol sm="12" xl="3" className="text-lg-right">
                    <Select
                      options={this.state.data_semester}
                      placeholder="Pilih Semester"
                      onChange={(value) => this.getListFilter(value.value)}
                    />
                  </CCol>
                  <CCol sm="12" xl="2" className="text-lg-right">
                    <CButton
                      color='info'
                      onClick={() => this.handleModalCheck()}
                    >
                      <b>Tambahkan Ke...</b>
                      </CButton>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
              <CAlert color={this.state.conditionAlert} show={this.state.statusRes}>
                {this.state.messageRes} <strong>{this.state.valueSK}</strong>
              </CAlert>
                <CAlert color="primary" show>
                  <strong>Pilih Rombongan Belajar dengan Ceklis Baris Tersebut</strong>
                </CAlert>
                <DataTable
                  columns={[
                    {
                      name: 'Tingkat Pendidikan',
                      selector: 'tingkat_pendidikan_id',
                      sortable: true,
                    },
                    {
                      name: 'Nama Rombel',
                      selector: 'nama',
                      sortable: true
                    },
                    {
                      name: 'Semester',
                      cell: row => <div>{row.semester_id.substring(0, 4) + ' Smt. ' + row.semester_id.substring(4, 5)}</div>,
                      sortable: true
                    },
                    {
                      name: 'Status',
                      cell: row =>
                      <div>
                      {row.soft_delete == "1" ?
                        <>
                          <CBadge color="info">Sudah Diupdate</CBadge>
                        </>
                      :
                        <>
                          <CBadge color="warning" style={{ color: "white" }}>Belum Diupdate</CBadge>
                        </>
                      }
                      </div>
                    },
                  ]}
                  data={this.state.data_kompetensi}
                  Clicked
                  pagination
                  striped
                  paginationPerPage={10}
                  responsive
                  selectableRowsHighlight
                  onSelectedRowsChange={this.handleSelectedRow}
                  clearSelectedRows={this.state.clearSelected}
                  selectableRows
                  noHeader
                />
              </CCardBody>
            </CCard>
          </CCol>
          <CCol sm="12">
            <CCard>
              <CForm onSubmit={this.handleSubmit}>
                <CCardHeader>
                  <h4>Tambah Rombongan Belajar Baru</h4>
                </CCardHeader>
                <CCardBody>
                  <CRow style={{ marginTop: 15 }}>
                    <CCol sm="12">
                      <CFormGroup>
                        <CLabel htmlFor="jTp">Tingkat Pendidikan : </CLabel>
                        <CSelect id="jTp" name="tingkat_pendidikan">
                          <option value="10">
                            Kelas 10
                                    </option>
                        </CSelect>
                      </CFormGroup>
                      <CFormGroup>
                        <CLabel htmlFor="text-input">Nama Rombel : </CLabel>
                        <CInput id="text-input" name="nama_rombel" onChange={this.handleInputKelas} autoComplete="off" />
                      </CFormGroup>
                      <CFormGroup>
                        <CLabel htmlFor="jRombel">Jenis Rombel : </CLabel>
                        <CSelect id="jRombel" name="jenis_rombel">
                          {this.state.dataJenisRombel.map((item, i) => {
                            return (
                              <option value={item.jenis_rombel}>
                                {item.nama_jenis_rombel}
                              </option>
                            )
                          })}
                        </CSelect>
                      </CFormGroup>
                    </CCol>
                  </CRow>
                </CCardBody>
                <CCardFooter>
                  <CButton type="submit" color="success"><CIcon name="cil-scrubber" /> Konfirmasi</CButton>
                </CCardFooter>
              </CForm>
            </CCard>
          </CCol>
        </CRow>

        <CModal
          show={modalInfo}
          onClose={() => this.setState({ modalInfo: false })}
          color="info"
        >
          <CForm onSubmit={this.handleSubmit}>
            <CModalHeader>
              <CModalTitle>Rombongan Belajar yang dipilih : </CModalTitle>
            </CModalHeader>
            <CModalBody>
              <CCard>
                <CCardBody style={{ overflowY: 'scroll', height: 200 }}>
                <ul>
                    {this.state.selectedRows.length > 0 ? this.state.selectedRows.map((item, i) => {
                      return (
                        <li key={i}>
                          {item.nama}
                        </li>
                      )
                    }) : <></>}
                  </ul>
                </CCardBody>
              </CCard>
              <CCol className="text-center">
                Akan tertambah ke <strong>Semester Selanjutnya</strong>
              </CCol>
            </CModalBody>
            <CModalFooter>
              <CButton color="danger" onClick={() => this.setState({ modalInfo: false })}>Batal</CButton>
              <CButton color="info" onClick={() => this.btnSyncRombel()}>Simpan</CButton>
            </CModalFooter>
          </CForm>
        </CModal>
      </>
    )
  }
}

export default ListKeahlianBidang
