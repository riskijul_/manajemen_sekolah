import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { history } from '../../_helpers';
import { apiRombelList, apiGetSemester, apiGetGuru } from '../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios';
import Select from 'react-select';

class ListKeahlian extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      users: null,
      paramsName: this.props.location.name,
      params: this.props.location.query,
      data_kompetensi: [],
      isLoadData: true,
      idJurusan: localStorage.getItem('idjurusan'),
      data_semester: [],
      modalAssign: false,
      isLoadGuru: true
    };

    if (!localStorage.getItem('idjurusan')) {
        this.props.history.push('/manajemen_sekolah/pengelolaan');
    }

    //console.log(localStorage.getItem('idjurusan'))
  }

  componentDidMount() {
    try {
      userService.getData(apiRombelList + this.state.idJurusan + '&semester_id=20192')
      .then(
          user => {
            this.setState({ data_kompetensi: user.data.data, isLoadData: false })
          },
          error => {
              if (error.response) {
                  if(error.response.status == 422){
                      this.setState({ statusRes: true })
                  }
              }
          }
      );
    }catch (error) {
      console.log("try Catch" +error);
    }

    this.getSemester()
  }

  getSemester(){
    try {
      userService.getData(apiGetSemester)
      .then(
          user => {
            var datas = user.data.data.map(item => {
              return(
                { value: item, label: item.substring(0, 4) + ' Smt. ' + item.substring(4, 5)}
              )
            })
          this.setState({ data_semester: datas })
          console.log(datas)
          },
          error => {
              if (error.response) {
                  if(error.response.status == 422){
                      this.setState({ statusRes: true })
                  }
              }
          }
      );
    }catch (error) {
      console.log("try Catch" +error);
    }
  }

  getList(value){
    try {
      userService.getData(apiRombelList + this.state.idJurusan + '&semester_id=' + value)
      .then(
          user => {
            this.setState({ data_kompetensi: user.data.data, isLoadData: false })
          },
          error => {
              if (error.response) {
                  if(error.response.status == 422){
                      this.setState({ statusRes: true })
                  }
              }
          }
      );
    }catch (error) {
      console.log("try Catch" +error);
    }
  }

  render() {
    const { currentUser, users, modalAssign } = this.state;
    return (
      <>
        <CRow>
          <CCol>
            <CCard>
              <CCardHeader>
                <CRow>
                <CCol sm="9">
                  <h4>Pilih Rombongan Belajar</h4>
                  <CButton onClick={() => this.setState({ modalAssign: true })}/>
                </CCol>
                <CCol sm="3" className="text-lg-right">
                  <Select
                    options={this.state.data_semester}
                    placeholder="Pilih Semester..."
                    onChange={(value) => this.getList(value.value)}
                  />
                </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
              <CDataTable
                items={this.state.data_kompetensi}
                fields={[{key: 'nama_rombel' }, { key: 'semester' }, { key: 'aksi', sorter: false }]}
                hover
                border
                itemsPerPage={10}
                pagination
                loading={this.state.isLoadData}
                scopedSlots = {{
                  'nama_rombel':
                    (item)=>(
                      <td>
                          {item.nama}
                      </td>
                    ),
                  'semester':
                    (item)=>(
                      <td>
                          {item.semester_id.substring(0, 4) + ' Smt. ' + item.semester_id.substring(4, 5)}
                      </td>
                    ),
                  'aksi':
                    (item)=>(
                      <td>
                          <CButton color="success" to={{pathname: `/manajemen_sekolah/pengelolaan/list_keahlian/detail_rombel`, query: item.rombongan_belajar_id, name: item.nama_jurusan, tp_id: item.tingkat_pendidikan_id, smstr_id: item.semester_id}}>Pilih</CButton>
                      </td>
                    )
                }}
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    )
  }
}

export default ListKeahlian
