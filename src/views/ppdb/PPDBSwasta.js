import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from 'axios'
import MainChartExample from '../charts/MainChartExample.js'
import { authenticationService } from '../../_services';

const WidgetsSekolah = lazy(() => import('../widgets/ppdb/WidgetsSekolah.js'))
const WidgetsBrand = lazy(() => import('../widgets/WidgetsBrand.js'))

class PPDBSwasta extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        users: null,
        namaSekolah: ''
    };

  }

  async componentDidMount(){
    try{
      const nama = await authenticationService.currentUserValue.data.nama_sekolah;
      this.setState({ namaSekolah: nama });
    }catch(error) {
      if(!alert('Selamat Datang di Website Manajemen Data Sekolah!')){window.location.reload();}
    }
  }

  render() {
    const { currentUser, users } = this.state;
    return (
      <>
      <CCard>
          <CCardBody>
            <CRow>
              <CCol sm="8">
                <h4>{this.state.namaSekolah}</h4>
              </CCol>
            </CRow>
          </CCardBody>
        </CCard>
        <WidgetsSekolah />
      </>
    )
  }
}

export default PPDBSwasta
