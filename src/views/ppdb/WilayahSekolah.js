import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CLabel,
  CAlert
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { history } from '../../_helpers';
import { apiGetKab, apiGetSekolah, apiGetKec } from '../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios';
import Select from 'react-select';

class WilayahSekolah extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      users: null,
      paramsName: this.props.location.name,
      params: localStorage.getItem("bentukPendidikanId"),
      isLoadData: true,
      dataWilayah: [],
      modalAssign: false,
      isLoadGuru: true,
      kdWilayah: null,
      dataWilayahKec: [],
      statusRes: false,
      conditionAlert: '',
      messageRes: ''
    };

    if (!localStorage.getItem("bentukPendidikanId")) {
        this.props.history.push('/manajemen_sekolah/ppdb');
    }
  }

  async componentDidMount() {
    try {
        await userService.getData(apiGetKab)
        .then(
            user => {
              var datas = user.data.data.map(item => {
                return(
                  { value: item, label: item.nama}
                )
              })
            this.setState({ dataWilayah: datas })
            //console.log(datas)
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            }
        );
      }catch (error) {
        console.log("try Catch" +error);
      }
  }

  async getKecamatan(value){
    await  userService.getData(apiGetKec + '3&mst_kode_wilayah=' + value.kode_wilayah)
    .then(
        user => {
          var datas = user.data.data.map(item => {
            return(
              { value: item, label: item.nama}
            )
          })
        this.setState({ dataWilayahKec: datas })
        //console.log(datas)
        },
        error => {
            if (error.response) {
                if(error.response.status == 422){
                    this.setState({ statusRes: true })
                }
            }
        }
    );
  }

  async getListSekolah(){
    if(this.state.kdWilayah != null){
      await userService.getData(apiGetSekolah + this.state.kdWilayah.replace(/\s/g, '') + '/' + this.state.params)
      .then(
          user => {
            if(user.data.data.length > 0){
              this.setState({ listSekolah: user.data.data, isLoadData: false , statusRes: false})
            }else{
              this.setState({
                statusRes: true,
                conditionAlert: 'warning',
                messageRes: 'Tidak Ada Sekolah Terkait di Wilayah Ini'
              })
            }
          //console.log(datas)
          },
          error => {
              if (error.response) {
                  if(error.response.status == 422){
                      this.setState({ statusRes: true })
                  }
              }
          }
      );
    }else{
      this.setState({
        statusRes: true,
        conditionAlert: 'warning',
        messageRes: 'Mohon Pilih Kabupaten dan Kecamatan Anda Terlebih Dahulu!'
      })
    }
  }

  getFinalList(value){
    this.setState({ kdWilayah: value.kode_wilayah })
  }

  render() {
    const { currentUser, users, modalAssign } = this.state;
    return (
      <>
        <CRow>
          <CCol sm="12">
            <CCard>
              <CCardHeader>
                <h4>Pilih Wilayah Sekolah Jenjang {this.state.paramsName == null ? localStorage.getItem("namaJenjang") : this.state.paramsName}</h4>
              </CCardHeader>
              <CCardBody>
              <CAlert color={this.state.conditionAlert} show={this.state.statusRes}>
                {this.state.messageRes} <strong>{this.state.valueSK}</strong>
              </CAlert>
                <CRow>
                  <CCol sm="12" xl="12">
                  <CLabel>Pilih Kabupaten</CLabel>
                  <Select
                      options={this.state.dataWilayah}
                      placeholder="Pilih Kabupaten..."
                      onChange={(value) => this.getKecamatan(value.value)}
                    />
                  </CCol>
                  <CCol sm="12" xl="12">
                    <br/>
                  <CLabel>Pilih Kecamatan</CLabel>
                  <Select
                      options={this.state.dataWilayahKec}
                      placeholder="Pilih Kecamatan..."
                      onChange={(value) => this.getFinalList(value.value)}
                    />
                  </CCol>
                </CRow>
              </CCardBody>
              <CCardFooter className="text-right">
                <CButton color="info" onClick={() => this.getListSekolah()}>Cari</CButton>
              </CCardFooter>
            </CCard>
          </CCol>
          <CCol sm="12">
            <CCard>
              <CCardBody>
                <CDataTable
                items={this.state.listSekolah}
                fields={[{key: 'nama' }, 'alamat_jalan','action']}
                hover
                bordered
                hover
                sorter
                tableFilter
                itemsPerPage={10}
                pagination
                loading={this.state.isLoadData}
                scopedSlots = {{
                  'action':
                  (item)=>{
                      return (
                          <td className="py-2">
                              <CButton variant="outline" color="primary" to={{pathname: `/manajemen_sekolah/ppdb/wilayah/list_siswa`}} onClick={() => localStorage.setItem("detailSekolah", JSON.stringify({namaSekolah : item.nama, npsnSekolah: item.npsn}))}>Pilih</CButton>
                          </td>
                      )
                  }
                }}
              />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    )
  }
}

export default WilayahSekolah
