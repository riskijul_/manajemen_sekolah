import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CLabel,
  CSelect,
  CFormGroup,
  CDataTable,
  CInput,
  CForm,
  CAlert,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import Select from 'react-select';
import { apiGetSiswa, apiPostPPDB } from '../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios'
import DataTable from 'react-data-table-component';
import DataTableExtensions from 'react-data-table-component-extensions';

class ListKeahlianBidang extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      users: null,
      params: JSON.parse(localStorage.getItem("detailSekolah")),
      listSiswa: [],
      data_semester: [],
      conditionAlert: '',
      messageRes: '',
      statusRes: false,
      valueSK: '',
      selectedRows: {data_send: []},
      modalInfo: false
    };

    if (!localStorage.getItem("detailSekolah")) {
      this.props.history.push('/manajemen_sekolah/ppdb/wilayah');
    }
  }

  componentDidMount() {
    this.getDataSiswa();
  }

  async getDataSiswa(){
    try {
      await userService.getData(apiGetSiswa + this.state.params.npsnSekolah)//sessionStorage.getItem('idjurusan');this.props.location.quasdy
        .then(
          user => {
            this.setState({ listSiswa: user.data.data, isLoadData: false, statusRes: false })
          },
          error => {
            if (error.response) {
              if (error.response.status == 422) {
                this.setState({ statusRes: true, messageRes: 'Terjadi Kesalahan Saat Menerima Data', conditionAlert: 'danger' })
              }
            }
          }
        );
    } catch (error) {
      console.log("try Catch" + error);
    }
  }

  handleChangeRow = (state) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
      
    var data_send = {
      data_send: state.selectedRows
    }
    this.setState({ selectedRows: data_send });
    console.log(data_send)
  };

  handleAmbilData(){
    if(this.state.selectedRows.data_send.length > 0){
      this.setState({modalInfo: true, statusRes: false})
    }else{
      this.setState({ statusRes: true, messageRes: 'Mohon Pilih Peserta Didik.', conditionAlert: 'danger' })
    }
  }

  async btnSendPPDB(){
    await userService.postData(apiPostPPDB, this.state.selectedRows)
      .then(
        user => {
          if(!alert('Pesan Dari Server : ' + user.data.message)){window.location.reload();}
          this.setState({ statusRes: true, messageRes: 'Sukses Mengirim Data Peserta Didik', conditionAlert: 'success'})
        },
        error => {
          this.setState({ statusRes: true, messageRes: 'Error Send Data', conditionAlert: 'warning' })
        }
      )
  }

  render() {
    const { currentUser, users, modalInfo } = this.state;
    return (
      <>
        <CRow>
          <CCol sm="12">
            <CCard>
              <CCardHeader>
                <CRow>
                  <CCol sm="3">
                    <h4>List Siswa</h4>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <CRow>
                  <CCol sm="12">
                    <CAlert color={this.state.conditionAlert} show={this.state.statusRes}>
                      {this.state.messageRes} <strong>{this.state.valueSK}</strong>
                    </CAlert>
                  </CCol>
                  <CCol sm="12">
                    <CRow>
                      <CCol sm="12" xl="9">
                        <CCard className="card h-100">
                          <CCardBody>
                            <table>
                              <tbody>
                                <tr>
                                  <td><b>Provinsi</b></td>
                                  <td width="100" className="text-center">:</td>
                                  <td>Jawa Barat</td>
                                </tr>
                                <tr>
                                  <td><b>Sekolah</b></td>
                                  <td className="text-center">:</td>
                                  <td>{this.state.params.namaSekolah}</td>
                                </tr>
                                <tr>
                                  <td><b>NPSN</b></td>
                                  <td className="text-center">:</td>
                                  <td>{this.state.params.npsnSekolah}</td>
                                </tr>
                              </tbody>
                            </table>
                          </CCardBody>
                        </CCard>
                      </CCol>
                      <CCol sm="12" xl="3">
                        <CCard className="card h-100">
                          <CCardBody>
                            <CCol className="text-center">
                            <b>{this.state.selectedRows.data_send.length > 0 ? this.state.selectedRows.data_send.length : <>0</>}</b> Peserta Didik Terpilih
                            </CCol>
                            <CCol  className="text-center">
                            <br></br>
                            <CButton color="primary" onClick={() => this.handleAmbilData()}><b>Ambil Data</b></CButton>
                            </CCol>
                          </CCardBody>
                        </CCard>
                      </CCol>
                    </CRow>
                  </CCol>
                  <CCol sm="12" style={{ paddingTop: 15}}>
                    <DataTableExtensions
                      columns={[
                        {
                          name: 'Nama Siswa',
                          selector: 'nama',
                          sortable: true,
                        },
                        {
                          name: 'NISN',
                          selector: 'nisn',
                          sortable: true
                        },
                        {
                          name: 'Jenis Kelamin',
                          selector: 'jenis_kelamin',
                          sortable: true
                        },
                        {
                          name: 'Alamat',
                          selector: 'alamat_jalan',
                          sortable: true
                        },
                      ]}
                      data={this.state.listSiswa}
                      export={false}
                      print={false}
                      filterPlaceholder="Cari Nama Siswa/NISN..."
                    >
                      <DataTable
                        noHeader
                        pagination
                        striped
                        paginationPerPage={10}
                        responsive
                        onSelectedRowsChange={this.handleChangeRow}
                        selectableRows
                      />
                    </DataTableExtensions>
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

        <CModal
          show={modalInfo}
          onClose={() => this.setState({ modalInfo: false })}
          color="info"
        >
          <CForm onSubmit={this.handleSubmit}>
            <CModalHeader>
              <CModalTitle>Peserta Didik Yang Terpilih : </CModalTitle>
            </CModalHeader>
            <CModalBody>
              <CCard>
                <CCardBody style={{ overflowY: 'scroll', height: 200 }}>
                <ol>
                    {this.state.selectedRows.data_send.length > 0 ? this.state.selectedRows.data_send.map((item, i) => {
                      return (
                        <li key={i}>
                          {item.nama}
                        </li>
                      )
                    }) : <></>}
                  </ol>
                </CCardBody>
              </CCard>
              <CCol className="text-center">
                Akan tertambah ke <strong>{authenticationService.currentUserValue.data.nama_sekolah}</strong>
              </CCol>
            </CModalBody>
            <CModalFooter>
              <CButton color="secondary" onClick={() => this.setState({ modalInfo: false })}>Cancel</CButton>
              <CButton color="info" onClick={() => this.btnSendPPDB()}>Konfirmasi</CButton>
            </CModalFooter>
          </CForm>
        </CModal>
      </>
    )
  }
}

export default ListKeahlianBidang
