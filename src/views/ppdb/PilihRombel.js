import React, { lazy } from 'react'
import {
  CBadge,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout,
  CDataTable,
  CLabel,
  CAlert,
  CFormGroup,
  CInput,
  CForm,
  CInputGroup,
  CInputFile
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { userService, authenticationService } from '../../_services';
import { history } from '../../_helpers';
import { apiRombel, apiJurusan } from '../../_components';
// import usersData from '../users/UsersData'
import axios from 'axios';
import Select from 'react-select';

class PilihRombel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: authenticationService.currentUserValue.data,
      users: null,
      paramsName: this.props.location.name,
      params: localStorage.getItem("bentukPendidikanId"),
      isLoadData: true,
      dataWilayah: [],
      modalAssign: false,
      isLoadGuru: true,
      kdWilayah: null,
      dataWilayahKec: [],
      statusRes: false,
      conditionAlert: '',
      messageRes: '',
      data_jurusan: []
    };

    if (!localStorage.getItem("bentukPendidikanId")) {
        this.props.history.push('/manajemen_sekolah/ppdb');
    }
  }

    componentDidMount() {
        this.getJurusan();
        this.getRombel();
    }

    async getJurusan(){
        await userService.getData(apiJurusan + "1")
        .then(
            user => {
              var datas = user.data.data.map(item => {
                return(
                  { value: item.jurusan_id, label: item.nama_jurusan}
                )
              })
            this.setState({ data_jurusan: datas })
            //console.log(datas)
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            }
        );
      }

    async getRombel(){
        await userService.getData(apiRombel)
        .then(
            user => {
            var datas = user.data.data.map(item => {
                return(
                { value: item.rombongan_belajar_id, label: item.nama}
                )
            })
            this.setState({ data_rombel_as: datas })
            //console.log(datas)
            },
            error => {
                if (error.response) {
                    if(error.response.status == 422){
                        this.setState({ statusRes: true })
                    }
                }
            }
        );
    }

    async selectJurusan(value){
        this.setState( { data_select_jurusan: value } );

        try {
            userService.getData(apiRombel + "?jurusan_induk=" + value)
            .then(
                user => {
                    var datas = user.data.data.map(item => {
                        return(
                            { value: item.rombongan_belajar_id, label: item.nama }
                        )
                    })
                  this.setState({ data_rombel: datas, isLoadingRombelGuru: false })
                },
                error => {
                    if (error.response) {
                        if(error.response.status == 422){
                            this.setState({ statusRes: true })
                        }
                    }
                }
            );
          }
          catch (error) {
            console.log("try Catch" +error);
          }
    }

//   getFinalList(value){
//     this.setState({ kdWilayah: value.kode_wilayah })
//   }

  render() {
    const { currentUser, users, modalAssign } = this.state;
    return (
      <>
        <CRow>
        <CCol xs="12">
            <CCard>
              <CCardHeader>
                Pilih Rombel
              </CCardHeader>
                <CCardBody>
                  <CForm className="form-horizontal" onSubmit={ this.handleSubmit }>
                    <CFormGroup>
                      <CLabel htmlFor="appendedInput">Cari Jurusan</CLabel>
                      <div className="controls">
                      <Select
                        isLoading={ this.state.isLoadingJurusan }
                        options={ this.state.data_jurusan }
                        placeholder=""
                        onChange={ (value) => this.selectJurusan(value.value) }
                      />
                      </div>
                    </CFormGroup>

                    <hr/>

                    <CFormGroup>
                      <CLabel htmlFor="appendedInput">Pilih Rombel</CLabel>
                      <div className="controls">
                      <Select
                        isLoading={ this.state.isLoadingRombelGuru }
                        options={ this.state.data_rombel }
                        placeholder="Cari Rombel..."
                      />
                      </div>
                    </CFormGroup>
                    <hr/>
                    <br/>
                    <div className="form-actions">
                      <CButton type="submit" color="info">Simpan</CButton> <CButton to={{ pathname: `/manajemen_sekolah/kelola_mapel_guru`}} color="danger">Batal</CButton>
                    </div>
                  </CForm>
                </CCardBody>
            </CCard>
        </CCol>
        </CRow>
      </>
    )
  }
}

export default PilihRombel
