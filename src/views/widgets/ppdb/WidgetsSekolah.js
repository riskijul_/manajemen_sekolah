import React from 'react'
import {
  CWidgetDropdown,
  CRow,
  CCol,
  CDropdown,
  CDropdownMenu,
  CDropdownItem,
  CDropdownToggle,
  CCardBody,
  CCard,
  CWidgetBrand,
  CButton,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ChartLineSimple from '../../charts/ChartLineSimple'
import ChartBarSimple from '../../charts/ChartBarSimple'
import { userService, authenticationService } from '../../../_services';
import { apiGetBentukPend } from '../../../_components';

class WidgetsSekolah extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
      dataPend: []
    };

  }

  async componentDidMount() {
    try {
      await userService.getData(apiGetBentukPend)
        .then(user => {
          this.setState({ dataPend: user.data.data });
        },
          error => {
            console.log(error)
          }
        )
    } catch (error) {
      console.log(error)
    }
    localStorage.removeItem("bentukPendidikanId")
    localStorage.removeItem("namaJenjang")
  }

  handleClickJenjang(bId, name){
    localStorage.setItem("bentukPendidikanId", bId)
    localStorage.setItem("namaJenjang", name)
  }

  render() {
    return (
      <CRow>
        {this.state.dataPend.map((item, i) => {
          return (
            <CCol sm="12" lg="4" key={i}>
              <CWidgetBrand
                color="gradient-info"
                rightHeader={<h5>{item.nama}</h5>}
                leftHeader={<CButton variant="outline" color="primary" to={{pathname: `/manajemen_sekolah/ppdb/wilayah`, name: item.nama}} onClick={() => this.handleClickJenjang(item.bentuk_pendidikan_id, item.nama)}>Pilih</CButton>}
              >
                <CImg
                  src="iconSMP.png"
                  height="125"
                  className="my-4"
                />
              </CWidgetBrand>
            </CCol>
          )
        })}
      </CRow>
    )
  }
}

export default WidgetsSekolah
