import React from 'react'
import { Link } from 'react-router-dom'
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import {
    CButton,
    CCard,
    CCardBody,
    CCardGroup,
    CCol,
    CContainer,
    CForm,
    CInput,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CRow,
    CImg,
    CTabContent,
    CTabPane,
    CNavItem,
    CNavLink,
    CTabs,
    CCardHeader,
    CNav
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { authenticationService } from '../../../_services';

class LoginPPDB extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            onTab: 0
        }

        // // redirect to home if already logged in
        if (authenticationService.currentLoginValue) {
            this.props.history.push('/manajemen_sekolah/dashboard_kelola_data');
        }
    }

    render() {
        return (
            <div className="c-app c-default-layout flex-row align-items-center">
                <CContainer>
                    <CRow className="justify-content-center">
                        <CCol md="8">
                            <CTabs activeTab={this.state.onTab} onActiveTabChange={idx => this.setState({ onTab: idx })}>
                                <CNav variant="tabs">
                                    <CNavItem style={{ backgroundColor: 'white'}}>
                                        <CNavLink>
                                            <CIcon name="cil-calculator" /> Manajemen Data Sekolah
                                        </CNavLink>
                                    </CNavItem>
                                    <CNavItem  style={{ backgroundColor: 'white'}}>
                                        <CNavLink>
                                            <CIcon name="cil-basket" /> PPDB Swasta
                                        </CNavLink>
                                    </CNavItem>
                                </CNav>
                                <CTabContent>
                                    <CTabPane>
                                        <CCardGroup>
                                            <CCard className="p-4">
                                                <CCardBody>
                                                    <Formik
                                                        initialValues={{
                                                            username: '',
                                                            password: ''
                                                        }}
                                                        validationSchema={Yup.object().shape({
                                                            username: Yup.string().required('Username is required'),
                                                            password: Yup.string().required('Password is required')
                                                        })}
                                                        onSubmit={({ username, password }, { setStatus, setSubmitting }) => {
                                                            setStatus();
                                                            authenticationService.login(username, password)
                                                                .then(
                                                                    user => {
                                                                        //const { from } = this.props.location.state || { from: { pathname: "/manajemen_sekolah/dashboard_kurikulum" } };
                                                                        this.props.history.push('/manajemen_sekolah/dashboard_kelola_data');
                                                                    },
                                                                    error => {
                                                                        setSubmitting(false);
                                                                        setStatus(error);
                                                                    }
                                                                );
                                                        }}
                                                        render={({ errors, status, touched, isSubmitting }) => (
                                                            <Form>
                                                                <h1>Login</h1><h3>Manajemen Data Sekolah</h3>
                                                                <p className="text-muted">Sign In with Your Registered Account</p>
                                                                <CInputGroup className="mb-3">
                                                                    <CInputGroupPrepend>
                                                                        <CInputGroupText>
                                                                            <CIcon name="cil-user" />
                                                                        </CInputGroupText>
                                                                    </CInputGroupPrepend>
                                                                    <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
                                                                    <ErrorMessage name="username" component="div" className="invalid-feedback" />
                                                                </CInputGroup>
                                                                <CInputGroup className="mb-4">
                                                                    <CInputGroupPrepend>
                                                                        <CInputGroupText>
                                                                            <CIcon name="cil-lock-locked" />
                                                                        </CInputGroupText>
                                                                    </CInputGroupPrepend>
                                                                    <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                                                                    <ErrorMessage name="password" component="div" className="invalid-feedback" />
                                                                </CInputGroup>
                                                                <CRow>
                                                                    <CCol xs="2">
                                                                        <CButton type="submit" className="btn btn-primary" disabled={isSubmitting}>Login</CButton>
                                                                    </CCol>
                                                                    <CCol xs="3" className="text-center">
                                                                        {isSubmitting && <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />}
                                                                    </CCol>
                                                                </CRow>
                                                                <CRow>
                                                                    <CCol style={{ paddingTop: 20 }}>
                                                                        {status &&
                                                                            <div className={'alert alert-danger'}>{status}</div>
                                                                        }
                                                                    </CCol>
                                                                </CRow>
                                                            </Form>
                                                        )}
                                                    />
                                                </CCardBody>
                                            </CCard>
                                            <CCard className="text-white bg-secondary py-5 d-md-down-none" style={{ width: '44%' }}>
                                                <CCardBody className="text-center">
                                                    <CImg
                                                        src="https://tikomdik.jabarprov.go.id/images/logo-tkd.png"
                                                        className="img-fluid"
                                                        alt="logo-tikomdik" />
                                                    <CImg
                                                        src="manaj.png"
                                                        className="img-fluid rounded"
                                                        style={{ paddingTop: 30 }}
                                                        alt="manaj-img" />
                                                </CCardBody>
                                            </CCard>
                                        </CCardGroup>
                                    </CTabPane>
                                    <CTabPane>
                                        <CCardGroup>
                                            <CCard className="p-4">
                                                <CCardBody>
                                                    <Formik
                                                        initialValues={{
                                                            username: '',
                                                            password: ''
                                                        }}
                                                        validationSchema={Yup.object().shape({
                                                            username: Yup.string().required('Username is required'),
                                                            password: Yup.string().required('Password is required')
                                                        })}
                                                        onSubmit={({ username, password }, { setStatus, setSubmitting }) => {
                                                            setStatus();
                                                            authenticationService.login(username, password)
                                                                .then(
                                                                    user => {
                                                                        //const { from } = this.props.location.state || { from: { pathname: "/manajemen_sekolah/dashboard_kurikulum" } };
                                                                        this.props.history.push('/manajemen_sekolah/dashboard_kelola_data');
                                                                    },
                                                                    error => {
                                                                        setSubmitting(false);
                                                                        setStatus(error);
                                                                    }
                                                                );
                                                        }}
                                                        render={({ errors, status, touched, isSubmitting }) => (
                                                            <Form>
                                                                <h1>Login</h1><h3>PPDB Swasta</h3>
                                                                <p className="text-muted">Sign In with Your Registered Account</p>
                                                                <CInputGroup className="mb-3">
                                                                    <CInputGroupPrepend>
                                                                        <CInputGroupText>
                                                                            <CIcon name="cil-user" />
                                                                        </CInputGroupText>
                                                                    </CInputGroupPrepend>
                                                                    <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
                                                                    <ErrorMessage name="username" component="div" className="invalid-feedback" />
                                                                </CInputGroup>
                                                                <CInputGroup className="mb-4">
                                                                    <CInputGroupPrepend>
                                                                        <CInputGroupText>
                                                                            <CIcon name="cil-lock-locked" />
                                                                        </CInputGroupText>
                                                                    </CInputGroupPrepend>
                                                                    <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                                                                    <ErrorMessage name="password" component="div" className="invalid-feedback" />
                                                                </CInputGroup>
                                                                <CRow>
                                                                    <CCol xs="2">
                                                                        <CButton type="submit" className="btn btn-primary" disabled={isSubmitting}>Login</CButton>
                                                                    </CCol>
                                                                    <CCol xs="3" className="text-center">
                                                                        {isSubmitting && <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />}
                                                                    </CCol>
                                                                </CRow>
                                                                <CRow>
                                                                    <CCol style={{ paddingTop: 20 }}>
                                                                        {status &&
                                                                            <div className={'alert alert-danger'}>{status}</div>
                                                                        }
                                                                    </CCol>
                                                                </CRow>
                                                            </Form>
                                                        )}
                                                    />
                                                </CCardBody>
                                            </CCard>
                                            <CCard className="text-white bg-secondary py-5 d-md-down-none" style={{ width: '44%' }}>
                                                <CCardBody className="text-center">
                                                    <CImg
                                                        src="https://tikomdik.jabarprov.go.id/images/logo-tkd.png"
                                                        className="img-fluid"
                                                        alt="logo-tikomdik" />
                                                    <CImg
                                                        src="manaj.png"
                                                        className="img-fluid rounded"
                                                        style={{ paddingTop: 30 }}
                                                        alt="manaj-img" />
                                                </CCardBody>
                                            </CCard>
                                        </CCardGroup>
                                    </CTabPane>
                                </CTabContent>
                            </CTabs>
                        </CCol>
                    </CRow>
                </CContainer>
            </div>
        )
    }
}

export default LoginPPDB
