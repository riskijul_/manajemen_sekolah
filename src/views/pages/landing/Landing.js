import React, { useState } from 'react'
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCarousel,
    CCarouselCaption,
    CCarouselControl,
    CCarouselIndicators,
    CCarouselInner,
    CCarouselItem,
    CCol,
    CRow,
    CButton
} from '@coreui/react'

const slides = [
    'https://www.dream-wallpaper.com/free-wallpaper/nature-wallpaper/dual-screen-sunset-wallpaper/dual-screen/free-wallpaper-11.jpg',
    'https://www.dream-wallpaper.com/free-wallpaper/nature-wallpaper/dual-screen-sunset-wallpaper/dual-screen/free-wallpaper-9.jpg'
]

class LandingScreen extends React.Component {
    render() {
        return (
            <CRow>
                <CCol xs="12" xl="12">
                    <CCarousel animate autoSlide={3000}>
                        <CCarouselIndicators />
                        <CCarouselInner>
                            <CCarouselItem>
                                <img className="d-block w-100" src={slides[0]} alt="slide 1" />
                                <CCarouselCaption>
                                    <h3>Manajemen Data Sekolah</h3>
                                </CCarouselCaption>
                            </CCarouselItem>
                            <CCarouselItem>
                                <img className="d-block w-100" src={slides[1]} alt="slide 2" />
                                <CCarouselCaption><h3>PPDB Swasta</h3></CCarouselCaption>
                            </CCarouselItem>
                        </CCarouselInner>
                        <CCarouselControl direction="prev" />
                        <CCarouselControl direction="next" />
                    </CCarousel>
                </CCol>
                <CCol sm="12" xs="12" xl="6">
                    <CCard xl="3" style={{ margin: 20}} className="text-center">
                        <CCardHeader color="info">
                            Manajemen Data Sekolah
                        </CCardHeader>
                        <CCardBody>
                            <CButton color="primary" to={'/login'}>Login</CButton>
                        </CCardBody>
                    </CCard>
                </CCol>
                <CCol sm="12" xs="12" xl="6">
                    <CCard xl="3" style={{ margin: 20}} className="text-center">
                        <CCardHeader color="info">
                            PPDB Swasta
                        </CCardHeader>
                        <CCardBody>
                            <CButton color="primary" to={'/login_ppdb_swasta'}>Login</CButton>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        )
    }
}

export default LandingScreen
