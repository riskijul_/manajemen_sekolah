// const httpUrl                     = 'https://atikan.dev.elemen.my.id/manajemen';
const httpUrl                        = 'https://atikan.test.elemen.my.id/manajemen';
// const httpLogin                   = 'https://atikan.dev.elemen.my.id/sso';
const httpLogin                      = 'http://103.155.104.14:8000';
const httpVerify                     = 'http://103.155.104.14:3000';
const lokal                          = 'http://192.168.100.199:6855';

//Login
export const apiLogin                = httpLogin + '/auth';
export const apiVerifyAuth           = lokal + '/verifyAuth';
// export const apiVerifyAuth           = httpVerify + '/auth/verify';

//REF
export const apiGetAgama             = lokal + '/kurikulum/ref/agama'
export const apiGetPangkatGol        = lokal + '/kurikulum/ref/pangkat_golongan'
export const apiGetJenisPTK          = lokal + '/kurikulum/ref/jenis_ptk'
export const apiGetLembagaPengangkat = lokal + '/kurikulum/ref/lembaga_pengangkat'
export const apiGetStatusKepegawaian = lokal + '/kurikulum/ref/status_kepegawaian'
export const apiGetPekerjaan         = lokal + '/kurikulum/ref/pekerjaan'
export const apiGetPerkawinan        = lokal + '/kurikulum/ref/status_perkawinan'

//====================MASTER====================//
export const apiGetPesertaDidik      = lokal + '/kurikulum/master/master_pd/'

export const apiGetPtk               = lokal + '/kurikulum/master/master_ptk/'

export const apiGetPrasarana         = lokal + '/kurikulum/master/master_ruangan/'
export const apiGetRuangan           = lokal + '/kurikulum/master/master_ruangan/'
export const apiGetDetailRuangan     = lokal + '/kurikulum/master/master_ruangan/detail_ruang/'

//GET_Data
//-----------MDSP
export const apiJurusan              = lokal + '/kurikulum/jurusan?tipe_jurusan=';
export const apiJurusanSp            = lokal + '/kurikulum/jurusan/list';

export const apiRombel               = lokal + '/kurikulum/rombel';
export const apiRombelList           = lokal + '/kurikulum/rombel?jurusan_id=';
export const apiRombelDetail         = lokal + '/kurikulum/rombel/anggota/';

export const apiPostSk               = lokal + '/kurikulum/jurusan';
export const apiDeleteJurusan        = lokal + '/kurikulum/jurusan/';

export const apiGetSemester          = lokal + '/kurikulum/rombel/semester';
export const apiCheckEmpatThn        = lokal + '/kurikulum/rombel/statusEmpatTahun?jurusan_id=';

export const apiGetMapel             = lokal + '/kurikulum/pembelajaran';
export const apiGetMapelGuru         = lokal + '/kurikulum/pembelajaran/guru';
export const apiGetGuru              = lokal + '/kurikulum/pembelajaran/guru/list?mata_pelajaran_id=';
export const apiGetRombelGuru        = lokal + '/kurikulum/pembelajaran/guru/rombel?ptk_terdaftar_id=';

export const apiGetPdNaik            = lokal + '/kurikulum/rombel/list/naik?jurusan_id='; //15052&tingkat_pendidikan_id=11

export const apiGetWaliKelas         = lokal + '/kurikulum/rombel/wali';
export const apiGetJenisRombel       = lokal + '/kurikulum/rombel/jenis_rombel';

export const apiGetSoal              = lokal + '/kurikulum/soal';
export const apiGetJawaban           = lokal + '/kurikulum/soal/jawaban?id_soal=';
export const apiGetMateri            = lokal + '/kurikulum/materi';

export const apiGetJadwal            = lokal + '/kurikulum/pembelajaran/jadwal/kurikulum/'
export const apiGetJadwalGuru        = lokal + '/kurikulum/pembelajaran/guru/matpel/'
export const apiGetJadwalMapel       = lokal + '/kurikulum/pembelajaran/guru/matpel_terdaftar/'

//-----------PPDB
export const apiGetBentukPend        = lokal + '/kurikulum/ppdb/bentuk_pendidikan';
export const apiGetSekolah           = lokal + '/kurikulum/ppdb/sekolah/';//:kode_wilayah/:bentuk_pendidikan_id';
export const apiGetKodeWilayah       = lokal + '/kurikulum/ppdb/';
export const apiGetSiswa             = lokal + '/kurikulum/ppdb/siswa/';//:npsn'
export const apiGetKab               = lokal + '/kurikulum/ppdb/wilayah?id_level_wilayah=2';//:npsn'
export const apiGetKec               = lokal + '/kurikulum/ppdb/wilayah?id_level_wilayah=';//3&mst_kode_wilayah=026900'//:npsn'
export const apiGetSiswaBaru         = lokal + '/kurikulum/ppdb/siswa';

//POST
export const apiPostJurusan          = lokal + '/kurikulum/jurusan';
export const apiPostRombel           = lokal + '/kurikulum/rombel';
export const apiPostMapelGuru        = lokal + '/kurikulum/pembelajaran';
export const apiPostWaliKelas        = lokal + '/kurikulum/rombel/wali';
export const apiPostPPDB             = lokal + '/kurikulum/ppdb';
export const apiPostSiswaBaru        = lokal + '/kurikulum/ppdb/rombel';
export const apiPostJadwal           = lokal + '/kurikulum/pembelajaran/jadwal_kurikulum'

//PATCH
export const apiPatchJurusan         = lokal + '/kurikulum/jurusan/';
export const apiPatchPd              = lokal + '/kurikulum/rombel/anggota';
export const apiPatchSoal            = lokal + '/kurikulum/soal';
export const apiPatchJawaban         = lokal + '/kurikulum/soal/jawaban';
export const apiPatchMateri          = lokal + '/kurikulum/materi';

//PUT
export const apiPutPGuru             = lokal + '/kurikulum/pembelajaran/guru';
export const apiPutPesertaDidik      = lokal + '/kurikulum/master/master_pd';
export const apiPutGuru              = lokal + '/kurikulum/master/master_ptk';
export const apiPutJadwal            = lokal + '/kurikulum/pembelajaran/jadwal_kurikulum';

//DELETE
export const apiDeleteJadwal         = lokal + '/kurikulum/pembelajaran/jadwal_kurikulum/';