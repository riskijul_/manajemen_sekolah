export function configureFakeBackend() {
    let users = [
        { id: 0, username: 'test', password: 'test', firstName: 'Admin', lastName: '', level: '0', param_route: '_admin' },
        { id: 1, username: 'test1', password: 'test1', firstName: 'Tata Usaha', lastName: '', level: '1', param_route: '_tata_usaha' },
        { id: 2, username: 'test2', password: 'test2', firstName: 'Wali Kelas', lastName: '', level: '2', param_route: '_wali' },
        { id: 3, username: 'test3', password: 'test3', firstName: 'Sekolah', lastName: '', level: '3', param_route: '_sekolah' },
        { id: 3, username: '710302200007@tikomdik.id', password: '19710302', firstName: 'Tata Usaha', lastName: '', level: '1', param_route: '_tata_usaha' }];
    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        const isLoggedIn = opts.headers['Authorization'] === 'Bearer fake-jwt-token';

        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(() => {
                // authenticate - public
                if (url.endsWith('/user/auth') && opts.method === 'POST') {
                    const params = JSON.parse(opts.body);
                    const user = users.find(x => x.username === params.username && x.password === params.password);
                    if (!user) return error('Username or password is incorrect');
                    return ok({
                        id: user.id,
                        username: user.username,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        level: user.level,
                        param_route: user.param_route,
                        token: 'fake-jwt-token'
                    });
                }

                // get users - secure
                if (url.endsWith('/user') && opts.method === 'GET') {
                    if (!isLoggedIn) return unauthorised();
                    return ok(users);
                }

                // pass through any requests not handled above
                realFetch(url, opts).then(response => resolve(response));

                // private helper functions

                function ok(body) {
                    resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(body)) })
                }

                function unauthorised() {
                    resolve({ status: 401, text: () => Promise.resolve(JSON.stringify({ message: 'Unauthorised' })) })
                }

                function error(message) {
                    resolve({ status: 400, text: () => Promise.resolve(JSON.stringify({ message })) })
                }
            }, 500);
        });
    }
}