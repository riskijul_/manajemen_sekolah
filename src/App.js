import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './scss/style.scss';
import { PrivateRoute, apiVerifyAuth } from './_components';
import { history } from './_helpers';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
const Landing   = React.lazy(() => import('./views/pages/landing/Landing'));
const Login     = React.lazy(() => import('./views/pages/login/Login'));
const LoginPPDB = React.lazy(() => import('./views/pages/login/LoginPPDB'));
const Register  = React.lazy(() => import('./views/pages/register/Register'));
const Page404   = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500   = React.lazy(() => import('./views/pages/page500/Page500'));

class App extends Component {

  render() {
    return (
      <HashRouter history={history}>
          <React.Suspense fallback={loading}>
            <Switch>
              <Route exact path="/landing" name="Login Page" render={props => <Landing {...props}/>} />
              <Route exact path="/login" name="Login MDSp" render={props => <Login {...props}/>} />
              <Route exact path="/login_ppdb_swasta" name="Login PPDB" render={props => <LoginPPDB {...props}/>} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
              <PrivateRoute path="/" name="Home" />
            </Switch>
          </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
