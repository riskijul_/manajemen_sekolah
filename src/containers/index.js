import TheContent from './TheContent'
import TheFooter from './TheFooter'
import TheHeader from './TheHeader'
import TheHeaderDropdown from './TheHeaderDropdown'
import TheHeaderDropdownMssg from './TheHeaderDropdownMssg'
import TheHeaderDropdownNotif from './TheHeaderDropdownNotif'
import TheHeaderDropdownTasks from './TheHeaderDropdownTasks'
import TheSidebar from './TheSidebar'

//PPDB
import TheContentPPDB from './PPDB/TheContent'
import TheFooterPPDB from './PPDB/TheFooter'
import TheHeaderPPDB from './PPDB/TheHeader'
import TheHeaderDropdownPPDB from './PPDB/TheHeaderDropdown'
import TheHeaderDropdownMssgPPDB from './PPDB/TheHeaderDropdownMssg'
import TheHeaderDropdownNotifPPDB from './PPDB/TheHeaderDropdownNotif'
import TheHeaderDropdownTasksPPDB from './PPDB/TheHeaderDropdownTasks'
import TheSidebarPPDB from './PPDB/TheSidebar'

//MAIN CONTENT
import TheLayout from './TheLayout'

//PPDB

export {
  TheContent,
  TheFooter,
  TheHeader,
  TheHeaderDropdown,
  TheHeaderDropdownMssg,
  TheHeaderDropdownNotif,
  TheHeaderDropdownTasks,
  TheSidebar,

  //PPDB
  TheContentPPDB,
  TheFooterPPDB,
  TheHeaderPPDB,
  TheHeaderDropdownPPDB,
  TheHeaderDropdownMssgPPDB,
  TheHeaderDropdownNotifPPDB,
  TheHeaderDropdownTasksPPDB,
  TheSidebarPPDB,

  TheLayout
}
