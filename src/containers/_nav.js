export default [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/manajemen_sekolah/dashboard_kelola_data',
    icon: 'cil-home'
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['MASTER']
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Master',
    route: '/base',
    icon: 'cil-people',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Guru',
        to: '/manajemen_sekolah/master/list_guru',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Peserta Didik',
        to: '/manajemen_sekolah/master/list_jurusan',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Ruangan',
        to: '/manajemen_sekolah/master/list_prasarana',
      },
    ]
  },

  {
    _tag: 'CSidebarNavTitle',
    _children: ['PENGELOLAAN']
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Pengelolaan',
    route: '/base',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Ambil Data Siswa Baru',
        to: '/manajemen_sekolah/ppdb',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Data Siswa Baru',
        to: '/manajemen_sekolah/siswa_baru',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Jurusan',
        to: '/manajemen_sekolah/jurusan',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Rombel',
        to: '/manajemen_sekolah/kelola_rombel',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Rombel Siswa',
        to: '/manajemen_sekolah/pengelolaan',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Wali Kelas',
        to: '/manajemen_sekolah/kelola_wali_kelas',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Jadwal Pelajaran',
        to: '/manajemen_sekolah/jadwal/list_jurusan',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Ambil Mapel C1',
        to: '/manajemen_sekolah/kelola_mapel_sekolah',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Penugasan Guru',
        to: '/manajemen_sekolah/kelola_mapel_guru',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Validasi Soal',
        to: '/manajemen_sekolah/validasi_soal',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Validasi Materi',
        to: '/manajemen_sekolah/validasi_materi',
      },
    ]
  },
]
