export default [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/ppdb/dashboard_ppdb',
    icon: 'cil-home'
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['---']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'PPDb Swasta',
    to: '/ppdb/ppdb_swasta',
    icon: 'cil-file'
  },
]