import React from 'react';
// import TambahKompetensiKeahlian from './views/kurikulum/TambahKompetensiKeahlian';

const Toaster = React.lazy(() => import('./views/notifications/toaster/Toaster'));
const Tables = React.lazy(() => import('./views/base/tables/Tables'));

const Breadcrumbs = React.lazy(() => import('./views/base/breadcrumbs/Breadcrumbs'));
const Cards = React.lazy(() => import('./views/base/cards/Cards'));
const Carousels = React.lazy(() => import('./views/base/carousels/Carousels'));
const Collapses = React.lazy(() => import('./views/base/collapses/Collapses'));
const BasicForms = React.lazy(() => import('./views/base/forms/BasicForms'));

const Jumbotrons = React.lazy(() => import('./views/base/jumbotrons/Jumbotrons'));
const ListGroups = React.lazy(() => import('./views/base/list-groups/ListGroups'));
const Navbars = React.lazy(() => import('./views/base/navbars/Navbars'));
const Navs = React.lazy(() => import('./views/base/navs/Navs'));
const Paginations = React.lazy(() => import('./views/base/paginations/Pagnations'));
const Popovers = React.lazy(() => import('./views/base/popovers/Popovers'));
const ProgressBar = React.lazy(() => import('./views/base/progress-bar/ProgressBar'));
const Switches = React.lazy(() => import('./views/base/switches/Switches'));

const Tabs = React.lazy(() => import('./views/base/tabs/Tabs'));
const Tooltips = React.lazy(() => import('./views/base/tooltips/Tooltips'));
const BrandButtons = React.lazy(() => import('./views/buttons/brand-buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() => import('./views/buttons/button-dropdowns/ButtonDropdowns'));
const ButtonGroups = React.lazy(() => import('./views/buttons/button-groups/ButtonGroups'));
const Buttons = React.lazy(() => import('./views/buttons/buttons/Buttons'));
const Charts = React.lazy(() => import('./views/charts/Charts'));
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/icons/coreui-icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/icons/flags/Flags'));
const Brands = React.lazy(() => import('./views/icons/brands/Brands'));
const Alerts = React.lazy(() => import('./views/notifications/alerts/Alerts'));
const Badges = React.lazy(() => import('./views/notifications/badges/Badges'));
const Modals = React.lazy(() => import('./views/notifications/modals/Modals'));
const Colors = React.lazy(() => import('./views/theme/colors/Colors'));
const Typography = React.lazy(() => import('./views/theme/typography/Typography'));
const Widgets = React.lazy(() => import('./views/widgets/Widgets'));
const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));

//MASTER [KURIKULUM]
const ListJurusan               = React.lazy(() => import('./views/kurikulum/master/ListJurusan'));
const ListRombel                = React.lazy(() => import('./views/kurikulum/master/ListRombel'));
const ListPesertaDidik          = React.lazy(() => import('./views/kurikulum/master/ListPesertaDidik'));
const EditPesertaDidik          = React.lazy(() => import('./views/kurikulum/master/EditPesertaDidik'));
const ListGuru                  = React.lazy(() => import('./views/kurikulum/master/ListGuru'));
const EditGuru                  = React.lazy(() => import('./views/kurikulum/master/EditGuru'));
const ListPrasarana             = React.lazy(() => import('./views/kurikulum/master/ListPrasarana'));
const ListRuangan               = React.lazy(() => import('./views/kurikulum/master/ListRuangan'));
const DetailRuangan             = React.lazy(() => import('./views/kurikulum/master/DetailRuangan'));

//JADWAL [KURIKULUM]
const ListJurusanJadwal         = React.lazy(() => import('./views/kurikulum/jadwal/ListJurusan'));
const ListRombelJadwal          = React.lazy(() => import('./views/kurikulum/jadwal/ListRombel'));
const ListJadwal                = React.lazy(() => import('./views/kurikulum/jadwal/ListJadwal'));
const EditJadwal                = React.lazy(() => import('./views/kurikulum/jadwal/EditJadwal'));

//KURIKULUM
const DashboardKurikulum        = React.lazy(() => import('./views/dashboard/DashboardTataUsaha'));
const LaporanWaliKelas          = React.lazy(() => import('./views/kurikulum/LaporanWaliKelas'));
const Pengelolaan               = React.lazy(() => import('./views/kurikulum/Pengelolaan'));
const DetailRombel              = React.lazy(() => import('./views/kurikulum/DetailRombel'));
const JurusanSp_Kur             = React.lazy(() => import('./views/kurikulum/JurusanSp'));
const DetailJurusan_Kur         = React.lazy(() => import('./views/kurikulum/DetailJurusan'));
const KelolaPD                  = React.lazy(() => import('./views/kurikulum/KelolaPd'));
const ListKeahlian              = React.lazy(() => import('./views/kurikulum/ListKeahlian'));
const ListKeahlianBidang        = React.lazy(() => import('./views/kurikulum/ListKeahlianBidang'));
const InputSK                   = React.lazy(() => import('./views/kurikulum/InputSK'));
const TambahKompetensiKeahlian  = React.lazy(() => import('./views/kurikulum/TambahKompetensiKeahlian'));
const KelolaRombel              = React.lazy(() => import('./views/kurikulum/KelolaRombel'));
const ListKeahlianRombel        = React.lazy(() => import('./views/kurikulum/ListKeahlianRombel'));
const KelolaGuru                = React.lazy(() => import('./views/kurikulum/KelolaGuru'));
const AssignMapelGuru           = React.lazy(() => import('./views/kurikulum/AssignMapelGuru'));
const KelolaWaliKelas           = React.lazy(() => import('./views/kurikulum/KelolaWaliKelas'));
const ListKompetensiKeahlian    = React.lazy(() => import('./views/kurikulum/ListKompetensiKeahlian'));

const KelolaMapelSekolah        = React.lazy(() => import('./views/kurikulum/KelolaMapelSekolah'));
const KelolaMapelGuru           = React.lazy(() => import('./views/kurikulum/KelolaMapelGuru'));

//PPDB
const DashboardPPDB             = React.lazy(() => import('./views/dashboard/DashboardPPDB'));
const PPDBSwasta                = React.lazy(() => import('./views/ppdb/PPDBSwasta'));
const PilihRombel               = React.lazy(() => import('./views/ppdb/PilihRombel'));
const WilayahSekolah            = React.lazy(() => import('./views/ppdb/WilayahSekolah'));
const ListSiswaPPDB             = React.lazy(() => import('./views/ppdb/ListSiswaPPDB'));

//VALIDASI SOAL
const ValidasiSoal              = React.lazy(() => import('./views/kurikulum/ValidasiSoal'));
const RincianSoal               = React.lazy(() => import('./views/kurikulum/RincianSoal'));
const ValidasiMateri            = React.lazy(() => import('./views/kurikulum/ValidasiMateri'));

const SiswaBaru                 = React.lazy(() => import('./views/kurikulum/SiswaBaru'));

const routes = [
  //MASTER
  { path: '/manajemen_sekolah/master/list_jurusan', name: 'Pilih Jurusan', component: ListJurusan, exact: true},
  { path: '/manajemen_sekolah/master/list_jurusan/list_rombel', name: 'Pilih Rombel', component: ListRombel, exact: true},
  { path: '/manajemen_sekolah/master/list_jurusan/list_rombel/list_peserta_didik', name: 'List Peserta Didik', component: ListPesertaDidik, exact: true},
  { path: '/manajemen_sekolah/master/list_jurusan/list_rombel/list_peserta_didik/edit_peserta_didik', name: 'Edit Peserta Didik', component: EditPesertaDidik },
  { path: '/manajemen_sekolah/master/list_guru', name: 'List Guru', component: ListGuru, exact: true },
  { path: '/manajemen_sekolah/master/list_guru/edit_guru', name: 'Edit Guru', component: EditGuru },
  { path: '/manajemen_sekolah/master/list_prasarana', name: 'List Prasarana', component: ListPrasarana, exact: true },
  { path: '/manajemen_sekolah/master/list_prasarana/list_ruangan', name: 'List Ruangan', component: ListRuangan, exact: true },
  { path: '/manajemen_sekolah/master/list_prasarana/list_ruangan/detail_ruangan', name: 'Detail Ruangan', component: DetailRuangan },

  //JADWAL
  { path: '/manajemen_sekolah/jadwal/list_jurusan', name: 'Pilih Jurusan', component: ListJurusanJadwal, exact: true },
  { path: '/manajemen_sekolah/jadwal/list_jurusan/list_rombel', name: 'Pilih Rombel', component: ListRombelJadwal, exact: true },
  { path: '/manajemen_sekolah/jadwal/list_jurusan/list_rombel/list_jadwal', name: 'List Jadwal Mata Pelajaran', component: ListJadwal, exact: true },
  { path: '/manajemen_sekolah/jadwal/list_jurusan/list_rombel/list_jadwal/edit_jadwal', name: 'Edit Jadwal Mata Pelajaran', component: EditJadwal },

  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme', name: 'Theme', component: Colors, exact: true },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', name: 'Base', component: Cards, exact: true },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/forms', name: 'Forms', component: BasicForms },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/brands', name: 'Brands', component: Brands },
  { path: '/notifications', name: 'Notifications', component: Alerts, exact: true },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/notifications/toaster', name: 'Toaster', component: Toaster },
  { path: '/widgets', name: 'Widgets', component: Widgets },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },

  { path: '/manajemen_sekolah/dashboard_kelola_data', name: 'Dashboard Pengelolaan Data', component: DashboardKurikulum },
  { path: '/manajemen_sekolah/laporan_wali_kelas', name: 'Laporan Wali Kelas', component: LaporanWaliKelas },
  //
  { path: '/manajemen_sekolah/pengelolaan', name: 'Kelola Peserta Didik', component: Pengelolaan, exact: true},
  { path: '/manajemen_sekolah/pengelolaan/list_keahlian/detail_rombel', name: 'Detail Rombel', component: DetailRombel},
  //
  { path: '/manajemen_sekolah/jurusan', name: 'Kelola Kompetensi Keahlian', component: JurusanSp_Kur, exact: true},
  { path: '/manajemen_sekolah/jurusan/detail', name: 'Jurusan Detail', component: DetailJurusan_Kur },
  { path: '/manajemen_sekolah/kelola_pd', name: 'Kelola Peserta Didik', component: KelolaPD },
  { path: '/manajemen_sekolah/pengelolaan/list_rombel', name: 'List Rombongan Belajar', component: ListKeahlian, exact: true },
  { path: '/manajemen_sekolah/jurusan/input_sk', name: 'Tambah Jurusan Baru', component: InputSK },
  { path: '/manajemen_sekolah/jurusan/tambah_kompetensi_keahlian', name: 'Lembar Isian Tambah Kompetensi Keahlian Baru', component: TambahKompetensiKeahlian},
  //
  { path: '/manajemen_sekolah/kelola_rombel', name: 'Kelola Rombongan Belajar', component: KelolaRombel, exact: true },
  { path: '/manajemen_sekolah/kelola_rombel/bidang_keahlian', name: 'List Rombongan Belajar', component: ListKeahlianBidang },
  { path: '/manajemen_sekolah/kelola_rombel/list_rombongan_belajar', name: 'List Rombongan Belajar', component: ListKeahlianRombel },
  { path: '/manajemen_sekolah/kelola_guru', name: 'Kelola Guru Pengajar', component: KelolaGuru, exact: true },

  { path: '/manajemen_sekolah/kelola_wali_kelas', name: 'Kelola Wali Kelas', component: KelolaWaliKelas, exact: true },
  { path: '/manajemen_sekolah/kelola_wali_kelas/list_kompetensi_keahlian', name: 'List Kompetensi Keahlian', component: ListKompetensiKeahlian },

  { path: '/manajemen_sekolah/kelola_mapel_sekolah', name: 'Kelola Mata Pelajaran Sekolah', component: KelolaMapelSekolah, exact: true },
  { path: '/manajemen_sekolah/kelola_mapel_guru', name: 'Kelola Mata Pelajaran Guru', component: KelolaMapelGuru, exact: true },
  { path: '/manajemen_sekolah/kelola_mapel_guru/penugasan_guru', name: 'Penugasan Guru', component: AssignMapelGuru },

  { path: '/ppdb/dashboard_ppdb', name: 'Dashboard PPDB', component: DashboardPPDB },
  { path: '/manajemen_sekolah/ppdb', name: 'Update Siswa Terdaftar', component: PPDBSwasta, exact: true},
  { path: '/manajemen_sekolah/ppdb/wilayah', name: 'Wilayah Sekolah', component: WilayahSekolah, exact: true },
  { path: '/manajemen_sekolah/ppdb/wilayah/list_siswa', name: 'List Siswa', component: ListSiswaPPDB },

  { path: '/manajemen_sekolah/validasi_soal', name: 'List Soal', component: ValidasiSoal, exact: true },
  { path: '/manajemen_sekolah/validasi_soal/rincian_soal', name: 'Rincian Soal', component: RincianSoal },
  { path: '/manajemen_sekolah/validasi_materi', name: 'Validasi Materi', component: ValidasiMateri, exact: true },

  { path: '/manajemen_sekolah/siswa_baru', name: 'Peserta Didik Baru', component: SiswaBaru, exact: true },

];

export default routes;
